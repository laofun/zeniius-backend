# Zeniius Backend

Social Network for CEO

## Prerequisites

* Go
* Dep
* Postgres

## Setup environment

* Create a postgres database
* Seed database

`cat migrations/*sql | pipe_into_postgres`

* Config `.env`
* Install dependecies with `dep ensure`

## How to login

A magic way to login.

1. Got a `register.html` HTML template
2. Signin with email and password
3. Signout and re signin
4. Inspect from the web client, check `/verifyPassword` outgoing network. Get `idToken`
5. Sign in `/login` with this `idToken`

## Auto reload in Development mode

To auto reload project when update source code:

* Install `bra`: `go get -u github.com/Unknwon/bra`
* Run `bra run`
* Config file: `.bra.toml`