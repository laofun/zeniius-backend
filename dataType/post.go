package dataType

type PostPrivacy struct {
	IsPublic   bool     `json:"is_public"`
	ShareGroup []string `json:"share_group"`
}

type PostShare struct {
	ShareGroup []string `json:"share_group"`
}
