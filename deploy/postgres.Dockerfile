FROM postgres:10.3

COPY ./migrations/*.sql /docker-entrypoint-initdb.d/
COPY ./deploy/unaccent.rules /usr/share/postgresql/10/tsearch_data/unaccent.rules