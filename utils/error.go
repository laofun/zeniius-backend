package utils

import "fmt"

// Resource Not found error
type notFound interface {
	NotFound() bool
}

func IsNotFound(err error) bool {
	nf, ok := err.(notFound)
	return ok && nf.NotFound()
}

type ResourceNotFound struct {
	ObjectType string
	ObjectID   int64
}

func (r ResourceNotFound) NotFound() bool {
	return true
}

func (r ResourceNotFound) Error() string {
	return fmt.Sprintf("%s %d not found", r.ObjectType, r.ObjectID)
}

// Resource already existed error
type alreadyExisted interface {
	Exist() bool
}

func IsExist(err error) bool {
	nf, ok := err.(alreadyExisted)
	return ok && nf.Exist()
}

type ResourceExisted struct {
	ObjectType string
	ObjectID   int64
}

func (r ResourceExisted) Exist() bool {
	return true
}

func (r ResourceExisted) Error() string {
	return fmt.Sprintf("%s %d exsited", r.ObjectType, r.ObjectID)
}

// Unauthorized to do action error
type unAuthorized interface {
	UnAuthorized() bool
}

func isUnAuthorized(err error) bool {
	ua, ok := err.(unAuthorized)
	return ok && ua.UnAuthorized()
}

type UnAuthorizedAction struct {
	UserID   int64
	Action   string
	ObjectID int64
}

func (u UnAuthorizedAction) UnAuthorized() bool {
	return true
}

func (u UnAuthorizedAction) Error() string {
	return fmt.Sprintf("user %d not authorized to %s of %d", u.UserID, u.Action, u.ObjectID)
}
