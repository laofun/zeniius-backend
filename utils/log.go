package utils

import (
	"fmt"
	"net/http"
)

func GetPath(r *http.Request) string {
	return fmt.Sprintf("%s %s", r.Host, r.URL.Path)
}
