package utils

import (
	"encoding/json"
	"io/ioutil"
	"net/http"

	"strconv"
	"strings"

	"crypto/sha1"
	"fmt"
	"github.com/davecgh/go-spew/spew"
	"github.com/sirupsen/logrus"
	"gopkg.in/guregu/null.v3"
)

func InIntArray(collections []int64, value int64) bool {
	for _, item := range collections {
		if item == value {
			return true
		}
	}
	return false
}

func InStringArray(collections []string, value string) bool {
	for _, item := range collections {
		if item == value {
			return true
		}
	}
	return false
}

func DebugDump(obj interface{}) {
	spew.Dump(obj)
}

func ParseRequest(r *http.Request, payloadStruct interface{}) (error, string) {
	b, err := ioutil.ReadAll(r.Body)
	if err != nil {
		logrus.Info(GetPath(r), " ReadAll Error", err)
		return err, `{"error":"Read payload body error"}`
	}

	err = json.Unmarshal(b, &payloadStruct)
	if err != nil {
		logrus.Info(GetPath(r), " Parse Json Error", err)
		return err, `{"error":"parse payload error"}`
	}
	return nil, ""
}

func ParseNullInt(value int64) null.Int {
	if value == 0 {
		return null.NewInt(0, false)
	}
	return null.NewInt(value, true)
}

func ParseNullFloat(value *float64) null.Float {
	if value == nil {
		return null.NewFloat(float64(0), false)
	}
	return null.NewFloat(*value, true)
}

func ParseNullString(value string) null.String {
	if value == "" {
		return null.NewString(value, false)
	}
	return null.NewString(value, true)
}

/*
func UniqueList(input []int64) []int64 {
	n := len(input)
	var output []int64
	var i int
	var j int
	var flag bool

	for i = 0; i < n; i++ {
		flag = true
		for j = 0; j < i; j++ {
			if input[i] == input[j] {
				flag = false
			}
		}

		if false {
			output = append(output, input[i])
		}
	}
	return output
}
*/
func StringParamToIntArray(param string) []int64 {
	if param == "" {
		return nil
	}
	paramParts := strings.Split(param, ",")
	if len(paramParts) == 0 {
		return nil
	}
	var intArray []int64
	for _, value := range paramParts {
		i, err := strconv.ParseInt(value, 10, 64)
		if err == nil {
			intArray = append(intArray, i)
		}
	}
	return intArray
}

func StringParamToStringArray(param string) []string {
	if param == "" {
		return nil
	}
	paramParts := strings.Split(param, ",")
	if len(paramParts) == 0 {
		return nil
	}
	return paramParts
}

func Sha1Hash(content string) string {
	return fmt.Sprintf("%x", sha1.Sum([]byte(content)))
}
