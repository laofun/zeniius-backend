package utils

const CostTypeFree = "free"
const CostTypeNegotiation = "negotiation"
const CostTypeRange = "range"

const PostTypeOpportunity = "opportunity"
const PostTypeDiscussion = "discussion"
const PostTypeEvent = "event"
