package utils

import (
	"os"

	"github.com/sirupsen/logrus"
)

func GetEnv(key string, defaultValue string) string {
	v := os.Getenv(key)
	if v == "" {
		logrus.Infof("Using default value %s for key %s", defaultValue, key)
		return defaultValue
	}
	logrus.Infof("Using value %s for key %s", v, key)
	return v
}
