package middleware

import (
	"context"
	"gitlab.com/rockship/backend/zeniius-backend/service"
	"gitlab.com/rockship/backend/zeniius-backend/utils"
	"net/http"
)

func ExtractLocale(next http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		locale := r.Header.Get("Accept-Language")
		if !utils.InStringArray(service.AcceptLanguage, locale) {
			locale = service.LanguageVietnam
		}

		ctx := context.WithValue(r.Context(), "locale", locale)
		next.ServeHTTP(w, r.WithContext(ctx))
	}
	return http.HandlerFunc(fn)
}
