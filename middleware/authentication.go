package middleware

import (
	"context"

	"net/http"

	"gitlab.com/rockship/backend/zeniius-backend/models"
	"gitlab.com/rockship/backend/zeniius-backend/service"
)

func VerifyAccessTokenMiddleware(next http.Handler) http.Handler {
	db := service.GetDatabaseConnection()

	fn := func(w http.ResponseWriter, r *http.Request) {
		var t models.AccessToken

		token := r.Header.Get("Authorization")
		if token == "" {
			http.Error(w, http.StatusText(401), http.StatusUnauthorized)
			return
		}

		isAuthorized := t.CheckAccessToken(db, token)
		if isAuthorized == false {
			http.Error(w, http.StatusText(401), http.StatusUnauthorized)
			return
		}

		ctx := context.WithValue(r.Context(), "user", t.User)
		next.ServeHTTP(w, r.WithContext(ctx))
	}
	return http.HandlerFunc(fn)
}
