package api

import (
	"gitlab.com/rockship/backend/zeniius-backend/models"
	response "gitlab.com/rockship/backend/zeniius-backend/response"
	"gitlab.com/rockship/backend/zeniius-backend/utils"
	"net/http"
)

type TranslateParam struct {
	ObjectType string `json:"object_type"`
	ObjectID   int64  `json:"object_id"`
	Locale     string `json:"locale"`
}

// swagger:route POST /translate translate translateText
//
// Translate text
// Security:
//		api_key:
// Responses:
//		200: Business
func (e *Env) TranslateText(w http.ResponseWriter, r *http.Request) error {
	var payload TranslateParam
	err, message := utils.ParseRequest(r, &payload)
	if err != nil {
		return ResponseFail(400, message, err)
	}
	translatedText, err := models.TranslateObjectText(e.DB, payload.ObjectType,
		payload.ObjectID, payload.Locale)
	if err != nil {
		return ResponseFail(500, "translate error", err)
	}
	res := map[string]string{
		"text": translatedText,
	}
	return ResponseData{200, nil, response.JsonEncodeModel(res)}

}
