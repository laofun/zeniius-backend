package api

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strconv"

	"github.com/go-chi/chi"

	"gitlab.com/rockship/backend/zeniius-backend/models"
)

// GetAllFollower get all user following me
// swagger:route GET /followers relationship_follow GetAllFollower
// Get all user following me
//
// Get all user following me
// Security:
//		api_key:
// Responses:
// 		200: UserList
func (e *Env) GetAllFollower(w http.ResponseWriter, r *http.Request) error {
	user := r.Context().Value("user").(*models.User)

	followerIDs, err := user.GetAllFollowingID(e.DB)
	if err != nil {
		return ResponseFail(400, "", err)
	}

	followers, err := models.GetMultiUserByID(e.DB, followerIDs)
	if err != nil {
		return ResponseFail(400, "", err)
	}

	jsonData, err := json.Marshal(followers)
	if err != nil {
		return ResponseFail(400, "", err)
	}
	return ResponseData{200, nil, jsonData}
}

// GetAllFollowering get all user i am following
// swagger:route GET /following relationship_follow GetAllFollowering
// Get all user i am following to
//
// Get all user i am following to
// Security:
//		api_key:
// Responses:
// 		200: UserList
func (e *Env) GetAllFollowering(w http.ResponseWriter, r *http.Request) error {
	user := r.Context().Value("user").(*models.User)

	followerIDs, err := user.GetAllFollowingID(e.DB)
	if err != nil {
		return ResponseFail(400, "", err)
	}

	followers, err := models.GetMultiUserByID(e.DB, followerIDs)
	if err != nil {
		return ResponseFail(400, "", err)
	}

	jsonData, err := json.Marshal(followers)
	if err != nil {
		return ResponseFail(400, "", err)
	}
	return ResponseData{200, nil, jsonData}
}

// GetUserFollowering get all following user of a specific UserID
// swagger:route GET /users/{id}/following relationship_follow user GetUserFollowering
// get all following user of a specific UserID
//
// get all following user of a specific UserID
// Security:
//		api_key:
// Responses:
// 		200: UserList
func (e *Env) GetUserFollowering(w http.ResponseWriter, r *http.Request) error {
	userID, err := strconv.ParseInt(chi.URLParam(r, "id"), 10, 64)
	if err != nil {
		return ResponseData{400, err, []byte(`{"error":"parse id error"}`)}
	}

	user := models.User{
		ID: userID,
	}
	followerIDs, err := user.GetAllFollowingID(e.DB)
	if err != nil {
		return ResponseFail(400, "", err)
	}

	followers, err := models.GetMultiUserByID(e.DB, followerIDs)
	if err != nil {
		return ResponseFail(400, "", err)
	}

	jsonData, err := json.Marshal(followers)
	if err != nil {
		return ResponseFail(400, "", err)
	}
	return ResponseData{200, nil, jsonData}
}

// AddNewFollowerParam ..
// swagger:parameters AddNewFollower
type AddNewFollowerParam struct {
	// ID of Follower
	//
	// in: body
	// required: true
	FollowerID int64 `json:"follower_id"`
}

// AddFollowering follow new user
// swagger:route POST /following relationship_follow AddNewFollower
// Follow a user
//
// Follow a user
// Security:
//		api_key:
// Responses:
// 		200: SimpleResponse
func (e *Env) AddFollowering(w http.ResponseWriter, r *http.Request) error {
	user := r.Context().Value("user").(*models.User)

	bPayload, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return ResponseFail(500, "Can't parse body request.", err)
	}

	payload := AddNewFollowerParam{}
	err = json.Unmarshal(bPayload, &payload)
	if err != nil {
		return ResponseFail(500, "JSON Wrong format.", err)
	}

	_, err = user.AddFollowing(e.DB, payload.FollowerID)
	if err != nil {
		return ResponseFail(500, "Follower not found!", err)
	}
	return ResponseSuccess()
}

// RemoveFollowerParam ..
// swagger:parameters RemoveFollowerParam
type RemoveFollowerParam struct {
	// ID of Follower
	// in: path
	// required: true
	FollowerID int64 `json:"follower_id"`
}

// RemoveFollowering unfollow
// swagger:route DELETE /following/{follower_id} relationship_follow RemoveFollowerParam
// Unfollow a user
//
// Remove a follower (unfollow) a user
// Security:
//		api_key:
// Responses:
// 		200: SimpleResponse
func (e *Env) RemoveFollowering(w http.ResponseWriter, r *http.Request) error {
	user := r.Context().Value("user").(*models.User)
	userIDParam := chi.URLParam(r, "followerID")

	userID, err := strconv.Atoi(userIDParam)
	if err != nil {
		return ResponseFail(400, "A valid follower id is required.", nil)
	}

	err = user.RemoveFollowingID(e.DB, int64(userID))
	if err != nil {
		return ResponseFail(400, "Follower not found!", err)
	}
	return ResponseSuccess()
}
