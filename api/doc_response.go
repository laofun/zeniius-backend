package api

import (
	"time"
)

// Default Response
// swagger:response Default
type DefaultResponse struct {
	// in: body
	Body struct {
		Message string `json:"message"`
	}
}

// SimpleResponse This will return status of your request.
// swagger:response SimpleResponse
type SimpleResponse struct {
	// Status of request
	Success bool `json:"success"`
	// Message of error
	Message string `json:"message"`
}

//
// Post
// swagger:response Post
type PostResponse struct {
	// in: body
	Body struct {
		ID             int64              `json:"id" `
		User           string             `json:"user,omitempty"`
		UserId         int64              `json:"-" `
		Type           string             `json:"type"`
		Title          string             `json:"title"`
		TitleEng       string             `json:"title_en" `
		Description    string             `json:"description"`
		DescriptionEng string             `json:"description_en" `
		CreatedAt      time.Time          `json:"created_at" `
		Files          []FileResponse     `json:"files" `
		ViewCount      int64              `json:"view_count" `
		CommentCount   int64              `json:"comment_count" `
		RegisterCount  int64              `json:"register_count" `
		LikeCount      int64              `json:"like_count" `
		ShareCount     int64              `json:"share_count" `
		Fields         string             `json:"fields"`
		Status         string             `json:"status" `
		DemandType     string             `json:"demand_type" `
		Location       string             `json:"location" `
		Lat            float64            `json:"location_lat" `
		Long           float64            `json:"location_long" `
		Comment        CommentResponse    `json:"comment" `
		Liked          bool               `json:"liked" `
		Registered     bool               `json:"registered" `
		IsPublic       bool               `json:"is_public" `
		EndTime        *time.Time         `json:"end_time" `
		StartTime      *time.Time         `json:"start_time" `
		Business       []BusinessResponse `json:"business" `
		Group          []GroupResponse    `json:"group"`
	}
}

// File
// swagger:response File
type FileResponse struct {
	ID         int64     `json:"id" `
	FullPath   string    `json:"fullPath" `
	Size       int64     `json:"size"`
	Width      int64     `json:"width"`
	Height     int64     `json:"height"`
	Type       string    `json:"type"`
	Thumbnail  string    `json:"thumbnail"`
	Filename   string    `json:"filename"`
	MimeType   string    `json:"mime_type" `
	Extension  string    `json:"extension"`
	CreatedAt  time.Time `json:"created_at" `
	BucketPath string    `json:"-" `
}

// Business
// swagger:response Business
type BusinessResponse struct {
	ID          int64      `json:"id" `
	Title       string     `json:"title" `
	Description string     `json:"description" `
	CreatedAt   *time.Time `json:"created_at" `
}

// Group
// swagger:response Group
type GroupResponse struct {
	ID          string `json:"id" `
	Name        string `json:"name" `
	Description string `json:"description" `
	Avatar      string `json:"avatar" `
}

// Company Business
// swagger:response CompanyBusiness
type CompanyBusinessResponse struct {
	ID          int64    `json:"id" `
	CompanyID   int64    `json:"company_id" `
	Business    struct{} `json:"business" `
	Description string   `json:"description" `
}

// Company Stub Profile
// swagger:response CompanyStub
type CompanyStub struct {
	ID   int64  `json:"id"`
	Name string `json:"name"`
}

// Share Post
// swagger:response SharePost
type SharePostResponse struct {
	SuccessGroups []string `json:"success_groups"`
	ErrorGroups   []string `json:"error_groups"`
}
