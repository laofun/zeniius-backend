// Package classification Zeniius Backend API.
//
// Description Zeniius backend server api .
//
//     Schemes: http, https
//     Host: zeniius.rockship.co
//     Version: 0.1.0
//     License: zeniius.rockship.co
//     Contact: https://rockship.co/
//     BasePath: /api/v1
//
//     Consumes:
//     - application/json
//
//     Produces:
//     - application/json
//
//     Security:
//     - api_key:
//	   - locale:
//
//     SecurityDefinitions:
//     api_key:
//          type: apiKey
//          name: Authorization
//          in: header
//     locale:
//          type: apiKey
//          name: Accept-Language
//          in: header
// swagger:meta
package api

import (
	"gitlab.com/rockship/backend/zeniius-backend/dataType"
	"time"
)

//swagger:parameters FirebaseToken
type FirebaseTokenAuth struct {
	// in:body
	Body LoginFirebase
}

// Common Error
// swagger:response GenericError
type GenericError struct {
	// in: body
	Body struct {
		Error string `json:"error"`
	}
}

// Access token
// swagger:response AccessToken
type AccessTokenResponse struct {
	// in: body
	Body struct {
		Platform   string    `json:"platform"`
		Token      string    `json:"access_token"`
		IPAddress  string    `json:"ip_address"`
		DeviceInfo string    `json:"device_info"`
		ExpiredAt  time.Time `json:"expired_at"`
		ExpiredIn  int32     `json:"expired_in"`
		CreatedAt  time.Time `json:"created_at"`
		User       string    `json:"user"`
	}
}

// Comment
// swagger:response Comment
type CommentResponse struct {
	// in: body
	Body struct {
		ID                    int64     `json:"id"`
		User                  string    `json:"user"`
		Content               string    `json:"content"`
		CreatedAt             time.Time `json:"created_at"`
		LikeCount             int       `json:"like_count" `
		ParentID              int       `json:"parent_id" `
		Attachment            []string  `json:"attachment"`
		PostID                int64     `json:"post_id"`
		HasChild              bool      `json:"has_children"`
		ChildrenCommentsCount int64     `json:"children_comment_count"`
		ChildrenComments      []string  `json:"children_comment"`
	}
}

// File
// swagger

// swagger:parameters listPosts
type ListPost struct {
	// Page number
	Page int `json:"page"`
	// Business IDs
	Business []int `json:"business"`
	// Group IDs
	Group []string `json:"group"`
	// Demand filter
	// enum : demands,ability,none
	Demand string `json:"demand"`
	// Filter
	// enum : public,related,following
	Filter string `json:"filter"`
}

// swagger:parameters listComments
type ListComment struct {
	// Page number
	Page int `json:"page"`
	// Post ID
	// in: path
	// required: true
	ID int `json:"id"`
}

// swagger:parameters uploadLink uploadFile
type UploadFileBody struct {
	// in: body
	Body UploadFile
}

// swagger:parameters listChildrenComments
type ListChildrenComment struct {
	// Page number
	Page int `json:"page"`
	// Comment ID
	// in: path
	// required: true
	ID int `json:"id"`
}

// swagger:parameters deleteComment
type DeleteComment struct {
	// Comment ID
	// in: path
	// required: true
	ID int `json:"id"`
}

// swagger:parameters updateComment
type UpdateComment struct {
	// Comment ID
	// in: path
	// required: true
	ID int `json:"id"`

	// Update comment payload
	// in: body
	Body UpdateCommentPayload
}

// swagger:parameters createComment
type CreateCommentPayload struct {
	// in: body
	Body CommentPayload
	// Post ID
	// in: path
	// required: true
	ID int `json:"id"`
}

// swagger:parameters searchUser
type SearchUserPayload struct {
	// Page number
	// in: query
	Page int `json:"page"`

	// Query
	// in: query
	Query string `json:"q"`
}

// swagger:parameters updateUserPersonal
type UserPersonalInfoPayload struct {
	// User personal information
	// in: body
	Body UpdateUserPersonalPayload
}

// swagger:parameters updateUserContact
type UserContactInfoPayload struct {
	// User contact
	// in: body
	Body UpdateUserContactPayload
}

// swagger:parameters updateUserAvatar updateUserCover
type UserImagePayload struct {
	// in: body
	Body UpdateUserImage
}

// swagger:parameters getUser GetUserFriend GetUserFollowering
type getUserProfilePayload struct {
	// User ID
	// in: path
	// required: true
	ID int `json:"id"`
}

// swagger:parameters createCompany
type UserCompanyPayload struct {
	// Company payload
	// in: body
	Body CompanyPayload
}

// swagger:parameters updateCompany
type UpdateUserCompanyPayload struct {
	// Company id
	// in: path
	// required: true
	ID int64 `json:"id"`

	// Company payload
	// in: body
	Body UpdateCompanyPayload
}

// swagger:parameters updateCompanyAlbum
type UpdateCompanyAlbumPayload struct {
	// Company id
	// in: path
	// required: true
	ID int64 `json:"id"`

	// Company payload
	// in: body
	Body CompanyAlbumPayload
}

// swagger:parameters addUserBusiness
type AddUserBusinessPayload struct {
	// User Business
	// in: body
	Body UserBusinessPayload
}

// swagger:parameters addCompanyBusiness
type AddCompanyBusinessPayload struct {
	// Company ID
	// in: path
	// required: true
	ID int64 `json:"id"`

	// Company business payload
	// in: body
	Body []CompanyBusinessPayload
}

// swagger:parameters updateUserVerification
type UpdateUserVerificationPayload struct {
	// User ID
	// in: path
	// required: true
	ID int64 `json:"id"`

	// Verification payload
	// in: body
	Body UserVerifyPayload
}

// swagger:parameters getCompanyOfUser
type GetCompanyOfUserPayload struct {
	// User ID
	// in: path
	// required: true
	ID int64 `json:"id"`
}

// swagger:parameters addCompanyCertificate
type AddCompanyCertificatePayload struct {
	// Company ID
	// in: path
	// required: true
	ID int64 `json:"id"`

	// add company certificate
	// in: body
	Body CompanyCertificatePayload
}

// swagger:parameters deleteCompanyCertificate
type DeleteCompanyCertificatePayload struct {
	// Company ID
	// in: path
	// required: true
	CompanyID int64 `json:"company_id"`

	// File ID
	// in: path
	// required: true
	FileID int64 `json:"file_id"`
}

// swagger:parameters reportPost
type CreatePostReportPayload struct {
	// Post ID
	// in: path
	// required: true
	PostID int64 `json:"id"`

	// Report payload
	// in: body
	// required: true
	Body ReportPostPayload
}

// swagger:parameters likePost registerPost unregisterPost getLikePostUser getRegisterPostUser getPostDetail deletePost sharePost
type LikePostPayload struct {
	// Post ID
	// in: path
	// required: true
	PostID int64 `json:"id"`
}

// swagger:parameters unlikePost
type UnlikePostPayload struct {
	// Post ID
	// in: path
	// required: true
	PostID int64 `json:"id"`
}

// swagger:parameters likeComment unlikeComment
type LikeCommentPayload struct {
	// Comment ID
	// in: path
	// required: true
	CommentID int64 `json:"id"`
}

// swagger:parameters createPost
type CreatePostPayload struct {
	// Create post payload
	// in: body
	Body PostPayload
}

// swagger:parameters updatePost
type UpdatePostPayload struct {
	// Post ID
	// in: path
	// required: true
	PostID int64 `json:"id"`
	// Update post payload
	// in: body
	Body PostUpdatePayload
}

// swagger:parameters addPostFile deletePostFile
type UpdatePostFilePayload struct {
	// Post ID
	// in: path
	// required: true
	PostID int64 `json:"post_id"`

	// File ID
	// in: path
	// required: true
	FileID int64 `json:"file_id"`
}

// swagger:parameters sharePost
type SharePostPayload struct {
	// Post ID
	// in: path
	// required: true
	PostID int64 `json:"id"`
	// Share post body
	// in: body
	Body dataType.PostShare
}

// swagger:parameters translateText
type TranslatePayload struct {
	// Translate payload
	// in: body
	Body TranslateParam
}
