package api

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strconv"

	"errors"
	"github.com/go-chi/chi"
	"github.com/sirupsen/logrus"
	"gitlab.com/rockship/backend/zeniius-backend/models"
	"gitlab.com/rockship/backend/zeniius-backend/response"
	"gitlab.com/rockship/backend/zeniius-backend/utils"
	"gopkg.in/guregu/null.v3"
)

type CommentPayload struct {
	// Comment content
	// required: true
	Content string `json:"content"`
	// Comment parent id
	ParentID int64 `json:"parent_id"`
	// Attachment id
	File int64 `json:"file_id"`
}

type UpdateCommentPayload struct {
	// Comment content
	// required: true
	Content string `json:"content"`
	// Attachment id
	File int64 `json:"file_id"`
}

//swagger:route POST /feed/{id}/comment comment createComment
//
// Create comment on post
//
// Create a comment on a post ID
// Security:
//		api_key:
// Responses:
// 		200: Comment
func (e *Env) CreateComment(w http.ResponseWriter, r *http.Request) error {

	postIdParam := chi.URLParam(r, "id")

	postId, err := strconv.ParseInt(postIdParam, 10, 64)

	if err != nil {
		return ResponseData{400, err, []byte(`{"error":"parse post id error"}`)}
	}

	b, err := ioutil.ReadAll(r.Body)

	if err != nil {
		logrus.Info(utils.GetPath(r), "Get signed url > ReadAll Error")
		return ResponseData{500, err, nil}
	}

	var commentPayload CommentPayload

	err = json.Unmarshal(b, &commentPayload)
	if err != nil {
		logrus.Info(utils.GetPath(r), "Get signed url > Unmarshal Error")
		return ResponseData{500, err, nil}
	}
	user := r.Context().Value("user").(*models.User)

	comment := models.Comment{
		PostID:  postId,
		Content: commentPayload.Content,
		UserID:  user.ID,
	}

	isValid := true
	if commentPayload.ParentID == 0 {
		isValid = false
	}
	parentID := null.NewInt(commentPayload.ParentID, isValid)
	comment.ParentID = parentID

	isValid = true
	if commentPayload.File == 0 {
		isValid = false
	}
	fileID := null.NewInt(commentPayload.File, isValid)
	comment.AttachmentID = fileID

	err = comment.Save(e.DB)

	if err != nil {
		return ResponseData{500, err, []byte(`{"error":"Save comment to DB error"}`)}
	}

	err = comment.GetDetail(e.DB)

	if err != nil {
		return ResponseData{500, err, []byte(`{"error":"Get comment detail"}`)}
	}

	return ResponseData{200, nil, response.JsonEncodeModel(comment)}

}

// Get comments of post swagger:route GET /feed/{id}/comment feed listComments
// Get all parent comments of post with its latest child comment
// Security:
//		api_key:
// Responses:
// 		200: Comment
func (e *Env) GetComment(w http.ResponseWriter, r *http.Request) error {
	postIdParam := chi.URLParam(r, "id")
	postId, err := strconv.Atoi(postIdParam)
	if err != nil {
		return ResponseData{400, err, []byte(`{"error":"parse post error"}`)}
	}

	page := r.URL.Query().Get("page")
	pageNumber, err := strconv.Atoi(page)
	if err != nil {
		pageNumber = 1
	}

	limit := models.CommentInPostLimit

	user := r.Context().Value("user").(*models.User)
	var c models.Comment
	comments, err := c.GetCommentOfPost(e.DB, int64(postId), pageNumber, limit, user.ID)

	jsonData, err := json.Marshal(comments)

	if err != nil {
		return ResponseData{500, err, []byte(`{"error":"parse json error"}`)}
	}

	return ResponseData{200, nil, jsonData}

}

// Get Children comment swagger:route GET /comment/{id}/children comment listChildrenComments
// Get children comment of a comment
// Security:
//		api_key:
// Responses:
//		200: []Comment
func (e *Env) GetChildrenComments(w http.ResponseWriter, r *http.Request) error {

	commentIDParam := chi.URLParam(r, "id")

	commentID, err := strconv.ParseInt(commentIDParam, 10, 64)

	if err != nil {
		return ResponseData{400, err, []byte(`{"error":"parse id error"}`)}
	}

	page := r.URL.Query().Get("page")

	pageNumber, err := strconv.Atoi(page)
	if err != nil {
		pageNumber = 1
	}
	limit := models.CommentInPostLimit

	user := r.Context().Value("user").(*models.User)
	var c models.Comment
	comments, err := c.GetChildrenCommentsOfComment(e.DB, commentID, pageNumber, limit, user.ID)

	jsonData, err := json.Marshal(comments)

	if err != nil {
		return ResponseData{500, err, []byte(`{"error":"parse json error"}`)}
	}

	return ResponseData{200, nil, jsonData}

}

// swagger:route DELETE /comment/{id} comment deleteComment
// Delete comment
// Security:
//		api_key:
// Responses:
//		200: DefaultResponse
func (e *Env) DeleteComments(w http.ResponseWriter, r *http.Request) error {

	commentIDParam := chi.URLParam(r, "id")

	commentID, err := strconv.ParseInt(commentIDParam, 10, 64)

	if err != nil {
		return ResponseData{400, err, []byte(`{"error":"parse id error"}`)}
	}

	user := r.Context().Value("user").(*models.User)

	err = models.DeleteComment(e.DB, commentID, user.ID)

	if err != nil {
		return ResponseData{500, err, []byte(`{"error":"delete comment error"}`)}
	}

	return ResponseData{200, nil, []byte(`{"message":"Delete comment success"}`)}

}

// swagger:route PUT /comment/{id} comment updateComment
// Update comment
// Security:
//		api_key:
// Responses:
//		200: DefaultResponse
func (e *Env) UpdateComment(w http.ResponseWriter, r *http.Request) error {

	commentIDParam := chi.URLParam(r, "id")
	commentID, err := strconv.ParseInt(commentIDParam, 10, 64)
	if err != nil {
		return ResponseData{400, err, []byte(`{"error":"parse id error"}`)}
	}
	var payload UpdateCommentPayload
	err, _ = utils.ParseRequest(r, &payload)
	if err != nil {
		return ResponseFail(400, "Parse payload error", err)
	}

	user := r.Context().Value("user").(*models.User)
	comment, err := models.GetComment(e.DB, commentID)
	if err != nil {
		return ResponseFail(404, "comment not found", err)
	}
	if comment.UserID != user.ID {
		return ResponseFail(400, "user is not owner of comment", errors.New("not owner"))
	}
	if payload.Content != "" {
		comment.Content = payload.Content
	}
	comment.AttachmentID = utils.ParseNullInt(payload.File)
	if comment.AttachmentID.Valid == true && models.CheckFileExist(e.DB, comment.AttachmentID.Int64) == false {
		return ResponseFail(404, "file not found", errors.New("not found"))
	}
	comment.Attachment = nil
	err = comment.UpdateComment(e.DB)

	comment, _ = models.GetComment(e.DB, commentID)
	if err != nil {
		return ResponseFail(500, "update comment error", err)
	}
	return ResponseData{200, nil, response.JsonEncodeModel(comment)}

}

// swagger:route POST /comment/{id}/like comment likeComment
// Like Comment
// Security:
//		api_key:
// Responses:
//		200: Default

func (e *Env) LikeComment(w http.ResponseWriter, r *http.Request) error {
	commentIdParam := chi.URLParam(r, "id")
	commentId, err := strconv.ParseInt(commentIdParam, 10, 64)
	if err != nil {
		return ResponseData{400, err, []byte(`{"error":"parse comment error"}`)}
	}

	user := r.Context().Value("user").(*models.User)
	err = models.LikeComment(e.DB, commentId, user.ID)

	if err != nil {
		if utils.IsExist(err) {
			return ResponseFail(400, "you have already liked this comment", err)
		} else {
			return ResponseFail(500, "like comment error", err)
		}
	}
	return ResponseSuccess()
}

// swagger:route DELETE /comment/{id}/like comment unlikeComment
// Unlike Comment
// Security:
//		api_key:
// Responses:
//		200: Default

func (e *Env) UnlikeComment(w http.ResponseWriter, r *http.Request) error {
	commentIdParam := chi.URLParam(r, "id")
	commentId, err := strconv.ParseInt(commentIdParam, 10, 64)
	if err != nil {
		return ResponseData{400, err, []byte(`{"error":"parse comment error"}`)}
	}

	user := r.Context().Value("user").(*models.User)
	err = models.UnLikeComment(e.DB, commentId, user.ID)

	if err != nil {
		if utils.IsNotFound(err) {
			return ResponseFail(400, "you have not liked this comment yet", err)
		} else {
			return ResponseFail(500, "unlike comment error", err)
		}
	}
	return ResponseSuccess()
}
