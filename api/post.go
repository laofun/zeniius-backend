package api

import (
	"encoding/json"
	"net/http"
	"strconv"

	"errors"
	"fmt"
	"github.com/asaskevich/govalidator"
	"github.com/go-chi/chi"
	"github.com/jinzhu/gorm/dialects/postgres"
	"gitlab.com/rockship/backend/zeniius-backend/dataType"
	"gitlab.com/rockship/backend/zeniius-backend/models"
	"gitlab.com/rockship/backend/zeniius-backend/response"
	"gitlab.com/rockship/backend/zeniius-backend/service"
	"gitlab.com/rockship/backend/zeniius-backend/utils"
	"time"
)

type ReportPostPayload struct {
	Message string `json:"message"`
}

// Get User Posts swagger:route GET /feed feed listPosts
//
// Get newfeeds
//
// Get user list of post from other people
// Security:
//		api_key:
// Responses:
// 		200: Post
func (e *Env) GetFeeds(w http.ResponseWriter, r *http.Request) error {

	page := r.URL.Query().Get("page")

	if page == "" {
		page = "1"
	}

	pageNumber, err := strconv.Atoi(page)
	limit := 5

	if err != nil {
		return ResponseData{400, err, []byte(`{"error":"parse page error"}`)}
	}

	businessIDsParam := r.URL.Query().Get("business")
	businessIDs := utils.StringParamToIntArray(businessIDsParam)

	groupIDsParam := r.URL.Query().Get("group")
	groupIDs := utils.StringParamToStringArray(groupIDsParam)

	demandParam := r.URL.Query().Get("demand")
	demand := utils.StringParamToStringArray(demandParam)
	if demand == nil {
		demand = []string{"demands", "ability", "none"}
	}

	user := r.Context().Value("user").(*models.User)
	filter := r.URL.Query().Get("filter")

	var data []models.Post

	locale, _ := r.Context().Value("locale").(string)

	data, err = models.GetPostWithFilter(e.DB, filter, user.ID, demand, businessIDs, groupIDs, pageNumber, limit, locale)

	if err != nil {
		return ResponseData{500, err, []byte(`{"error":"Internal error"}`)}
	}
	return ResponseData{200, nil, response.JsonEncodeModel(data)}
}

func (e *Env) SearchFeeds(w http.ResponseWriter, r *http.Request) error {
	return ResponseSuccess()
}

type PostPayload struct {
	// required: true
	Title    string  `json:"title" valid:"required, length(1|300)"`
	Business []int64 `json:"business"`
	// required: true
	//enum: opportunity,discussion,event
	Type string `json:"type" valid:"required, in(opportunity|discussion|event), post_type"`
	// required: true
	Description  string   `json:"description" valid:"required"`
	Location     string   `json:"location"`
	LocationLat  *float64 `json:"location_lat" valid:"latitude"`
	LocationLong *float64 `json:"location_long" valid:"longitude"`
	// enum: demands,ability
	DemandType     string               `json:"demand_type" valid:"in(demands|ability|none), required"`
	EndTime        *time.Time           `json:"end_time"`
	StartTime      *time.Time           `json:"start_time"`
	Privacy        dataType.PostPrivacy `json:"privacy" valid:"required, post_privacy"`
	Cost           *PostCost            `json:"cost"`
	Files          []int64              `json:"files"`
	EnableRegister bool                 `json:"enable_register"`
}

type PostCost struct {
	Description string `json:"description"`
	// enum: free,negotiation,range
	Type       string   `json:"type" valid:"in(free|negotiation|range)"`
	IsEstimate bool     `json:"is_estimate"`
	CostFrom   *float64 `json:"cost_from" valid:"post_cost"`
	CostTo     *float64 `json:"cost_to"`
}

// swagger:route POST /post post createPost
// Create Post
// Security:
//		api_key:
// Responses:
//		200: Default
func (e *Env) CreatePost(w http.ResponseWriter, r *http.Request) error {
	var payload PostPayload
	err, message := utils.ParseRequest(r, &payload)
	if err != nil {
		return ResponseFail(400, message, err)
	}

	result, err := govalidator.ValidateStruct(payload)
	if !result {
		return ResponseFail(400, err.Error(), err)
	}

	user := r.Context().Value("user").(*models.User)
	postFieldsMap := map[string]interface{}{
		"cost":            payload.Cost,
		"enable_register": payload.EnableRegister,
	}
	postFields, err := json.Marshal(postFieldsMap)

	if err != nil {
		return ResponseFail(400, "parse cost error", err)
	}

	if len(payload.Files) > models.PostFileLimit {
		return ResponseFail(400, fmt.Sprintf("feature images limit is %d", models.PostFileLimit), errors.New("pass file limit"))
	}

	locale := r.Context().Value("locale").(string)
	textLocale := service.DetectLanguage(payload.Description)

	p := models.Post{
		UserId:      user.ID,
		Title:       payload.Title,
		Description: payload.Description,
		Locale:      textLocale,
		Type:        payload.Type,
		Lat:         utils.ParseNullFloat(payload.LocationLat),
		Long:        utils.ParseNullFloat(payload.LocationLong),
		DemandType:  payload.DemandType,
		EndTime:     payload.EndTime,
		StartTime:   payload.StartTime,
		Fields:      postgres.Jsonb{postFields},
	}
	err = p.Create(e.DB, payload.Business, payload.Files, payload.Privacy)
	if err != nil {
		return ResponseFail(500, "create post eror", err)
	}
	go p.SaveTranslate(e.DB, locale)
	return ResponseData{200, nil, response.JsonEncodeModel(p)}

}

type PostUpdatePayload struct {
	PostPayload
	Status string `json:"status" valid:"in(created|urgent|satisfied|canceled)"`
}

// swagger:route PUT /post/{id} post updatePost
// Update Post
// Security:
//		api_key:
// Responses:
//		200: Default

func (e *Env) UpdatePost(w http.ResponseWriter, r *http.Request) error {
	postId, err := strconv.ParseInt(chi.URLParam(r, "id"), 10, 64)
	if err != nil {
		return ResponseFail(400, "parse post id error", err)
	}
	var payload PostUpdatePayload
	err, message := utils.ParseRequest(r, &payload)
	if err != nil {
		return ResponseFail(400, message, err)
	}
	result, err := govalidator.ValidateStruct(payload)

	if !result {
		return ResponseFail(400, err.Error(), err)
	}

	post, err := models.GetPost(e.DB, postId)

	if err != nil {
		return ResponseFail(404, "post not found", err)
	}

	user := r.Context().Value("user").(*models.User)
	if post.UserId != user.ID {
		return ResponseFail(400, "you can not edit this post", errors.New("unauthorized"))
	}

	post.Status = payload.Status
	post.Type = payload.Type
	post.Title = payload.Title
	post.DemandType = payload.DemandType
	post.Description = payload.Description
	post.Location = payload.Location
	post.Long = utils.ParseNullFloat(payload.LocationLong)
	post.Lat = utils.ParseNullFloat(payload.LocationLat)
	post.StartTime = payload.StartTime
	post.EndTime = payload.EndTime
	postFieldsMap := map[string]interface{}{
		"cost":            payload.Cost,
		"enable_register": payload.EnableRegister,
	}
	postFields, err := json.Marshal(postFieldsMap)
	post.Fields = postgres.Jsonb{postFields}
	err = post.UpdatePost(e.DB, payload.Business, payload.Privacy)

	if err != nil {
		return ResponseFail(500, "update post error", err)
	}
	locale, _ := r.Context().Value("locale").(string)
	post, err = models.GetPostDetail(e.DB, postId, user.ID, locale)

	if err != nil {
		return ResponseFail(500, "update post error", err)
	}

	return ResponseData{200, nil, response.JsonEncodeModel(post)}
}

// swagger:route POST /post/{post_id}/file/{file_id} post addPostFile
// Add post file
// Security:
//		api_key:
// Responses:
//		200: Default

func (e *Env) AddPostFile(w http.ResponseWriter, r *http.Request) error {
	postId, err := strconv.ParseInt(chi.URLParam(r, "post_id"), 10, 64)
	if err != nil {
		return ResponseFail(400, "parse post id error", err)
	}

	fileID, err := strconv.ParseInt(chi.URLParam(r, "file_id"), 10, 64)
	if err != nil {
		return ResponseFail(400, "parse file id error", err)
	}

	post, err := models.GetPost(e.DB, postId)

	if err != nil {
		return ResponseFail(404, "post not found", err)
	}

	user := r.Context().Value("user").(*models.User)
	if post.UserId != user.ID {
		return ResponseFail(400, "you can not edit this post", errors.New("unauthorized"))
	}

	count, _ := post.CountPostFile(e.DB)

	if count >= models.PostFileLimit {
		return ResponseFail(400, fmt.Sprintf("feature images limit is %d", models.PostFileLimit), errors.New("pass file limit"))
	}

	err = post.AddPostFile(e.DB, fileID)

	if utils.IsNotFound(err) {
		return ResponseFail(404, "file not found", err)
	}

	if utils.IsExist(err) {
		return ResponseFail(400, "file existed", err)
	}

	if err != nil {
		return ResponseFail(500, "add post file error", err)
	}
	return ResponseSuccess()
}

// swagger:route DELETE /post/{post_id}/file/{file_id} post deletePostFile
// Delete post file
// Security:
//		api_key:
// Responses:
//		200: Default

func (e *Env) DeletePostFile(w http.ResponseWriter, r *http.Request) error {
	postId, err := strconv.ParseInt(chi.URLParam(r, "post_id"), 10, 64)
	if err != nil {
		return ResponseFail(400, "parse post id error", err)
	}

	fileID, err := strconv.ParseInt(chi.URLParam(r, "file_id"), 10, 64)
	if err != nil {
		return ResponseFail(400, "parse file id error", err)
	}

	post, err := models.GetPost(e.DB, postId)

	if err != nil {
		return ResponseFail(404, "post not found", err)
	}

	user := r.Context().Value("user").(*models.User)
	if post.UserId != user.ID {
		return ResponseFail(400, "you can not edit this post", errors.New("unauthorized"))
	}

	err = post.DeletePostFile(e.DB, fileID)

	if utils.IsNotFound(err) {
		return ResponseFail(404, "file not found", err)
	}

	if err != nil {
		return ResponseFail(500, "delete post file error", err)
	}
	return ResponseSuccess()
}

// swagger:route POST /post/{id}/report post reportPost
// Report Post
// Security:
//		api_key:
// Responses:
//		200: Default

func (e *Env) ReportPost(w http.ResponseWriter, r *http.Request) error {
	postIdParam := chi.URLParam(r, "id")
	postId, err := strconv.ParseInt(postIdParam, 10, 64)
	if err != nil {
		return ResponseData{400, err, []byte(`{"error":"parse post error"}`)}
	}

	var payload ReportPostPayload
	err, _ = utils.ParseRequest(r, &payload)
	if err != nil {
		return ResponseFail(400, "Parse payload error", err)
	}

	user := r.Context().Value("user").(*models.User)
	err = models.ReportPost(e.DB, postId, user.ID, payload.Message)

	if err != nil {
		if utils.IsExist(err) {
			return ResponseFail(400, "you have already reported this post", err)
		} else {
			return ResponseFail(500, "Report post error", err)
		}
	}
	return ResponseSuccess()
}

// swagger:route POST /post/{id}/like post likePost
// Like Post
// Security:
//		api_key:
// Responses:
//		200: Default

func (e *Env) LikePost(w http.ResponseWriter, r *http.Request) error {
	postIdParam := chi.URLParam(r, "id")
	postId, err := strconv.ParseInt(postIdParam, 10, 64)
	if err != nil {
		return ResponseData{400, err, []byte(`{"error":"parse post error"}`)}
	}

	user := r.Context().Value("user").(*models.User)
	err = models.LikePost(e.DB, postId, user.ID)

	if err != nil {
		if utils.IsExist(err) {
			return ResponseFail(400, "you have already liked this post", err)
		} else {
			return ResponseFail(500, "like post error", err)
		}
	}
	return ResponseSuccess()
}

// swagger:route DELETE /post/{id}/like post unlikePost
// Unlike Post
// Security:
//		api_key:
// Responses:
//		200: Default

func (e *Env) UnlikePost(w http.ResponseWriter, r *http.Request) error {
	postIdParam := chi.URLParam(r, "id")
	postId, err := strconv.ParseInt(postIdParam, 10, 64)
	if err != nil {
		return ResponseData{400, err, []byte(`{"error":"parse post error"}`)}
	}

	user := r.Context().Value("user").(*models.User)
	err = models.UnLikePost(e.DB, postId, user.ID)

	if err != nil {
		if utils.IsNotFound(err) {
			return ResponseFail(400, "you have not liked this post yet", err)
		} else {
			return ResponseFail(500, "unlike post error", err)
		}
	}
	return ResponseSuccess()
}

// swagger:route POST /post/{id}/register post registerPost
// Register Post
// Security:
//		api_key:
// Responses:
//		200: Default

func (e *Env) RegisterPost(w http.ResponseWriter, r *http.Request) error {
	postIdParam := chi.URLParam(r, "id")
	postId, err := strconv.ParseInt(postIdParam, 10, 64)
	if err != nil {
		return ResponseData{400, err, []byte(`{"error":"parse post error"}`)}
	}

	user := r.Context().Value("user").(*models.User)
	err = models.RegisterPost(e.DB, postId, user.ID)

	if err != nil {
		if utils.IsExist(err) {
			return ResponseFail(400, "you have already registerd this post", err)
		} else {
			return ResponseFail(500, "register post error", err)
		}
	}
	return ResponseSuccess()
}

// swagger:route DELETE /post/{id}/register post unregisterPost
// Unregister Post
// Security:
//		api_key:
// Responses:
//		200: Default

func (e *Env) UnregisterPost(w http.ResponseWriter, r *http.Request) error {
	postIdParam := chi.URLParam(r, "id")
	postId, err := strconv.ParseInt(postIdParam, 10, 64)
	if err != nil {
		return ResponseData{400, err, []byte(`{"error":"parse post error"}`)}
	}

	user := r.Context().Value("user").(*models.User)
	err = models.UnRegisterPost(e.DB, postId, user.ID)

	if err != nil {
		if utils.IsNotFound(err) {
			return ResponseFail(400, "you have not registerd this post yet", err)
		} else {
			return ResponseFail(500, "unregister post error", err)
		}
	}
	return ResponseSuccess()
}

// swagger:route GET /post/{id}/like post getLikePostUser
// Unlike Post
// Security:
//		api_key:
// Responses:
//		200: Default

func (e *Env) GetUserLikePost(w http.ResponseWriter, r *http.Request) error {
	postId, err := strconv.ParseInt(chi.URLParam(r, "id"), 10, 64)
	if err != nil {
		return ResponseData{400, err, []byte(`{"error":"parse post error"}`)}
	}

	post, err := models.GetPost(e.DB, postId)
	if err != nil {
		return ResponseFail(404, "post not found", err)
	}

	users, err := post.ListLikedUser(e.DB)

	if err != nil {
		return ResponseFail(500, "get liked user error", err)
	}

	return ResponseData{200, nil, response.JsonEncodeModel(users)}
}

// swagger:route GET /post/{id}/register post getRegisterPostUser
// Unlike Post
// Security:
//		api_key:
// Responses:
//		200: Default

func (e *Env) GetUserRegisterPost(w http.ResponseWriter, r *http.Request) error {
	postId, err := strconv.ParseInt(chi.URLParam(r, "id"), 10, 64)
	if err != nil {
		return ResponseData{400, err, []byte(`{"error":"parse post error"}`)}
	}

	post, err := models.GetPost(e.DB, postId)
	if err != nil {
		return ResponseFail(404, "post not found", err)
	}

	users, err := post.ListRegisteredUser(e.DB)

	if err != nil {
		return ResponseFail(500, "get registered user error", err)
	}

	return ResponseData{200, nil, response.JsonEncodeModel(users)}
}

// swagger:route GET /post/{id} post getPostDetail
// Get Post Detail
// Security:
//		api_key:
// Responses:
//		200: Default

func (e *Env) GetPostDetail(w http.ResponseWriter, r *http.Request) error {
	postId, err := strconv.ParseInt(chi.URLParam(r, "id"), 10, 64)
	if err != nil {
		return ResponseData{400, err, []byte(`{"error":"parse post error"}`)}
	}
	user := r.Context().Value("user").(*models.User)
	if !models.CheckPostPrivacy(e.DB, postId, *user) {
		return ResponseFail(403, "you can not view this post", errors.New("unauthorized"))
	}
	locale, _ := r.Context().Value("locale").(string)
	post, err := models.GetPostDetail(e.DB, postId, user.ID, locale)
	if utils.IsNotFound(err) {
		return ResponseFail(404, "post not found", err)
	}
	if err != nil {
		return ResponseFail(500, "get post detail error", err)
	}

	return ResponseData{200, nil, response.JsonEncodeModel(post)}
}

// swagger:route Delete /post/{id} post deletePost
// Delete Post
// Security:
//		api_key:
// Responses:
//		200: Default

func (e *Env) DeletePost(w http.ResponseWriter, r *http.Request) error {
	postId, err := strconv.ParseInt(chi.URLParam(r, "id"), 10, 64)
	if err != nil {
		return ResponseData{400, err, []byte(`{"error":"parse post error"}`)}
	}
	user := r.Context().Value("user").(*models.User)
	post, err := models.GetPost(e.DB, postId)

	if err != nil {
		return ResponseFail(404, "post not found", err)
	}

	if post.UserId != user.ID {
		return ResponseFail(403, "you can not delete this post", err)
	}

	err = post.Delete(e.DB)

	if err != nil {
		return ResponseFail(500, "delete post error", err)
	}
	return ResponseSuccess()
}

// swagger:route Post /post/{id}/share post sharePost
// Share Post
// Security:
//		api_key:
// Responses:
//		200: SharePost

func (e *Env) SharePost(w http.ResponseWriter, r *http.Request) error {
	postId, err := strconv.ParseInt(chi.URLParam(r, "id"), 10, 64)
	if err != nil {
		return ResponseData{400, err, []byte(`{"error":"parse post error"}`)}
	}
	var payload dataType.PostShare
	err, message := utils.ParseRequest(r, &payload)
	if err != nil {
		return ResponseFail(400, message, err)
	}
	user := r.Context().Value("user").(*models.User)
	post, err := models.GetPost(e.DB, postId)
	if err != nil {
		return ResponseFail(404, "post not found", err)
	}
	successGroups, errorGroups, err := post.Share(e.DB, *user, payload.ShareGroup)
	if err != nil {
		return ResponseFail(500, "share post error", err)
	}

	result := map[string]interface{}{
		"success_groups": successGroups,
		"error_groups":   errorGroups,
	}

	return ResponseData{200, nil, response.JsonEncodeModel(result)}
}
