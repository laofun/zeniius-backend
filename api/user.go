package api

import (
	"encoding/json"
	"net/http"
	"strconv"
	"time"

	"github.com/asaskevich/govalidator"
	"github.com/go-chi/chi"
	"gitlab.com/rockship/backend/zeniius-backend/models"
	"gitlab.com/rockship/backend/zeniius-backend/response"
	"gitlab.com/rockship/backend/zeniius-backend/utils"
)

// GetAllUser ..
// swagger:route GET /user user listUser
//
// Get all user
//
// Get list of all user in  app
// Security:
//		api_key:
// Responses:
// 		200: []User
func (e *Env) GetAllUser(w http.ResponseWriter, r *http.Request) error {
	listUserProfile, err := models.GetAllUser(e.DB)
	if err != nil {
		return ResponseData{500, err, []byte(`{"message":"Error Get User Profiles"}`)}
	}

	for key, _ := range listUserProfile {
		listUserProfile[key].GetExtraUserData(e.DB)
	}

	jsonData, err := json.Marshal(listUserProfile)
	if err != nil {
		return ResponseData{500, err, []byte(`{"error":"parse json error"}`)}
	}
	return ResponseData{200, nil, jsonData}
}

// GetUserFollower ..
// swagger:route GET /user/me/follower user listUserFollower
//
// Get all user
//
// Get list of all user in  app
// Security:
//		api_key:
// Responses:
// 		200: []User
func (e *Env) GetUserFollower(w http.ResponseWriter, r *http.Request) error {
	listUserProfile, err := models.GetMultiUserByID(e.DB, nil)
	if err != nil {
		return ResponseData{500, err, []byte(`{"message":"Error Get User Profiles"}`)}
	}

	jsonData, err := json.Marshal(listUserProfile)
	if err != nil {
		return ResponseData{500, err, []byte(`{"error":"parse json error"}`)}
	}
	return ResponseData{200, nil, jsonData}
}

// SearchUser ..
// swagger:route GET /user/search user searchUser
//
// Search user
//
// Search all CEO by name, email, company info , ability , post ...
// Security:
//		api_key:
// Responses:
// 		200: []User
func (e *Env) SearchUser(w http.ResponseWriter, r *http.Request) error {
	query := r.URL.Query().Get("q")
	page := r.URL.Query().Get("page")
	pageNumber, err := strconv.Atoi(page)
	if err != nil {
		pageNumber = 1
	}

	users, err := models.SearchUser(e.DB, query, pageNumber)
	if err != nil {
		return ResponseData{500, err, []byte(`{"error":"Search user error"}`)}
	}
	return ResponseData{200, nil, response.JsonEncodeModel(users)}
}

// GetCurrentUser ..
// swagger:route GET /user/me user currentUser
//
// Get current user profile
//
// Get current user log in profile
//
// Security:
//		api_key:
// Responses:
// 		200: User
func (e *Env) GetCurrentUser(w http.ResponseWriter, r *http.Request) error {
	var u *models.User

	u = r.Context().Value("user").(*models.User)
	err := u.GetDetailUser(e.DB)
	if err != nil {
		return ResponseData{400, err, []byte(`{"error":"Get user profile error"}`)}
	}

	return ResponseData{200, nil, response.JsonEncodeModel(u)}
}

// GetUserProfile ..
// swagger:route GET /user/{id} user getUser
//
// Get user profile of CEO
// Get CEO profile
//
// Security:
//		api_key:
// Responses:
// 		200: UserExtend
func (e *Env) GetUserProfile(w http.ResponseWriter, r *http.Request) error {
	user := r.Context().Value("user").(*models.User)

	idParam := chi.URLParam(r, "id")
	id, err := strconv.ParseInt(idParam, 10, 64)
	if err != nil {
		return ResponseData{400, err, []byte(`{"error":"parse id error"}`)}
	}

	userExtendInfo, err := models.GetUserRelationshipByID(e.DB, id, user.ID)
	if err != nil {
		if err.Error() == "record not found" {
			return ResponseData{404, err, []byte(`{}`)}
		}
		return ResponseData{500, err, []byte(`{"error":"Get user profile error"}`)}
	}
	return ResponseData{200, nil, response.JsonEncodeModel(userExtendInfo)}
}

// UpdateUserPersonalPayload ..
type UpdateUserPersonalPayload struct {
	Fullname    string     `json:"fullname"`
	Description string     `json:"description"`
	Birthday    *time.Time `json:"birthday"`
}

// UpdateUserPersonalInformation ..
// swagger:route PUT /user/me/personal user updateUserPersonal
//
// Update User personal information
//
// Update User personal information : fullname, description , birthday
//
// Security:
//		api_key:
// Responses:
// 		200: User
func (e *Env) UpdateUserPersonalInformation(w http.ResponseWriter, r *http.Request) error {
	var payload UpdateUserPersonalPayload
	err, message := utils.ParseRequest(r, &payload)
	if err != nil {
		return ResponseData{403, err, []byte(message)}
	}

	user := r.Context().Value("user").(*models.User)
	userProfile, err := models.GetUserByID(e.DB, user.ID)

	if err != nil {
		return ResponseData{403, err, []byte(message)}
	}

	if payload.Fullname != "" {
		userProfile.FullName = payload.Fullname
	}

	if payload.Description != "" {
		userProfile.Description = payload.Description
	}

	if payload.Birthday != nil {
		userProfile.Birthday = payload.Birthday
	}

	err = userProfile.UpdateUser(e.DB)
	if err != nil {
		return ResponseData{500, err, []byte(`{"message": "Update user error"}`)}
	}
	go models.RefreshUserSearchView(e.DB)
	return ResponseData{200, nil, response.JsonEncodeModel(userProfile)}
}

// UpdateUserContactPayload ..
type UpdateUserContactPayload struct {
	Phone    string `json:"phone_number" valid:"numeric"`
	Email    string `json:"email" valid:"email,optional"`
	Facebook string `json:"facebook_link" valid:"url,optional"`
}

// UpdateUserContact ..
// swagger:route PUT /user/me/contact user updateUserContact
//
// Update User contact
//
// Update User contact : phone, email, facebook link
//
// Security:
//		api_key:
// Responses:
// 		200: User
func (e *Env) UpdateUserContact(w http.ResponseWriter, r *http.Request) error {
	var payload UpdateUserContactPayload
	err, message := utils.ParseRequest(r, &payload)
	if err != nil {
		return ResponseData{403, err, []byte(message)}
	}

	_, err = govalidator.ValidateStruct(payload)

	if err != nil {
		return ResponseData{403, err, response.JsonEncodeModel(err)}
	}

	user := r.Context().Value("user").(*models.User)
	userProfile, err := models.GetUserByID(e.DB, user.ID)
	if err != nil {
		return ResponseData{403, err, []byte(message)}
	}

	if payload.Phone != "" {
		userProfile.Phone = payload.Phone
	}

	if payload.Email != "" {
		userProfile.Email = payload.Email
	}

	if payload.Facebook != "" {
		userProfile.Facebook = payload.Facebook
	}

	err = userProfile.UpdateUser(e.DB)
	if err != nil {
		return ResponseData{500, err, []byte(`{"message": "Update user error"}`)}
	}
	go models.RefreshUserSearchView(e.DB)
	return ResponseData{200, nil, response.JsonEncodeModel(userProfile)}
}

// UpdateUserImage ..
type UpdateUserImage struct {
	// File ID
	// require: true
	File int64 `json:"file"`
}

// UpdateUserAvatar ..
// swagger:route PUT /user/me/avatar user updateUserAvatar
//
// Update User avatar
//
// Update User avatar
//
// Security:
//		api_key:
// Responses:
// 		200: string
func (e *Env) UpdateUserAvatar(w http.ResponseWriter, r *http.Request) error {
	var payload UpdateUserImage
	err, message := utils.ParseRequest(r, &payload)

	if err != nil {
		return ResponseData{403, err, []byte(message)}
	}

	if models.CheckFileExist(e.DB, payload.File) == false {
		return ResponseData{404, err, []byte(`{"message": "File not found"}`)}
	}

	user := r.Context().Value("user").(*models.User)
	err = user.UpdateUserFields(e.DB, map[string]interface{}{"profile_pic": payload.File})
	if err != nil {
		return ResponseData{500, err, []byte(`{"message": "Update user error"}`)}
	}
	return ResponseData{200, nil, []byte(`{"message": "Update user avatar success"}`)}

}

// UpdateUserCover ..
// swagger:route PUT /user/me/cover user updateUserCover
//
// Update User cover
//
// Update User cover
//
// Security:
//		api_key:
// Responses:
// 		200: string
func (e *Env) UpdateUserCover(w http.ResponseWriter, r *http.Request) error {
	var payload UpdateUserImage
	err, message := utils.ParseRequest(r, &payload)
	if err != nil {
		return ResponseData{403, err, []byte(message)}
	}

	if models.CheckFileExist(e.DB, payload.File) == false {
		return ResponseData{404, err, []byte(`{"message": "File not found"}`)}
	}

	user := r.Context().Value("user").(*models.User)
	err = user.UpdateUserFields(e.DB, map[string]interface{}{"cover_pic": payload.File})
	if err != nil {
		return ResponseData{500, err, []byte(`{"message": "Update user error"}`)}
	}
	return ResponseData{200, nil, []byte(`{"message": "Update user cover success"}`)}
}

// UserInfoPayload ..
type UserInfoPayload struct {
	UpdateUserPersonalPayload
	UpdateUserContactPayload
}

type UserVerifyPayload struct {
	CompleteInfo bool `json:"complete_info"`
	Verified     bool `json:"verified"`
}

// swagger:route PUT /user/{id}/verification user updateUserVerification
//
// Update user verification
//
// Update user verification
//
// Security:
//		api_key:
// Responses:
// 		200: string
func (e *Env) VerifyUser(w http.ResponseWriter, r *http.Request) error {
	idParam := chi.URLParam(r, "id")
	id, err := strconv.ParseInt(idParam, 10, 64)
	if err != nil {
		return ResponseFail(400, "parse id error", err)
	}

	var payload UserVerifyPayload
	err, _ = utils.ParseRequest(r, &payload)

	if err != nil {
		return ResponseFail(400, "parse payload error", err)
	}

	fields := map[string]interface{}{
		"complete_info": payload.CompleteInfo,
		"is_verify":     payload.Verified,
	}

	user := models.User{
		ID: id,
	}
	err = user.UpdateUserFields(e.DB, fields)

	if err != nil {
		return ResponseFail(500, "update user error", err)
	}

	user, _ = models.GetUserByID(e.DB, id)
	go models.RefreshUserSearchView(e.DB)
	return ResponseData{200, nil, response.JsonEncodeModel(user)}

}
