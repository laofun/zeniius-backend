package api

import (
	"encoding/json"
)

// ResponseSuccess ..
func ResponseSuccess() ResponseData {
	return ResponseData{
		Code:    200,
		Err:     nil,
		Results: []byte(`{"success":true}`),
	}
}

// ResponseFail ..
func ResponseFail(statusCode int, message string, err error) ResponseData {
	res := SimpleResponse{
		Success: false,
		Message: message,
	}

	bPayload, _ := json.Marshal(res)

	return ResponseData{
		Code:    statusCode,
		Err:     err,
		Results: bPayload,
	}
}
