package api

import (
	"log"
	"net/http"
	//"github.com/davecgh/go-spew/spew"
)

// ErrorInterface interface for StatusError
type ResponseInterface interface {
	error
	Status() int
	Data() []byte
}

// ResponseData with a custom error and http status code
type ResponseData struct {
	Code    int
	Err     error
	Results []byte
}

// Error for satisfy error interface
func (rd ResponseData) Error() string {
	return rd.Err.Error()
}

// Status return http status code
func (rd ResponseData) Status() int {
	return rd.Code
}

func (rd ResponseData) Data() []byte {
	return rd.Results
}

type responseError struct {
	Message string
}

// HandleAPI get handler with custom return error
type HandleAPI func(w http.ResponseWriter, r *http.Request) error

// Reg return http.HandleFunc satisfy interface
func (h HandleAPI) Reg(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("content-type", "application/json")
	err := h(w, r)
	if err != nil {
		switch e := err.(type) {
		case ResponseInterface:
			switch e.Status() {
			case 400, 401, 403, 404:
				log.Printf("HTTP %d - %s", e.Status(), e)
				w.WriteHeader(e.Status())
				w.Write(e.Data())

			case 500:
				log.Printf("HTTP %d - %s", e.Status(), e)
				w.WriteHeader(e.Status())
				w.Write(e.Data())

			case 200:
				w.Write(e.Data())
			default:
				http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
			}
		default:
			http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		}
	}
}
