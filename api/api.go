package api

import (
	"github.com/jinzhu/gorm"
	"github.com/sirupsen/logrus"
)

// Env ..
type Env struct {
	DB  *gorm.DB
	Log *logrus.Entry
}
