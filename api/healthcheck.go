package api

import "net/http"

// PingResponse ..
// swagger:response PingResponse
type PingResponse struct {
	// in: body
	Body struct {
		// Ping | Pong
		Success bool `json:"sucess"`
	}
}

// Ping swagger:route GET / Ping
// Ping check server running
// Responses:
//		200: PingResponse
func (e *Env) Ping(w http.ResponseWriter, r *http.Request) error {
	return ResponseData{200, nil, []byte(`{"success":true}`)}
}
