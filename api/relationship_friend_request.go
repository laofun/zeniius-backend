package api

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strconv"

	"github.com/go-chi/chi"
	"gitlab.com/rockship/backend/zeniius-backend/models"
)

// GetAllFriendRequest ..
// swagger:route GET /friends/request relationship_friend GetAllFriendRequest
// Get all friend request
//
// Get all friend requestof this user
// Security:
//		api_key:
// Responses:
// 		200: UserList
func (e *Env) GetAllFriendRequest(w http.ResponseWriter, r *http.Request) error {
	user := r.Context().Value("user").(*models.User)

	fRequests, errString, err := user.ListReceivedFriendRequest(e.DB)
	if err != nil {
		return ResponseFail(400, errString, err)
	}

	friends, err := models.GetMultiUserByID(e.DB, fRequests)
	if err != nil {
		return ResponseFail(400, "", err)
	}

	jsonData, err := json.Marshal(friends)
	if err != nil {
		return ResponseFail(400, "", err)
	}
	return ResponseData{200, nil, jsonData}
}

// AddNewFriendRequestParam ..
// swagger:parameters AddNewFriendRequest
type AddNewFriendRequestParam struct {
	// ID of Friend
	//
	// in: body
	// required: true
	FriendID int64 `json:"friend_id"`
}

// AddNewFriendRequest add new friend request
// swagger:route POST /friends/request relationship_friend AddNewFriendRequest
// Add new friend request
//
// Add new friend request
// Security:
//		api_key:
// Responses:
// 		200: SimpleResponse
func (e *Env) AddNewFriendRequest(w http.ResponseWriter, r *http.Request) error {
	user := r.Context().Value("user").(*models.User)

	bPayload, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return ResponseFail(400, "JSON Wrong format", err)
	}

	param := AddNewFriendRequestParam{}
	err = json.Unmarshal(bPayload, &param)
	if err != nil {
		return ResponseFail(400, "JSON Wrong format", err)
	}

	_, errString, err := user.MakeFriendRequest(e.DB, param.FriendID)
	if err != nil {
		return ResponseFail(400, errString, err)
	}
	return ResponseSuccess()
}

// AcceptFriendRequestParam ..
// swagger:parameters AcceptFriendRequest
type AcceptFriendRequestParam struct {
	// ID of the request
	//
	// in: body
	// required: true
	FriendID int64 `json:"friend_id"`
}

// AcceptFriendRequest accept a friend request
// swagger:route PUT /friends/request relationship_friend AcceptFriendRequest
// Accept a friend request
//
// Accept a friend request
// Security:
//		api_key:
// Responses:
// 		200: SimpleResponse
func (e *Env) AcceptFriendRequest(w http.ResponseWriter, r *http.Request) error {
	user := r.Context().Value("user").(*models.User)

	bPayload, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return ResponseFail(400, "JSON Wrong format", err)
	}

	param := AcceptFriendRequestParam{}
	err = json.Unmarshal(bPayload, &param)
	if err != nil {
		return ResponseFail(400, "JSON Wrong format", err)
	}

	_, errString, err := user.AcceptFriendRequest(e.DB, param.FriendID)
	if err != nil {
		return ResponseFail(400, errString, err)
	}
	return ResponseSuccess()

}

// RemoveFriendRequestParam ..
// swagger:parameters RemoveFriendRequest
type RemoveFriendRequestParam struct {
	// ID of the request
	// in: path
	// required: true
	FriendID int64 `json:"friend_id"`
}

// RemoveFriendRequest decline a friend request
// swagger:route DELETE /friends/request/{friendID} relationship_friend RemoveFriendRequest
// Decline a friend request
//
// Decline a friend request
// Security:
//		api_key:
// Responses:
// 		200: SimpleResponse
func (e *Env) RemoveFriendRequest(w http.ResponseWriter, r *http.Request) error {
	user := r.Context().Value("user").(*models.User)

	requestIDParam := chi.URLParam(r, "friendID")
	requestID, err := strconv.Atoi(requestIDParam)
	if err != nil {
		return ResponseFail(400, "A valid request id is required.", nil)
	}

	_, errString, err := user.DeclineFriendRequest(e.DB, int64(requestID))
	if err != nil {
		return ResponseFail(400, errString, err)
	}
	return ResponseSuccess()
}
