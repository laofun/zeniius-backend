package api

import (
	"encoding/json"
	"net/http"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/rockship/backend/zeniius-backend/models"
	"gitlab.com/rockship/backend/zeniius-backend/service"
	"gitlab.com/rockship/backend/zeniius-backend/utils"
)

type LoginFirebase struct {
	// required: true
	Token string `json:"firebase_token"`
	// android | ios
	Platform string `json:"platform"`
}

// LoginWithFirebaseToken ..
// swagger:route POST /login authentication FirebaseToken
//
// Login with firebase
//
// Sending firebase jwt token after authenticate with firebase to receive access token
// Responses:
// 		403: GenericError
//      200: AccessToken
func (e *Env) LoginWithFirebaseToken(w http.ResponseWriter, r *http.Request) error {
	var payload LoginFirebase
	err, message := utils.ParseRequest(r, &payload)
	if err != nil {
		return ResponseData{403, err, []byte(message)}
	}

	verifiedToken, err := service.VerifyIDToken(payload.Token)
	if err != nil {
		return ResponseData{403, err, []byte(`{"error":"Token not valid"}`)}
	}

	id := verifiedToken.UID
	var user *models.User
	email, _ := verifiedToken.Claims["email"].(string)

	user, err = models.GetUserByFirebaseID(e.DB, id, email)
	if err != nil {
		logrus.Error("Get user profile with firebase id: ", err)
		return ResponseData{403, err, []byte(`{"error":"Get user info error"}`)}
	}

	now := time.Now()
	accessToken := models.AccessToken{
		Platform:  payload.Platform,
		Token:     models.RandString(80),
		IPAddress: r.RemoteAddr,
		ExpiredAt: now.Add(time.Second * models.AccessTokenDuration),
		ExpiredIn: models.AccessTokenDuration,
		UserID:    user.ID,
	}
	err = accessToken.Save(e.DB)
	accessToken.User = user
	if err != nil {
		return ResponseData{500, err, []byte(`{"error":"Internal server error"}`)}
	}

	jsonData, err := json.Marshal(accessToken)
	if err != nil {
		return ResponseData{500, err, []byte(`{"error":"parse json error"}`)}
	}
	return ResponseData{200, nil, jsonData}

}
