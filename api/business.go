package api

import (
	"net/http"

	"github.com/go-chi/chi"
	"github.com/sirupsen/logrus"
	"gitlab.com/rockship/backend/zeniius-backend/models"
	"gitlab.com/rockship/backend/zeniius-backend/response"
	"gitlab.com/rockship/backend/zeniius-backend/utils"
	"strconv"
)

type UserBusinessPayload struct {
	Business []int64 `json:"business_ids"`
}

type CompanyBusinessPayload struct {
	Business    int64  `json:"business"`
	Description string `json:"description"`
}

// Ping swagger:route GET /business business getBusiness
//
// Get all business area
// Security:
//		api_key:
// Responses:
//		200: Business
func (e *Env) GetListBusiness(w http.ResponseWriter, r *http.Request) error {
	var b models.Business
	var listBusiness []models.Business
	listBusiness, err := b.GetList(e.DB)
	if err != nil {
		return ResponseData{500, err, []byte(`{"error":"List business error"}`)}
	}
	return ResponseData{200, nil, response.JsonEncodeModel(listBusiness)}

}

// Ping swagger:route POST /user/me/business business addUserBusiness
//
// Add business to user profile
// Security:
//		api_key:
// Responses:
//		200: Default
func (e *Env) AddUserBusiness(w http.ResponseWriter, r *http.Request) error {
	var payload UserBusinessPayload
	err, message := utils.ParseRequest(r, &payload)

	if err != nil {
		return ResponseData{403, err, []byte(message)}
	}

	var u *models.User

	u = r.Context().Value("user").(*models.User)

	err = models.AddUserBusinessList(e.DB, u.ID, payload.Business)

	if err != nil {
		return ResponseData{500, err, []byte(`{"message": "save business error"}`)}
	}
	go models.RefreshUserSearchView(e.DB)
	return ResponseData{200, nil, []byte(`{"message": "save business success"}`)}
}

// swagger:route POST /user/me/company/{id}/business business addCompanyBusiness
//
// Add business to company profile
// Security:
//		api_key:
// Responses:
//		200: []CompanyBusiness
func (e *Env) AddCompanyBusiness(w http.ResponseWriter, r *http.Request) error {
	var businessPayload []CompanyBusinessPayload
	var companyBusiness []models.CompanyBusiness
	var createdID []int64
	err, _ := utils.ParseRequest(r, &businessPayload)

	idParam := chi.URLParam(r, "id")
	companyID, err := strconv.ParseInt(idParam, 10, 64)

	if err != nil {
		return ResponseData{400, err, []byte(`{"error":"parse id error"}`)}
	}

	if err != nil {
		return ResponseFail(400, "parse payload error", err)
	}

	if len(businessPayload) == 0 {
		return ResponseData{200, nil, []byte(`[]`)}
	}

	for _, value := range businessPayload {
		if value.Business == 0 || companyID == 0 {
			continue
		}
		c, err := models.CreateCompanyBusiness(e.DB, companyID, value.Business, value.Description)
		if err != nil {
			continue
		}
		companyBusiness = append(companyBusiness, c)
		createdID = append(createdID, c.BusinessID)
	}
	if len(createdID) > 0 {
		err = models.DeleteCompanyBusinessNotInList(e.DB, companyID, createdID)
		if err != nil {
			logrus.Error("delete company business error", err)
		}
	}

	return ResponseData{200, nil, response.JsonEncodeModel(companyBusiness)}

}
