package api

type User struct {
	Users     int64  `json:"id"`
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	Avatar    string `json:"avatar"`
	Cover     string `json:"cover"`
	Email     string `json:"email"`
}

type UserExtend struct {
	Users     int64  `json:"id"`
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	Avatar    string `json:"avatar"`
	Cover     string `json:"cover"`
	Email     string `json:"email"`

	IsFriend      bool `json:"is_friend"`
	IsSentRequest bool `json:"is_sent_request"`
	IsFollow      bool `json:"is_follow"`
}

// UserResponse object of a user
// swagger:response User
type UserResponse struct {
	// in: body
	Body User
}

// UserExtendResponse object of a user extend
// swagger:response UserExtend
type UserExtendResponse struct {
	// in: body
	Body UserExtend
}

// UserListResponse list of object of a user
// swagger:response UserList
type UserListResponse struct {
	// List of users
	// in: body
	Body []User `json:"users"`
}
