package api

import (
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/go-chi/chi"
	"gitlab.com/rockship/backend/zeniius-backend/models"
)

// GetAllFriend ..
// swagger:route GET /friends relationship_friend GetAllFriend
// Get all friend
//
// Get all friend of this user
// Security:
//		api_key:
// Responses:
// 		200: UserList
func (e *Env) GetAllFriend(w http.ResponseWriter, r *http.Request) error {
	user := r.Context().Value("user").(*models.User)

	friendIDs, errString, err := user.ListMyFriend(e.DB)
	if err != nil {
		return ResponseFail(400, errString, err)
	}

	friends, err := models.GetMultiUserByID(e.DB, friendIDs)
	if err != nil {
		return ResponseFail(400, "", err)
	}

	friendList := make([]models.User, 0)

	for _, friend := range friends {
		friendTmp := models.GetExtraUserData(e.DB, friend)
		friendList = append(friendList, friendTmp)
	}

	jsonData, err := json.Marshal(friendList)
	if err != nil {
		return ResponseFail(400, "Build response failed.", err)
	}
	return ResponseData{200, nil, jsonData}
}

// GetUserFriend ..
// swagger:route GET /users/{id}/friends relationship_friend GetUserFriend
// Get all friend of a defined user
//
// Get all friend of a defined user
// Security:
//		api_key:
// Responses:
// 		200: UserList
func (e *Env) GetUserFriend(w http.ResponseWriter, r *http.Request) error {
	userID, err := strconv.ParseInt(chi.URLParam(r, "id"), 10, 64)
	if err != nil {
		return ResponseData{400, err, []byte(`{"error":"parse id error"}`)}
	}
	user := models.User{
		ID: userID,
	}

	friendIDs, errString, err := user.ListMyFriend(e.DB)
	if err != nil {
		return ResponseFail(400, errString, err)
	}

	friends, err := models.GetMultiUserByID(e.DB, friendIDs)
	if err != nil {
		return ResponseFail(400, "", err)
	}

	friendList := make([]models.User, 0)

	for _, friend := range friends {
		friendTmp := models.GetExtraUserData(e.DB, friend)
		friendList = append(friendList, friendTmp)
	}

	jsonData, err := json.Marshal(friendList)
	if err != nil {
		return ResponseFail(400, "Build response failed.", err)
	}
	return ResponseData{200, nil, jsonData}
}

// RemoveFriendParam ..
// swagger:parameters RemoveFriend
type RemoveFriendParam struct {
	// ID of Follower
	// in: path
	// required: true
	RequestsID int64 `json:"request_id"`
}

// RemoveFriend remove a friend from friend list
// swagger:route DELETE /friends/{friendID} relationship_friend RemoveFriend
// Unfriend
//
// Unfriend
// Security:
//		api_key:
// Responses:
// 		200: SimpleResponse
func (e *Env) RemoveFriend(w http.ResponseWriter, r *http.Request) error {
	user := r.Context().Value("user").(*models.User)
	friendIDParam := chi.URLParam(r, "friendID")

	friendID, err := strconv.Atoi(friendIDParam)
	if err != nil {
		return ResponseFail(400, "A valid request id is required.", nil)
	}

	errString, err := user.RemoveFriend(e.DB, int64(friendID))
	if err != nil {
		return ResponseFail(400, errString, err)
	}
	return ResponseSuccess()
}
