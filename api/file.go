package api

import (
	"net/http"
	"strconv"

	"github.com/go-chi/chi"
	"github.com/sirupsen/logrus"
	"gitlab.com/rockship/backend/zeniius-backend/models"
	"gitlab.com/rockship/backend/zeniius-backend/response"
	"gitlab.com/rockship/backend/zeniius-backend/service"
	"gitlab.com/rockship/backend/zeniius-backend/utils"
)

type UploadFile struct {
	// required: true
	Filename string `json:"filename"`
	// required: true
	ContentType string `json:"content_type"`
	// file thumbnail
	Thumbnail string `json:"thumbnail"`
	Width     int64  `json:"width"`
	Height    int64  `json:"height"`
	// file type: image | video | user_avatar | user_cover | company_logo | company_namecard | company_other | chat_file
	// enum: image, video, user_avatar, user_cover, company_logo, company_namecard, company_other, chat_file
	// required: true
	Type string `json:"type"`
}

// swagger:route POST /comment/file/url comment uploadLink
//
// Get signed url for comment
//
// Get upload url for comment file
// Security:
//		api_key:
// Responses:
// 		200: SignedUrl
func (e *Env) GetUploadLink(w http.ResponseWriter, r *http.Request) error {

	var file UploadFile
	err, message := utils.ParseRequest(r, &file)
	if err != nil {
		return ResponseData{403, err, []byte(message)}
	}

	url, err := service.CreateFileSignedUrl(file.Filename, file.ContentType, file.Type)

	if err != nil {
		logrus.Error("Get signed url > create signed url error", err)
		return ResponseData{403, err, []byte(`{"error":"create signed url error"`)}
	}

	var thumbnail response.SignedUrl
	if file.Thumbnail != "" {
		thumbnail, err = service.CreateFileSignedUrl(file.Thumbnail, file.ContentType, file.Type)
		if err != nil {
			return ResponseFail(500, "create thumbnail url error", err)
		}
	}

	f := models.File{
		FullPath:   url.DownloadURL,
		Type:       file.Type,
		Width:      file.Width,
		Height:     file.Height,
		MimeType:   file.ContentType,
		Filename:   url.Filename,
		BucketPath: service.GetFileBucketPath(file.Type),
		Thumbnail:  utils.ParseNullString(thumbnail.DownloadURL),
	}

	err = f.Save(e.DB)

	if err != nil {
		logrus.Info(utils.GetPath(r), "Get signed url > Save File")
		return ResponseData{500, err, []byte(`{"error":"Save file to DB error"}`)}
	}

	url.FileID = f.ID
	url.ThumbnailUpload = thumbnail.UploadURL

	return ResponseData{200, nil, response.JsonEncodeModel(url)}

}

// swagger:route POST /file/upload-url file uploadFile
//
// Get signed url for file
//
// Get upload url for file
// Security:
//		api_key:
// Responses:
// 		200: SignedUrl
func (e *Env) GetFileUploadLink(w http.ResponseWriter, r *http.Request) error {
	return e.GetUploadLink(w, r)
}

// swagger:route GET /files/{fid} file getFile
//
// Get full download file path
//
// Get full download file path
// Security:
//		api_key:
// Responses:
// 		301: string
func (e *Env) GetFileFullPath(w http.ResponseWriter, r *http.Request) error {
	fileID, err := strconv.ParseInt(chi.URLParam(r, "fid"), 10, 64)
	if err != nil {
		return ResponseData{400, err, []byte(`{"error":"parse id error"}`)}
	}

	fullPath, err := models.GetFileFullPath(e.DB, fileID)
	if err != nil {
		return ResponseFail(404, "File not found", err)
	}

	http.Redirect(w, r, fullPath, 302)
	return nil
}
