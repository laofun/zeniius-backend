package api

import (
	"github.com/go-chi/chi"
	"gitlab.com/rockship/backend/zeniius-backend/models"
	"gitlab.com/rockship/backend/zeniius-backend/response"
	"gitlab.com/rockship/backend/zeniius-backend/utils"
	"gopkg.in/guregu/null.v3"
	"net/http"
	"strconv"
	"time"
)

type CompanyPayload struct {
	Name        string                         `json:"name"`
	Description string                         `json:"description"`
	Contact     string                         `json:"contact"`
	Address     string                         `json:"address"`
	Phone       string                         `json:"phone"`
	Email       string                         `json:"email"`
	Facebook    string                         `json:"facebook_link"`
	Website     string                         `json:"website"`
	Role        string                         `json:"role"`
	Business    []CompanyBusinessCreatePayload `json:"business"`
}

type UpdateCompanyPayload struct {
	CompanyPayload
	NameCardFront int64   `json:"name_card_front"`
	NameCardBack  int64   `json:"name_card_back"`
	Logo          int64   `json:"logo"`
	Album         []int64 `json:"album"`
}

type CompanyBusinessCreatePayload struct {
	ID          int64  `json:"id"`
	Description string `json:"content"`
}

type CompanyAlbumPayload struct {
	RemoveFiles []int64 `json:"remove_files"`
	AddFiles    []int64 `json:"add_files"`
}

type CompanyCertificatePayload struct {
	File int64 `json:"file"`
}

// Ping swagger:route POST /user/me/company company createCompany
//
// Create user company
// Security:
//		api_key:
// Responses:
//		200: Company
func (e *Env) CreateCompany(w http.ResponseWriter, r *http.Request) error {
	var payload CompanyPayload
	err, message := utils.ParseRequest(r, &payload)

	if err != nil {
		return ResponseData{403, err, []byte(message)}
	}
	var u *models.User

	u = r.Context().Value("user").(*models.User)

	company := models.Company{
		Name:        payload.Name,
		Description: payload.Description,
		Contact:     payload.Contact,
		Address:     payload.Address,
		Phone:       payload.Phone,
		Email:       payload.Email,
		Facebook:    payload.Facebook,
		Website:     payload.Website,
	}

	err = company.Save(e.DB)

	if err != nil {
		return ResponseData{500, err, []byte(`{"message": "save company error"}`)}
	}

	var businessOfCompany []models.CompanyBusiness
	for _, value := range payload.Business {
		companyBusiness, err := models.CreateCompanyBusiness(e.DB, company.ID, value.ID, value.Description)
		if err == nil {
			businessOfCompany = append(businessOfCompany, companyBusiness)
		}
	}
	company.Business = businessOfCompany
	err = company.AddUserToCompany(e.DB, company.ID, u.ID, payload.Role)

	if err != nil {
		return ResponseData{500, err, []byte(`{"message": "save company error"}`)}
	}

	company.Role = payload.Role
	go models.RefreshUserSearchView(e.DB)
	return ResponseData{200, nil, response.JsonEncodeModel(company)}

}

// swagger:route GET /user/me/company company getListUserCompany
//
// Get list user company
//
// Security:
//		api_key:
// Responses:
//		200: Company
func (e *Env) GetListCompany(w http.ResponseWriter, r *http.Request) error {
	user := r.Context().Value("user").(*models.User)
	var listCompany []models.Company
	listCompany, err := models.GetListCompanyOfUser(e.DB, user.ID)
	if err != nil {
		return ResponseData{500, err, []byte(`{"message": "get user company error"}`)}
	}
	return ResponseData{200, nil, response.JsonEncodeModel(listCompany)}
}

// swagger:route GET /user/{id}/company company getCompanyOfUser
//
// Get list company of a user
//
// Security:
//		api_key:
// Responses:
//		200: Company
func (e *Env) GetListCompanyOfUser(w http.ResponseWriter, r *http.Request) error {
	idParam := chi.URLParam(r, "id")
	id, err := strconv.ParseInt(idParam, 10, 64)
	if err != nil {
		return ResponseData{400, err, []byte(`{"error":"parse id error"}`)}
	}

	var listCompany []models.Company
	listCompany, err = models.GetListCompanyOfUser(e.DB, id)
	if err != nil {
		return ResponseData{500, err, []byte(`{"message": "get user company error"}`)}
	}
	return ResponseData{200, nil, response.JsonEncodeModel(listCompany)}
}

// swagger:route PUT /user/me/company/{id} company updateCompany
//
// Update user company
// Security:
//		api_key:
// Responses:
//		200: Company
func (e *Env) UpdateUserCompany(w http.ResponseWriter, r *http.Request) error {
	idParam := chi.URLParam(r, "id")
	id, err := strconv.ParseInt(idParam, 10, 64)
	if err != nil {
		return ResponseData{400, err, []byte(`{"error":"parse id error"}`)}
	}
	var payload UpdateCompanyPayload
	err, message := utils.ParseRequest(r, &payload)

	if err != nil {
		return ResponseData{403, err, []byte(message)}
	}
	company, err := models.GetCompanyBasicInfo(e.DB, id)
	if err != nil {
		return ResponseFail(404, "company not found", err)
	}
	user := r.Context().Value("user").(*models.User)
	now := time.Now()

	company.Name = payload.Name
	company.Description = payload.Description
	company.Phone = payload.Phone
	company.Email = payload.Email
	company.Address = payload.Address
	company.Facebook = payload.Facebook
	company.Website = payload.Website
	company.UpdatedAt = &now

	if payload.Logo > 0 && models.CheckFileExist(e.DB, payload.Logo) {
		company.LogoPicture = null.NewInt(payload.Logo, true)
	}

	if payload.NameCardBack > 0 && models.CheckFileExist(e.DB, payload.NameCardBack) {
		company.NameCardBackPicture = null.NewInt(payload.NameCardBack, true)
	}

	if payload.NameCardFront > 0 && models.CheckFileExist(e.DB, payload.NameCardFront) {
		company.NameCardFrontPicture = null.NewInt(payload.NameCardFront, true)
	}

	err = company.Update(e.DB, user.ID, payload.Role)
	if err != nil {
		return ResponseData{400, err, []byte(`{"error":"save company error"}`)}
	}

	company, err = models.GetCompanyProfile(e.DB, company.ID)

	if err != nil {
		return ResponseData{500, err, []byte(`{"error":"get company profile error"}`)}
	}

	company.Role = payload.Role
	go models.RefreshUserSearchView(e.DB)
	return ResponseData{200, nil, response.JsonEncodeModel(company)}
}

// swagger:route PUT /user/me/company/{id}/album company updateCompanyAlbum
//
// Update company album
// Security:
//		api_key:
// Responses:
//		200: Company
func (e *Env) UpdateCompanyAlbum(w http.ResponseWriter, r *http.Request) error {
	idParam := chi.URLParam(r, "id")
	id, err := strconv.ParseInt(idParam, 10, 64)
	if err != nil {
		return ResponseData{400, err, []byte(`{"error":"parse id error"}`)}
	}
	var payload CompanyAlbumPayload
	err, message := utils.ParseRequest(r, &payload)

	if err != nil {
		return ResponseData{403, err, []byte(message)}
	}

	company, err := models.GetCompanyProfile(e.DB, id)

	if err != nil {
		return ResponseFail(404, "company not found", err)
	}

	err = company.UpdateCompanyAlbum(e.DB, payload.AddFiles, payload.RemoveFiles)

	if err != nil {
		return ResponseFail(500, "Update album error", err)
	}

	company, err = models.GetCompanyProfile(e.DB, id)

	return ResponseData{200, nil, response.JsonEncodeModel(company)}
}

// swagger:route POST /user/me/company/{id}/certificate company addCompanyCertificate
//
// Add company certificate
// Security:
//		api_key:
// Responses:
//		200: Default
func (e *Env) AddCompanyCertificate(w http.ResponseWriter, r *http.Request) error {
	idParam := chi.URLParam(r, "id")
	id, err := strconv.ParseInt(idParam, 10, 64)
	if err != nil {
		return ResponseData{400, err, []byte(`{"error":"parse id error"}`)}
	}
	var payload CompanyCertificatePayload
	err, message := utils.ParseRequest(r, &payload)

	if err != nil {
		return ResponseData{403, err, []byte(message)}
	}
	if models.CheckCompanyExist(e.DB, id) == false {
		return ResponseFail(404, "company not found", nil)
	}
	c := models.Company{
		ID: id,
	}

	certificate, err := c.AddCertificate(e.DB, payload.File)
	if err != nil {
		return ResponseFail(500, "add company certificate error", err)
	}

	return ResponseData{200, nil, response.JsonEncodeModel(certificate)}
}

// swagger:route DELETE /user/me/company/{company_id}/certificate/{file_id} company deleteCompanyCertificate
//
// Delete company certificate
// Security:
//		api_key:
// Responses:
//		200: Default
func (e *Env) DeleteCompanyCertificate(w http.ResponseWriter, r *http.Request) error {
	companyParam := chi.URLParam(r, "company_id")
	companyID, err := strconv.ParseInt(companyParam, 10, 64)
	if err != nil {
		return ResponseData{400, err, []byte(`{"error":"parse id error"}`)}
	}

	fileParam := chi.URLParam(r, "file_id")
	fileID, err := strconv.ParseInt(fileParam, 10, 64)
	if err != nil {
		return ResponseData{400, err, []byte(`{"error":"parse id error"}`)}
	}

	err = models.DeleteCompanyCertificates(e.DB, companyID, fileID)
	if err != nil {
		return ResponseFail(500, "delete company certificate error", err)
	}

	return ResponseSuccess()
}

// swagger:route GET /company company getAllCompany
//
// Get list company
//
// Security:
//		api_key:
// Responses:
//		200: []CompanyStub
func (e *Env) GetAllCompany(w http.ResponseWriter, r *http.Request) error {
	var listCompany []models.CompanyStub
	listCompany, err := models.GetCompanyStubProfiles(e.DB)
	if err != nil {
		return ResponseData{500, err, []byte(`{"message": "get company stub error"}`)}
	}
	return ResponseData{200, nil, response.JsonEncodeModel(listCompany)}
}
