ALTER TABLE posts
  ADD COLUMN locale varchar(20);

UPDATE posts set locale = 'vi';

ALTER TABLE comments
    ADD COLUMN locale varchar(20);

UPDATE comments set locale = 'vi';
-- Create translate_text table

CREATE SEQUENCE "translate_text_id_seq"
  INCREMENT 1
  MINVALUE  1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

CREATE TABLE "public"."translate_text" (
  "id" int8 NOT NULL DEFAULT nextval('translate_text_id_seq'::regclass),
  "object_type" varchar(20) COLLATE "pg_catalog"."default",
  "object_id" int8,
  "translated_text" text COLLATE "pg_catalog"."default",
  "locale" varchar(20),
  "created_at" timestamp(6) default NOW(),
  "updated_at" timestamp(6) default NOW()
);

ALTER TABLE "public"."translate_text" ADD CONSTRAINT "translate_text_pkey" PRIMARY KEY ("id");

ALTER TABLE translate_text
    ADD COLUMN original_hash varchar(500);