CREATE SEQUENCE "post_business_id_seq"
  INCREMENT 1
  MINVALUE  1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

CREATE TABLE "post_business" (
  "id" int8 NOT NULL DEFAULT nextval('post_business_id_seq'::regclass),
  "post_id" int8,
  "business_id" int8,
  "created_at" timestamp(0) DEFAULT now(),
  PRIMARY KEY ("id")
);

