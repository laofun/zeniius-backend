DROP SEQUENCE IF EXISTS "company_album_id_seq";
CREATE SEQUENCE "company_album_id_seq"
  INCREMENT 1
  MINVALUE  1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
CREATE TABLE company_album (
  "id" int8 NOT NULL DEFAULT nextval('company_album_id_seq'::regclass),
  "company_id" int8,
  "file_id" int8,
  "created_at" timestamp(6),
  "updated_at" timestamp(6),
  PRIMARY KEY ("id")
);