CREATE SEQUENCE "company_certificates_id_seq"
  INCREMENT 1
  MINVALUE  1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

CREATE TABLE "company_certificates" (
  "id" int8 NOT NULL DEFAULT nextval('company_certificates_id_seq'::regclass),
  "company_id" int8,
  "file_id" int8,
  "created_at" timestamp(0) DEFAULT now(),
  "deleted_at" timestamp(0),
  PRIMARY KEY ("id")
);