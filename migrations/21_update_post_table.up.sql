ALTER TABLE posts
  DROP  format_type,
  ADD COLUMN demand_type varchar(20),
  ADD COLUMN is_public boolean default TRUE,
  ADD COLUMN start_time timestamp,
  ADD COLUMN end_time timestamp;