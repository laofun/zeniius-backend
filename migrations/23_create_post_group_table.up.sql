DROP TABLE post_share;

CREATE SEQUENCE "post_group_id_seq"
  INCREMENT 1
  MINVALUE  1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

CREATE TABLE "post_group" (
  "id" int8 NOT NULL DEFAULT nextval('post_group_id_seq'::regclass),
  "post_id" int8,
  "user_id" int8,
  "group_id" uuid,
  "created_at" timestamp(0) DEFAULT now(),
  "deleted_at" timestamp(0),
  PRIMARY KEY ("id")
);