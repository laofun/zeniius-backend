CREATE SEQUENCE "company_business_id_seq"
  INCREMENT 1
  MINVALUE  1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
CREATE TABLE company_business (
  "id" int8 NOT NULL DEFAULT nextval('company_business_id_seq'::regclass),
  "company_id" int8,
  "business_id" int8,
  "created_at" timestamp(6) DEFAULT NOW(),
  "updated_at" timestamp(6),
  "deleted_at" timestamp(6),
  "description" varchar(1000),
  PRIMARY KEY ("id")
);