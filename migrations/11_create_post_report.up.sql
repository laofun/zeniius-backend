CREATE SEQUENCE "post_reports_id_seq"
  INCREMENT 1
  MINVALUE  1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

CREATE TABLE "post_reports" (
  "id" int8 NOT NULL DEFAULT nextval('post_reports_id_seq'::regclass),
  "post_id" int8,
  "user_id" int8,
  "created_at" timestamp(0) DEFAULT now(),
  "message" varchar(500),
  PRIMARY KEY ("id")
);