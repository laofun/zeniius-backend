DROP MATERIALIZED VIEW user_search;
CREATE MATERIALIZED VIEW user_search AS
  SELECT u.id, to_tsvector(unaccent(COALESCE(u.full_name, ''))) ||
               to_tsvector(unaccent(COALESCE(u.description, ''))) ||
               to_tsvector(unaccent(COALESCE(u.email, ''))) ||
               to_tsvector(COALESCE(u.phone_number, '')) ||
               to_tsvector(unaccent(COALESCE(string_agg(c.name, ' '), '')))||
               to_tsvector(unaccent(COALESCE(string_agg(c.phone_number, ' '), '')))||
               to_tsvector(unaccent(COALESCE(string_agg(c.description, ' '), '')))
               as docs,
               concat_ws(',',
                         unaccent(COALESCE(u.full_name, '')),
                         unaccent(COALESCE(u.description, '')),
                         unaccent(COALESCE(u.email, '')),
                         unaccent(COALESCE(u.phone_number, '')),
                         unaccent(COALESCE(string_agg(c.name, ' '), '')),
                         unaccent(COALESCE(string_agg(c.phone_number, ' '), '')),
                         unaccent(COALESCE(string_agg(c.description, ' '), ''))
               ) as literal_doc
  FROM users u
    left join user_company uc on u.id = uc.user_id
    left join companies c on c.id = uc.company_id
  WHERE u.is_verify = true
  GROUP BY u.id;
