

-- ----------------------------
-- Sequence structure for ability_cards_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."ability_cards_id_seq";
CREATE SEQUENCE "public"."ability_cards_id_seq"
  INCREMENT 1
  MINVALUE  1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

-- ----------------------------
-- Sequence structure for business_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."business_id_seq";
CREATE SEQUENCE "public"."business_id_seq"
  INCREMENT 1
  MINVALUE  1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

-- ----------------------------
-- Sequence structure for comments_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."comments_id_seq";
CREATE SEQUENCE "public"."comments_id_seq"
  INCREMENT 1
  MINVALUE  1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

-- ----------------------------
-- Sequence structure for companies_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."companies_id_seq";
CREATE SEQUENCE "public"."companies_id_seq"
  INCREMENT 1
  MINVALUE  1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

-- ----------------------------
-- Sequence structure for emoticon_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."emoticon_id_seq";
CREATE SEQUENCE "public"."emoticon_id_seq"
  INCREMENT 1
  MINVALUE  1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

-- ----------------------------
-- Sequence structure for files_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."files_id_seq";
CREATE SEQUENCE "public"."files_id_seq"
  INCREMENT 1
  MINVALUE  1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

-- ----------------------------
-- Sequence structure for fk_user_to_post_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."fk_user_to_post_id_seq";
CREATE SEQUENCE "public"."fk_user_to_post_id_seq"
  INCREMENT 1
  MINVALUE  1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

-- ----------------------------
-- Sequence structure for fk_user_to_post_user_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."fk_user_to_post_user_id_seq";
CREATE SEQUENCE "public"."fk_user_to_post_user_id_seq"
  INCREMENT 1
  MINVALUE  1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

-- ----------------------------
-- Sequence structure for post_like_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."post_like_id_seq";
CREATE SEQUENCE "public"."post_like_id_seq"
  INCREMENT 1
  MINVALUE  1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

-- ----------------------------
-- Sequence structure for post_register_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."post_register_id_seq";
CREATE SEQUENCE "public"."post_register_id_seq"
  INCREMENT 1
  MINVALUE  1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

-- ----------------------------
-- Sequence structure for post_share_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."post_share_id_seq";
CREATE SEQUENCE "public"."post_share_id_seq"
  INCREMENT 1
  MINVALUE  1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

-- ----------------------------
-- Sequence structure for post_view_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."post_view_id_seq";
CREATE SEQUENCE "public"."post_view_id_seq"
  INCREMENT 1
  MINVALUE  1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

-- ----------------------------
-- Sequence structure for skills_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."skills_id_seq";
CREATE SEQUENCE "public"."skills_id_seq"
  INCREMENT 1
  MINVALUE  1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

-- ----------------------------
-- Sequence structure for user_access_token_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."user_access_token_id_seq";
CREATE SEQUENCE "public"."user_access_token_id_seq"
  INCREMENT 1
  MINVALUE  1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

-- ----------------------------
-- Sequence structure for user_follow_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."user_follow_id_seq";
CREATE SEQUENCE "public"."user_follow_id_seq"
  INCREMENT 1
  MINVALUE  1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

-- ----------------------------
-- Sequence structure for user_skill_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."user_skill_id_seq";
CREATE SEQUENCE "public"."user_skill_id_seq"
  INCREMENT 1
  MINVALUE  1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

-- ----------------------------
-- Sequence structure for users_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."users_id_seq";
CREATE SEQUENCE "public"."users_id_seq"
  INCREMENT 1
  MINVALUE  1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

-- ----------------------------
-- Table structure for ability_cards
-- ----------------------------
DROP TABLE IF EXISTS "public"."ability_cards";
CREATE TABLE "public"."ability_cards" (
  "id" int8 NOT NULL DEFAULT nextval('ability_cards_id_seq'::regclass),
  "user_skill_id" int8,
  "creator_id" int8,
  "owner_id" int8,
  "start_time" timestamp(0),
  "end_time" timestamp(0),
  "use_limit" int4,
  "time_use" int4,
  "is_enable" bool,
  "description" varchar(255) COLLATE "pg_catalog"."default",
  "created_at" timestamp(0),
  "updated_at" timestamp(0),
  "deleted_at" timestamp(0),
  "transaction_id" int8
)
;
ALTER TABLE "public"."ability_cards" OWNER TO "rockship";

-- ----------------------------
-- Table structure for business
-- ----------------------------
DROP TABLE IF EXISTS "public"."business";
CREATE TABLE "public"."business" (
  "id" int8 NOT NULL DEFAULT nextval('business_id_seq'::regclass),
  "title" varchar(512) COLLATE "pg_catalog"."default",
  "description" text COLLATE "pg_catalog"."default",
  "created_at" timestamp(0),
  "updated_at" timestamp(0),
  "deleted_at" timestamp(0)
)
;
ALTER TABLE "public"."business" OWNER TO "rockship";

-- ----------------------------
-- Table structure for comment_like
-- ----------------------------
DROP TABLE IF EXISTS "public"."comment_like";
CREATE TABLE "public"."comment_like" (
  "comment_id" int8 NOT NULL,
  "user_id" int8 NOT NULL,
  "created_at" timestamp(0),
  "deleted_at" timestamp(0)
)
;
ALTER TABLE "public"."comment_like" OWNER TO "rockship";

-- ----------------------------
-- Table structure for comments
-- ----------------------------
DROP TABLE IF EXISTS "public"."comments";
CREATE TABLE "public"."comments" (
  "id" int8 NOT NULL DEFAULT nextval('comments_id_seq'::regclass),
  "post_id" int8,
  "parent_id" int8,
  "content" text COLLATE "pg_catalog"."default",
  "attachment" int8,
  "user_id" int8,
  "created_at" timestamp(0),
  "updated_at" timestamp(0),
  "original_id" int8,
  "deleted_at" timestamp(0)
)
;
ALTER TABLE "public"."comments" OWNER TO "rockship";

-- ----------------------------
-- Records of comments
-- ----------------------------
BEGIN;
INSERT INTO "public"."comments" VALUES (1, 2, NULL, 'test comment', NULL, 2, '2018-07-19 13:43:53', NULL, NULL, NULL);
INSERT INTO "public"."comments" VALUES (2, 2, NULL, 'test comment', NULL, 3, '2018-07-19 13:44:02', NULL, NULL, NULL);
INSERT INTO "public"."comments" VALUES (3, 3, NULL, 'test comment', NULL, 2, '2018-07-19 13:44:12', NULL, NULL, NULL);
INSERT INTO "public"."comments" VALUES (4, 3, NULL, 'test comment', NULL, 3, '2018-07-19 13:44:22', NULL, NULL, NULL);
INSERT INTO "public"."comments" VALUES (5, 3, NULL, 'test comment', NULL, 4, '2018-07-19 13:44:34', NULL, NULL, NULL);
INSERT INTO "public"."comments" VALUES (6, 4, NULL, 'test comment', NULL, 1, '2018-07-19 13:44:44', NULL, NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for companies
-- ----------------------------
DROP TABLE IF EXISTS "public"."companies";
CREATE TABLE "public"."companies" (
  "id" int8 NOT NULL DEFAULT nextval('companies_id_seq'::regclass),
  "name" varchar(512) COLLATE "pg_catalog"."default",
  "owner_id" int8,
  "description" text COLLATE "pg_catalog"."default",
  "contact" varchar(255) COLLATE "pg_catalog"."default",
  "address" varchar(255) COLLATE "pg_catalog"."default",
  "business_registration_cert" int8,
  "created_at" timestamp(0),
  "updated_at" timestamp(0),
  "deleted_at" timestamp(0),
  "logo" int8
)
;
ALTER TABLE "public"."companies" OWNER TO "rockship";

-- ----------------------------
-- Table structure for emoticons
-- ----------------------------
DROP TABLE IF EXISTS "public"."emoticons";
CREATE TABLE "public"."emoticons" (
  "id" int8 NOT NULL DEFAULT nextval('emoticon_id_seq'::regclass),
  "name" varchar(255) COLLATE "pg_catalog"."default",
  "image" int8
)
;
ALTER TABLE "public"."emoticons" OWNER TO "rockship";

-- ----------------------------
-- Table structure for files
-- ----------------------------
DROP TABLE IF EXISTS "public"."files";
CREATE TABLE "public"."files" (
  "id" int8 NOT NULL DEFAULT nextval('files_id_seq'::regclass),
  "filename" varchar(512) COLLATE "pg_catalog"."default",
  "mime_type" varchar(255) COLLATE "pg_catalog"."default",
  "extension" varchar(255) COLLATE "pg_catalog"."default",
  "full_path" varchar(512) COLLATE "pg_catalog"."default",
  "type" varchar(20) COLLATE "pg_catalog"."default",
  "size" int8,
  "width" int8,
  "height" int8,
  "thumbnail" varchar(255) COLLATE "pg_catalog"."default",
  "created_at" timestamp(0),
  "updated_at" timestamp(0),
  "deleted_at" timestamp(0)
)
;
ALTER TABLE "public"."files" OWNER TO "rockship";

-- ----------------------------
-- Records of files
-- ----------------------------
BEGIN;
INSERT INTO "public"."files" VALUES (1, 'avatar', 'image/jpeg', 'jpg', 'https://s3.amazonaws.com/uifaces/faces/twitter/adobi/128.jpg', 'profile_pic', 50, 50, 50, NULL, '2018-07-13 13:31:54', NULL, NULL);
INSERT INTO "public"."files" VALUES (2, 'avatar', 'image/jpeg', 'jpg', 'https://s3.amazonaws.com/uifaces/faces/twitter/dmackerman/128.jpg', 'profile_pic', 50, 50, 50, NULL, '2018-07-13 13:31:54', NULL, NULL);
INSERT INTO "public"."files" VALUES (3, 'avatar', 'image/jpeg', 'jpg', 'https://s3.amazonaws.com/uifaces/faces/twitter/looneydoodle/128.jpg', 'profile_pic', 50, 50, 50, NULL, '2018-07-13 13:31:54', NULL, NULL);
INSERT INTO "public"."files" VALUES (5, 'avatar', 'image/jpeg', 'jpg', 'https://s3.amazonaws.com/uifaces/faces/twitter/malgordon/128.jpg', 'profile_pic', 50, 50, 50, NULL, '2018-07-13 13:31:54', NULL, NULL);
INSERT INTO "public"."files" VALUES (4, 'avatar', 'video/mp4', 'mp4', 'http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4', 'video', 1000, 400, 400, NULL, '2018-07-13 13:31:54', NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for post_ability_card
-- ----------------------------
DROP TABLE IF EXISTS "public"."post_ability_card";
CREATE TABLE "public"."post_ability_card" (
  "post_id" int8 NOT NULL,
  "ability_card_id" int8 NOT NULL,
  "created_at" timestamp(0),
  "deleted_at" timestamp(0)
)
;
ALTER TABLE "public"."post_ability_card" OWNER TO "rockship";

-- ----------------------------
-- Table structure for post_file
-- ----------------------------
DROP TABLE IF EXISTS "public"."post_file";
CREATE TABLE "public"."post_file" (
  "post_id" int8 NOT NULL,
  "file_id" int8 NOT NULL,
  "created_at" timestamp(0),
  "deleted_at" timestamp(0)
)
;
ALTER TABLE "public"."post_file" OWNER TO "rockship";

-- ----------------------------
-- Records of post_file
-- ----------------------------
BEGIN;
INSERT INTO "public"."post_file" VALUES (2, 2, '2018-07-19 14:44:34', NULL);
INSERT INTO "public"."post_file" VALUES (2, 3, '2018-07-19 14:44:41', NULL);
INSERT INTO "public"."post_file" VALUES (3, 4, '2018-07-19 14:44:52', NULL);
COMMIT;

-- ----------------------------
-- Table structure for post_like
-- ----------------------------
DROP TABLE IF EXISTS "public"."post_like";
CREATE TABLE "public"."post_like" (
  "id" int8 NOT NULL DEFAULT nextval('post_like_id_seq'::regclass),
  "post_id" int8,
  "created_at" timestamp(0),
  "updated_at" timestamp(0),
  "deleted_at" timestamp(0),
  "user_id" int8
)
;
ALTER TABLE "public"."post_like" OWNER TO "rockship";

-- ----------------------------
-- Records of post_like
-- ----------------------------
BEGIN;
INSERT INTO "public"."post_like" VALUES (1, 2, '2018-07-19 13:43:53', NULL, NULL, 2);
INSERT INTO "public"."post_like" VALUES (2, 2, '2018-07-19 13:44:02', NULL, NULL, 3);
INSERT INTO "public"."post_like" VALUES (5, 3, '2018-07-19 13:44:34', NULL, NULL, 4);
INSERT INTO "public"."post_like" VALUES (6, 4, '2018-07-19 13:44:44', NULL, NULL, 1);
INSERT INTO "public"."post_like" VALUES (3, 5, '2018-07-19 13:44:12', NULL, NULL, 2);
INSERT INTO "public"."post_like" VALUES (4, 4, '2018-07-19 13:44:22', NULL, NULL, 3);
COMMIT;

-- ----------------------------
-- Table structure for post_register
-- ----------------------------
DROP TABLE IF EXISTS "public"."post_register";
CREATE TABLE "public"."post_register" (
  "id" int8 NOT NULL DEFAULT nextval('post_register_id_seq'::regclass),
  "post_id" int8,
  "count" int8,
  "created_at" timestamp(0),
  "user_id" int8,
  "updated_at" timestamp(0),
  "deleted_at" timestamp(0)
)
;
ALTER TABLE "public"."post_register" OWNER TO "rockship";

-- ----------------------------
-- Records of post_register
-- ----------------------------
BEGIN;
INSERT INTO "public"."post_register" VALUES (1, 2, NULL, '2018-07-19 13:43:53', 2, NULL, NULL);
INSERT INTO "public"."post_register" VALUES (2, 2, NULL, '2018-07-19 13:44:02', 3, NULL, NULL);
INSERT INTO "public"."post_register" VALUES (6, 4, NULL, '2018-07-19 13:44:44', 1, NULL, NULL);
INSERT INTO "public"."post_register" VALUES (4, 4, NULL, '2018-07-19 13:44:22', 3, NULL, NULL);
INSERT INTO "public"."post_register" VALUES (5, 5, NULL, '2018-07-19 13:44:34', 4, NULL, NULL);
INSERT INTO "public"."post_register" VALUES (3, 2, NULL, '2018-07-19 13:44:12', 2, NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for post_share
-- ----------------------------
DROP TABLE IF EXISTS "public"."post_share";
CREATE TABLE "public"."post_share" (
  "id" int8 NOT NULL DEFAULT nextval('post_share_id_seq'::regclass),
  "post_id" int8,
  "created_at" timestamp(0),
  "updated_at" timestamp(0),
  "deleted_at" timestamp(0),
  "user_id" int8
)
;
ALTER TABLE "public"."post_share" OWNER TO "rockship";

-- ----------------------------
-- Records of post_share
-- ----------------------------
BEGIN;
INSERT INTO "public"."post_share" VALUES (1, 2, '2018-07-19 13:43:53', NULL, NULL, 2);
INSERT INTO "public"."post_share" VALUES (2, 2, '2018-07-19 13:44:02', NULL, NULL, 3);
INSERT INTO "public"."post_share" VALUES (4, 3, '2018-07-19 13:44:22', NULL, NULL, 3);
INSERT INTO "public"."post_share" VALUES (6, 4, '2018-07-19 13:44:44', NULL, NULL, 1);
INSERT INTO "public"."post_share" VALUES (3, 2, '2018-07-19 13:44:12', NULL, NULL, 2);
INSERT INTO "public"."post_share" VALUES (5, 2, '2018-07-19 13:44:34', NULL, NULL, 4);
COMMIT;

-- ----------------------------
-- Table structure for post_view
-- ----------------------------
DROP TABLE IF EXISTS "public"."post_view";
CREATE TABLE "public"."post_view" (
  "id" int8 NOT NULL DEFAULT nextval('post_view_id_seq'::regclass),
  "post_id" int8,
  "user_id" int8,
  "created_at" timestamp(0)
)
;
ALTER TABLE "public"."post_view" OWNER TO "rockship";

-- ----------------------------
-- Records of post_view
-- ----------------------------
BEGIN;
INSERT INTO "public"."post_view" VALUES (1, 2, 2, '2018-07-19 13:43:53');
INSERT INTO "public"."post_view" VALUES (2, 2, 3, '2018-07-19 13:44:02');
INSERT INTO "public"."post_view" VALUES (3, 3, 2, '2018-07-19 13:44:12');
INSERT INTO "public"."post_view" VALUES (4, 3, 3, '2018-07-19 13:44:22');
INSERT INTO "public"."post_view" VALUES (5, 3, 4, '2018-07-19 13:44:34');
INSERT INTO "public"."post_view" VALUES (6, 4, 1, '2018-07-19 13:44:44');
COMMIT;

-- ----------------------------
-- Table structure for posts
-- ----------------------------
DROP TABLE IF EXISTS "public"."posts";
CREATE TABLE "public"."posts" (
  "id" int8 NOT NULL DEFAULT nextval('fk_user_to_post_id_seq'::regclass),
  "user_id" int8 NOT NULL DEFAULT nextval('fk_user_to_post_user_id_seq'::regclass),
  "type" varchar(20) COLLATE "pg_catalog"."default",
  "title" varchar(512) COLLATE "pg_catalog"."default",
  "description" text COLLATE "pg_catalog"."default",
  "created_at" timestamp(0),
  "updated_at" timestamp(0),
  "deleted_at" timestamp(0),
  "fields" jsonb,
  "status" varchar(20) COLLATE "pg_catalog"."default",
  "format_type" varchar(20) COLLATE "pg_catalog"."default",
  "location_lat" float4,
  "location_long" float4
)
;
ALTER TABLE "public"."posts" OWNER TO "rockship";

-- ----------------------------
-- Records of posts
-- ----------------------------
BEGIN;
INSERT INTO "public"."posts" VALUES (5, 2, 'event', 'Autem odit enim nulla quia porro vel dolorem autem ', 'Voluptatum beatae similique assumenda blanditiis consequatur quas. Autem aut quasi. Nihil aliquid sunt sapiente sed ratione voluptas illum sed eos. Officiis quia est est commodi. Possimus quis sunt.', '2018-07-13 13:34:50', NULL, NULL, '{}', 'sent', 'text', NULL, NULL);
INSERT INTO "public"."posts" VALUES (2, 3, 'event', 'this is a new event for everyone ', 'Voluptatum beatae similique assumenda blanditiis consequatur quas. Autem aut quasi. Nihil aliquid sunt sapiente sed ratione voluptas illum sed eos. Officiis quia est est commodi. Possimus quis sunt.', '2018-07-13 13:34:50', NULL, NULL, '{}', 'sent', 'images', NULL, NULL);
INSERT INTO "public"."posts" VALUES (3, 4, 'event', 'Maiores hic voluptas sed consequatur possimus sit expedita. ', 'Officiis veritatis voluptatem. Minima incidunt eveniet aperiam quidem ea corporis voluptas sint. Excepturi eveniet impedit ut deserunt nemo fuga tenetur atque incidunt. Iste dolorum aspernatur harum perspiciatis.

Voluptatum est vero cupiditate ea magnam laborum quo. Eius dolorum et delectus sapiente nam reprehenderit. Aut ea velit voluptate reiciendis non quibusdam.

Doloremque qui amet eveniet voluptatem incidunt adipisci. Rem rerum sequi et vitae et ipsam esse et. Aliquam odit ab amet necessitatibus quis.', '2018-07-13 13:34:50', NULL, NULL, '{}', 'sent', 'videos', NULL, NULL);
INSERT INTO "public"."posts" VALUES (6, 3, 'event', 'this is a new event for everyone ', 'Voluptatum beatae similique assumenda blanditiis consequatur quas. Autem aut quasi. Nihil aliquid sunt sapiente sed ratione voluptas illum sed eos. Officiis quia est est commodi. Possimus quis sunt.', '2018-07-13 13:34:50', NULL, NULL, '{}', 'sent', 'text', NULL, NULL);
INSERT INTO "public"."posts" VALUES (7, 4, 'event', 'Minima voluptatem eos voluptas suscipit incidunt dolores omnis omnis.', 'Voluptatum beatae similique assumenda blanditiis consequatur quas. Autem aut quasi. Nihil aliquid sunt sapiente sed ratione voluptas illum sed eos. Officiis quia est est commodi. Possimus quis sunt.', '2018-07-13 13:34:50', NULL, NULL, '{}', 'sent', 'text', NULL, NULL);
INSERT INTO "public"."posts" VALUES (8, 5, 'event', 'Autem odit enim nulla quia porro vel dolorem autem ', 'Voluptatum beatae similique assumenda blanditiis consequatur quas. Autem aut quasi. Nihil aliquid sunt sapiente sed ratione voluptas illum sed eos. Officiis quia est est commodi. Possimus quis sunt.', '2018-07-13 13:34:50', NULL, NULL, '{}', 'sent', 'text', NULL, NULL);
INSERT INTO "public"."posts" VALUES (9, 2, 'event', 'Maiores hic voluptas sed consequatur possimus sit expedita. ', 'Officiis veritatis voluptatem. Minima incidunt eveniet aperiam quidem ea corporis voluptas sint. Excepturi eveniet impedit ut deserunt nemo fuga tenetur atque incidunt. Iste dolorum aspernatur harum perspiciatis.

Voluptatum est vero cupiditate ea magnam laborum quo. Eius dolorum et delectus sapiente nam reprehenderit. Aut ea velit voluptate reiciendis non quibusdam.

Doloremque qui amet eveniet voluptatem incidunt adipisci. Rem rerum sequi et vitae et ipsam esse et. Aliquam odit ab amet necessitatibus quis.', '2018-07-13 13:34:50', NULL, NULL, '{}', 'sent', 'text', NULL, NULL);
INSERT INTO "public"."posts" VALUES (1, 2, 'event', 'this is a new event for everyone ', 'Voluptatum beatae similique assumenda blanditiis consequatur quas. Autem aut quasi. Nihil aliquid sunt sapiente sed ratione voluptas illum sed eos. Officiis quia est est commodi. Possimus quis sunt.', '2018-07-13 13:34:50', NULL, NULL, '{}', 'sent', 'text', NULL, NULL);
INSERT INTO "public"."posts" VALUES (4, 5, 'event', 'Sint corporis atque sapiente inventore. ', 'Voluptatum beatae similique assumenda blanditiis consequatur quas. Autem aut quasi. Nihil aliquid sunt sapiente sed ratione voluptas illum sed eos. Officiis quia est est commodi. Possimus quis sunt.', '2018-07-13 13:34:50', NULL, NULL, '{}', 'sent', 'location', 40.7128, 74.006);
INSERT INTO "public"."posts" VALUES (10, 1, 'event', 'test title', 'this is description', NULL, NULL, NULL, NULL, 'urgent', 'text', NULL, NULL);
INSERT INTO "public"."posts" VALUES (11, 2, 'event', 'test title', 'this is description', '2018-07-27 10:18:35', NULL, NULL, 'null', 'urgent', 'text', NULL, NULL);
INSERT INTO "public"."posts" VALUES (12, 2, 'event', 'test title', 'this is description', '2018-07-27 10:42:59', NULL, NULL, 'null', 'urgent', 'text', NULL, NULL);
INSERT INTO "public"."posts" VALUES (13, 2, 'event', 'test title', 'this is description', '2018-07-27 11:05:39', NULL, NULL, NULL, 'urgent', 'text', NULL, NULL);
INSERT INTO "public"."posts" VALUES (14, 2, 'event', 'test title', 'this is description', '2018-07-27 11:11:00', NULL, NULL, '{"test": "new test"}', 'urgent', 'text', NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for skills
-- ----------------------------
DROP TABLE IF EXISTS "public"."skills";
CREATE TABLE "public"."skills" (
  "id" int8 NOT NULL DEFAULT nextval('skills_id_seq'::regclass),
  "name" varchar(255) COLLATE "pg_catalog"."default",
  "description" text COLLATE "pg_catalog"."default",
  "created_at" timestamp(0),
  "updated_at" timestamp(0),
  "deleted_at" timestamp(0)
)
;
ALTER TABLE "public"."skills" OWNER TO "rockship";

-- ----------------------------
-- Table structure for user_access_token
-- ----------------------------
DROP TABLE IF EXISTS "public"."user_access_token";
CREATE TABLE "public"."user_access_token" (
  "id" int8 NOT NULL DEFAULT nextval('user_access_token_id_seq'::regclass),
  "platform" varchar(20) COLLATE "pg_catalog"."default",
  "access_token" varchar(255) COLLATE "pg_catalog"."default",
  "ip_address" varchar(50) COLLATE "pg_catalog"."default",
  "device_info" jsonb,
  "expired_at" timestamp(0),
  "expired_in" int4,
  "created_at" timestamp(0) DEFAULT now(),
  "updated_at" timestamp(0),
  "user_id" int8
)
;
ALTER TABLE "public"."user_access_token" OWNER TO "rockship";

-- ----------------------------
-- Records of user_access_token
-- ----------------------------
BEGIN;
INSERT INTO "public"."user_access_token" VALUES (3, 'android', '7p1QtARmqULZCU6giA0QvzvrYfH6U8u3I3ofcZYy8c5RJ3Yl6b9k2fipKIXcQP0JcMGk16c7QPJpvOce', '[::1]:58301', NULL, '2018-08-30 14:19:15', 2592000, '2018-07-31 14:19:15', NULL, 1);
INSERT INTO "public"."user_access_token" VALUES (4, 'android', 'J0IlAi6ATrNp4bLJCW9QIPViOpeo7xzctN57CM13yUg4SrkHgaTQ6z1yUnkMOPk2SyAAT9xCpXHUaMbR', '[::1]:58782', NULL, '2018-08-30 15:21:50', 2592000, '2018-07-31 15:21:50', NULL, 1);
COMMIT;

-- ----------------------------
-- Table structure for user_business
-- ----------------------------
DROP TABLE IF EXISTS "public"."user_business";
CREATE TABLE "public"."user_business" (
  "user_id" int8 NOT NULL,
  "business_id" int8 NOT NULL,
  "created_at" timestamp(0)
)
;
ALTER TABLE "public"."user_business" OWNER TO "rockship";

-- ----------------------------
-- Table structure for user_company
-- ----------------------------
DROP TABLE IF EXISTS "public"."user_company";
CREATE TABLE "public"."user_company" (
  "user_id" int8 NOT NULL,
  "company_id" int8 NOT NULL,
  "created_at" timestamp(0),
  "role" varchar(20) COLLATE "pg_catalog"."default"
)
;
ALTER TABLE "public"."user_company" OWNER TO "rockship";

-- ----------------------------
-- Table structure for user_follow
-- ----------------------------
DROP TABLE IF EXISTS "public"."user_follow";
CREATE TABLE "public"."user_follow" (
  "id" int8 NOT NULL DEFAULT nextval('user_follow_id_seq'::regclass),
  "user_id" int8,
  "follower_id" int8,
  "created_id" timestamp(0),
  "deleted_at" timestamp(0)
)
;
ALTER TABLE "public"."user_follow" OWNER TO "rockship";

-- ----------------------------
-- Records of user_follow
-- ----------------------------
BEGIN;
INSERT INTO "public"."user_follow" VALUES (1, 2, 1, '2018-07-18 15:57:40', NULL);
INSERT INTO "public"."user_follow" VALUES (2, 3, 1, '2018-07-18 15:58:55', NULL);
INSERT INTO "public"."user_follow" VALUES (3, 4, 1, '2018-07-18 15:59:06', NULL);
INSERT INTO "public"."user_follow" VALUES (4, 5, 1, '2018-07-18 15:59:24', NULL);
COMMIT;

-- ----------------------------
-- Table structure for user_skill
-- ----------------------------
DROP TABLE IF EXISTS "public"."user_skill";
CREATE TABLE "public"."user_skill" (
  "id" int8 NOT NULL DEFAULT nextval('user_skill_id_seq'::regclass),
  "user_id" int8,
  "skill_id" int8,
  "reputation" int2,
  "issue_number_limit" int2,
  "issue_time_limit" int8,
  "created_at" timestamp(0),
  "updated_at" timestamp(0),
  "deleted_at" timestamp(0)
)
;
ALTER TABLE "public"."user_skill" OWNER TO "rockship";

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS "public"."users";
CREATE TABLE "public"."users" (
  "id" int8 NOT NULL DEFAULT nextval('users_id_seq'::regclass),
  "first_name" varchar(255) COLLATE "pg_catalog"."default",
  "middle_name" varchar(255) COLLATE "pg_catalog"."default",
  "last_name" varchar(255) COLLATE "pg_catalog"."default",
  "birthday" date,
  "phone_number" varchar(20) COLLATE "pg_catalog"."default",
  "username" varchar(255) COLLATE "pg_catalog"."default",
  "profile_pic" int8,
  "email" varchar(255) COLLATE "pg_catalog"."default",
  "created_at" timestamp(0),
  "updated_at" timestamp(0),
  "deleted_at" timestamp(0),
  "cover_pic" int8,
  "facebook_link" varchar(255) COLLATE "pg_catalog"."default",
  "name_card_front" int8,
  "name_card_back" int8,
  "firebase_id" varchar(50) COLLATE "pg_catalog"."default"
)
;
ALTER TABLE "public"."users" OWNER TO "rockship";

-- ----------------------------
-- Records of users
-- ----------------------------
BEGIN;
INSERT INTO "public"."users" VALUES (3, 'alex', 'st', 'nike', NULL, '0999999999', 'this_is_mike2', 3, 'mike2@mike.com', '2018-07-13 13:33:13', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO "public"."users" VALUES (4, 'pop', 'st', 'top', NULL, '0999999999', 'this_is_mike3', 4, 'mike3@mike.com', '2018-07-13 13:33:13', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO "public"."users" VALUES (5, 'sam', 'st', 'sun', NULL, '0999999999', 'this_is_mike4', 5, 'mike4@mike.com', '2018-07-13 13:33:13', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO "public"."users" VALUES (2, 'jogn', 'st', 'nisggwa', NULL, '0999999999', 'this_is_mike1', 2, 'mike1@mike.com', '2018-07-13 13:33:13', NULL, NULL, 3, NULL, NULL, NULL, NULL);
INSERT INTO "public"."users" VALUES (1, 'mike', 'st', 'nigg', NULL, '0999999999', 'this_is_mike', 1, 'mike@mike.com', '2018-07-13 13:33:13', NULL, NULL, 2, NULL, NULL, NULL, 'i2YtKBHIFOOrioxiTgi9pEOrC1P2');
COMMIT;

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."ability_cards_id_seq"
OWNED BY "public"."ability_cards"."id";
SELECT setval('"public"."ability_cards_id_seq"', 2, false);
ALTER SEQUENCE "public"."business_id_seq"
OWNED BY "public"."business"."id";
SELECT setval('"public"."business_id_seq"', 2, false);
ALTER SEQUENCE "public"."comments_id_seq"
OWNED BY "public"."comments"."id";
SELECT setval('"public"."comments_id_seq"', 7, true);
ALTER SEQUENCE "public"."companies_id_seq"
OWNED BY "public"."companies"."id";
SELECT setval('"public"."companies_id_seq"', 2, false);
ALTER SEQUENCE "public"."emoticon_id_seq"
OWNED BY "public"."emoticons"."id";
SELECT setval('"public"."emoticon_id_seq"', 2, false);
ALTER SEQUENCE "public"."files_id_seq"
OWNED BY "public"."files"."id";
SELECT setval('"public"."files_id_seq"', 6, true);
ALTER SEQUENCE "public"."fk_user_to_post_id_seq"
OWNED BY "public"."posts"."id";
SELECT setval('"public"."fk_user_to_post_id_seq"', 15, true);
ALTER SEQUENCE "public"."fk_user_to_post_user_id_seq"
OWNED BY "public"."posts"."user_id";
SELECT setval('"public"."fk_user_to_post_user_id_seq"', 2, false);
ALTER SEQUENCE "public"."post_like_id_seq"
OWNED BY "public"."post_like"."id";
SELECT setval('"public"."post_like_id_seq"', 7, true);
ALTER SEQUENCE "public"."post_register_id_seq"
OWNED BY "public"."post_register"."id";
SELECT setval('"public"."post_register_id_seq"', 7, true);
ALTER SEQUENCE "public"."post_share_id_seq"
OWNED BY "public"."post_share"."id";
SELECT setval('"public"."post_share_id_seq"', 7, true);
ALTER SEQUENCE "public"."post_view_id_seq"
OWNED BY "public"."post_view"."id";
SELECT setval('"public"."post_view_id_seq"', 4, true);
ALTER SEQUENCE "public"."skills_id_seq"
OWNED BY "public"."skills"."id";
SELECT setval('"public"."skills_id_seq"', 2, false);
ALTER SEQUENCE "public"."user_access_token_id_seq"
OWNED BY "public"."user_access_token"."id";
SELECT setval('"public"."user_access_token_id_seq"', 5, true);
ALTER SEQUENCE "public"."user_follow_id_seq"
OWNED BY "public"."user_follow"."id";
SELECT setval('"public"."user_follow_id_seq"', 2, false);
ALTER SEQUENCE "public"."user_skill_id_seq"
OWNED BY "public"."user_skill"."id";
SELECT setval('"public"."user_skill_id_seq"', 2, false);
ALTER SEQUENCE "public"."users_id_seq"
OWNED BY "public"."users"."id";
SELECT setval('"public"."users_id_seq"', 6, true);

-- ----------------------------
-- Primary Key structure for table ability_cards
-- ----------------------------
ALTER TABLE "public"."ability_cards" ADD CONSTRAINT "ability_cards_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table business
-- ----------------------------
ALTER TABLE "public"."business" ADD CONSTRAINT "business_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table comment_like
-- ----------------------------
ALTER TABLE "public"."comment_like" ADD CONSTRAINT "comment_like_pkey" PRIMARY KEY ("comment_id", "user_id");

-- ----------------------------
-- Primary Key structure for table comments
-- ----------------------------
ALTER TABLE "public"."comments" ADD CONSTRAINT "comments_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table companies
-- ----------------------------
ALTER TABLE "public"."companies" ADD CONSTRAINT "companies_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table emoticons
-- ----------------------------
ALTER TABLE "public"."emoticons" ADD CONSTRAINT "emoticon_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table files
-- ----------------------------
ALTER TABLE "public"."files" ADD CONSTRAINT "files_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table post_ability_card
-- ----------------------------
ALTER TABLE "public"."post_ability_card" ADD CONSTRAINT "post_ability_card_pkey" PRIMARY KEY ("post_id", "ability_card_id");

-- ----------------------------
-- Primary Key structure for table post_file
-- ----------------------------
ALTER TABLE "public"."post_file" ADD CONSTRAINT "post_file_pkey" PRIMARY KEY ("post_id", "file_id");

-- ----------------------------
-- Primary Key structure for table post_like
-- ----------------------------
ALTER TABLE "public"."post_like" ADD CONSTRAINT "post_like_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table post_register
-- ----------------------------
ALTER TABLE "public"."post_register" ADD CONSTRAINT "post_register_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table post_share
-- ----------------------------
ALTER TABLE "public"."post_share" ADD CONSTRAINT "post_share_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table post_view
-- ----------------------------
ALTER TABLE "public"."post_view" ADD CONSTRAINT "post_view_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table posts
-- ----------------------------
ALTER TABLE "public"."posts" ADD CONSTRAINT "fk_user_to_post_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table skills
-- ----------------------------
ALTER TABLE "public"."skills" ADD CONSTRAINT "skills_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table user_access_token
-- ----------------------------
ALTER TABLE "public"."user_access_token" ADD CONSTRAINT "user_access_token_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table user_business
-- ----------------------------
ALTER TABLE "public"."user_business" ADD CONSTRAINT "user_business_pkey" PRIMARY KEY ("user_id", "business_id");

-- ----------------------------
-- Primary Key structure for table user_company
-- ----------------------------
ALTER TABLE "public"."user_company" ADD CONSTRAINT "user_company_pkey" PRIMARY KEY ("user_id", "company_id");

-- ----------------------------
-- Primary Key structure for table user_follow
-- ----------------------------
ALTER TABLE "public"."user_follow" ADD CONSTRAINT "user_follow_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table user_skill
-- ----------------------------
ALTER TABLE "public"."user_skill" ADD CONSTRAINT "user_skill_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table users
-- ----------------------------
ALTER TABLE "public"."users" ADD CONSTRAINT "users_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Foreign Keys structure for table ability_cards
-- ----------------------------
ALTER TABLE "public"."ability_cards" ADD CONSTRAINT "ability_cards_creator_id_fkey" FOREIGN KEY ("creator_id") REFERENCES "public"."users" ("id") ON DELETE SET NULL ON UPDATE CASCADE;
ALTER TABLE "public"."ability_cards" ADD CONSTRAINT "ability_cards_owner_id_fkey" FOREIGN KEY ("owner_id") REFERENCES "public"."users" ("id") ON DELETE SET NULL ON UPDATE CASCADE;
ALTER TABLE "public"."ability_cards" ADD CONSTRAINT "ability_cards_user_skill_id_fkey" FOREIGN KEY ("user_skill_id") REFERENCES "public"."user_skill" ("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Keys structure for table comment_like
-- ----------------------------
ALTER TABLE "public"."comment_like" ADD CONSTRAINT "comment_like_comment_id_fkey" FOREIGN KEY ("comment_id") REFERENCES "public"."comments" ("id") ON DELETE SET NULL ON UPDATE CASCADE;
ALTER TABLE "public"."comment_like" ADD CONSTRAINT "comment_like_user_id_fkey" FOREIGN KEY ("user_id") REFERENCES "public"."users" ("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Keys structure for table comments
-- ----------------------------
ALTER TABLE "public"."comments" ADD CONSTRAINT "comments_original_id_fkey" FOREIGN KEY ("original_id") REFERENCES "public"."comments" ("id") ON DELETE SET NULL ON UPDATE CASCADE;
ALTER TABLE "public"."comments" ADD CONSTRAINT "comments_parent_id_fkey" FOREIGN KEY ("parent_id") REFERENCES "public"."comments" ("id") ON DELETE SET NULL ON UPDATE CASCADE;
ALTER TABLE "public"."comments" ADD CONSTRAINT "comments_post_id_fkey" FOREIGN KEY ("post_id") REFERENCES "public"."posts" ("id") ON DELETE SET NULL ON UPDATE CASCADE;
ALTER TABLE "public"."comments" ADD CONSTRAINT "comments_user_id_fkey" FOREIGN KEY ("user_id") REFERENCES "public"."users" ("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Keys structure for table companies
-- ----------------------------
ALTER TABLE "public"."companies" ADD CONSTRAINT "companies_business_registration_cert_fkey" FOREIGN KEY ("business_registration_cert") REFERENCES "public"."files" ("id") ON DELETE SET NULL ON UPDATE CASCADE;
ALTER TABLE "public"."companies" ADD CONSTRAINT "companies_logo_fkey" FOREIGN KEY ("logo") REFERENCES "public"."files" ("id") ON DELETE SET NULL ON UPDATE CASCADE;
ALTER TABLE "public"."companies" ADD CONSTRAINT "companies_owner_id_fkey" FOREIGN KEY ("owner_id") REFERENCES "public"."users" ("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Keys structure for table emoticons
-- ----------------------------
ALTER TABLE "public"."emoticons" ADD CONSTRAINT "emoticon_image_fkey" FOREIGN KEY ("image") REFERENCES "public"."files" ("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Keys structure for table post_ability_card
-- ----------------------------
ALTER TABLE "public"."post_ability_card" ADD CONSTRAINT "post_ability_card_ability_card_id_fkey" FOREIGN KEY ("ability_card_id") REFERENCES "public"."ability_cards" ("id") ON DELETE SET NULL ON UPDATE CASCADE;
ALTER TABLE "public"."post_ability_card" ADD CONSTRAINT "post_ability_card_post_id_fkey" FOREIGN KEY ("post_id") REFERENCES "public"."posts" ("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Keys structure for table post_file
-- ----------------------------
ALTER TABLE "public"."post_file" ADD CONSTRAINT "post_file_file_id_fkey" FOREIGN KEY ("file_id") REFERENCES "public"."files" ("id") ON DELETE SET NULL ON UPDATE CASCADE;
ALTER TABLE "public"."post_file" ADD CONSTRAINT "post_file_post_id_fkey" FOREIGN KEY ("post_id") REFERENCES "public"."posts" ("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Keys structure for table post_like
-- ----------------------------
ALTER TABLE "public"."post_like" ADD CONSTRAINT "post_like_post_id_fkey" FOREIGN KEY ("post_id") REFERENCES "public"."posts" ("id") ON DELETE SET NULL ON UPDATE CASCADE;
ALTER TABLE "public"."post_like" ADD CONSTRAINT "post_like_user_id_fkey" FOREIGN KEY ("user_id") REFERENCES "public"."users" ("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Keys structure for table post_register
-- ----------------------------
ALTER TABLE "public"."post_register" ADD CONSTRAINT "post_register_post_id_fkey" FOREIGN KEY ("post_id") REFERENCES "public"."posts" ("id") ON DELETE SET NULL ON UPDATE CASCADE;
ALTER TABLE "public"."post_register" ADD CONSTRAINT "post_register_user_id_fkey" FOREIGN KEY ("user_id") REFERENCES "public"."users" ("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Keys structure for table post_share
-- ----------------------------
ALTER TABLE "public"."post_share" ADD CONSTRAINT "post_share_post_id_fkey" FOREIGN KEY ("post_id") REFERENCES "public"."posts" ("id") ON DELETE SET NULL ON UPDATE CASCADE;
ALTER TABLE "public"."post_share" ADD CONSTRAINT "post_share_user_id_fkey" FOREIGN KEY ("user_id") REFERENCES "public"."users" ("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Keys structure for table post_view
-- ----------------------------
ALTER TABLE "public"."post_view" ADD CONSTRAINT "post_view_post_id_fkey" FOREIGN KEY ("post_id") REFERENCES "public"."posts" ("id") ON DELETE SET NULL ON UPDATE CASCADE;
ALTER TABLE "public"."post_view" ADD CONSTRAINT "post_view_user_id_fkey" FOREIGN KEY ("user_id") REFERENCES "public"."users" ("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Keys structure for table posts
-- ----------------------------
ALTER TABLE "public"."posts" ADD CONSTRAINT "fk_user_to_post_user_id_fkey" FOREIGN KEY ("user_id") REFERENCES "public"."users" ("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Keys structure for table user_business
-- ----------------------------
ALTER TABLE "public"."user_business" ADD CONSTRAINT "user_business_business_id_fkey" FOREIGN KEY ("business_id") REFERENCES "public"."business" ("id") ON DELETE SET NULL ON UPDATE CASCADE;
ALTER TABLE "public"."user_business" ADD CONSTRAINT "user_business_user_id_fkey" FOREIGN KEY ("user_id") REFERENCES "public"."users" ("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Keys structure for table user_company
-- ----------------------------
ALTER TABLE "public"."user_company" ADD CONSTRAINT "user_company_company_id_fkey" FOREIGN KEY ("company_id") REFERENCES "public"."companies" ("id") ON DELETE SET NULL ON UPDATE CASCADE;
ALTER TABLE "public"."user_company" ADD CONSTRAINT "user_company_user_id_fkey" FOREIGN KEY ("user_id") REFERENCES "public"."users" ("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Keys structure for table user_skill
-- ----------------------------
ALTER TABLE "public"."user_skill" ADD CONSTRAINT "user_skill_skill_id_fkey" FOREIGN KEY ("skill_id") REFERENCES "public"."skills" ("id") ON DELETE SET NULL ON UPDATE CASCADE;
ALTER TABLE "public"."user_skill" ADD CONSTRAINT "user_skill_user_id_fkey" FOREIGN KEY ("user_id") REFERENCES "public"."users" ("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Keys structure for table users
-- ----------------------------
ALTER TABLE "public"."users" ADD CONSTRAINT "users_cover_pic_fkey" FOREIGN KEY ("cover_pic") REFERENCES "public"."files" ("id") ON DELETE SET NULL ON UPDATE CASCADE;
ALTER TABLE "public"."users" ADD CONSTRAINT "users_name_card_back_fkey" FOREIGN KEY ("name_card_back") REFERENCES "public"."files" ("id") ON DELETE SET NULL ON UPDATE CASCADE;
ALTER TABLE "public"."users" ADD CONSTRAINT "users_name_card_front_fkey" FOREIGN KEY ("name_card_front") REFERENCES "public"."files" ("id") ON DELETE SET NULL ON UPDATE CASCADE;
ALTER TABLE "public"."users" ADD CONSTRAINT "users_profile_pic_fkey" FOREIGN KEY ("profile_pic") REFERENCES "public"."files" ("id") ON DELETE SET NULL ON UPDATE CASCADE;
