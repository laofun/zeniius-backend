DROP MATERIALIZED VIEW user_search;

ALTER TABLE users
  DROP  first_name,
  DROP  middle_name,
  DROP  last_name,
  DROP name_card_back,
  DROP name_card_front,
  ADD COLUMN full_name varchar(255),
  ADD COLUMN is_verify BOOLEAN DEFAULT FALSE,
  ADD COLUMN verify_at TIMESTAMP,
  ADD COLUMN description TEXT ;


CREATE MATERIALIZED VIEW user_search AS
  SELECT u.id, to_tsvector(COALESCE(u.username, '')) || to_tsvector(COALESCE(u.full_name, '')) ||
               to_tsvector(COALESCE(u.email, '')) || to_tsvector(COALESCE(u.phone_number, '')) || to_tsvector(COALESCE(string_agg(c.name, ' '), ''))
               || to_tsvector(COALESCE(string_agg(c.description, ' '), '')) || to_tsvector(COALESCE(string_agg(s.description, ' '), '')) ||
               to_tsvector(COALESCE(string_agg(s.name, ' '), '')) || to_tsvector(COALESCE(string_agg(p.title, ' '), ''))
               || to_tsvector(COALESCE(string_agg(p.description, ' '), ''))
    as docs
  FROM users u
    left join user_company uc on u.id = uc.user_id
    left join companies c on c.id = uc.company_id
    left join user_skill us on u.id = us.user_id
    left join skills s on s.id = us.skill_id
    left join posts p on u.id = p.user_id
  GROUP BY u.id;