ALTER TABLE companies
  ADD COLUMN phone_number varchar(20),
  ADD COLUMN email varchar(50),
  ADD COLUMN facebook_link varchar(50),
  ADD COLUMN website varchar(50),
  ADD COLUMN name_card_front int8,
  ADD COLUMN name_card_back int8;