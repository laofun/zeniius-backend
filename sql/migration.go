package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"

	"firebase.google.com/go"
	"google.golang.org/api/option"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func main() {

	ctx := context.Background()
	conf := &firebase.Config{
		DatabaseURL: "https://gng-production.firebaseio.com",
	}
	// Fetch the service account key JSON file contents
	opt := option.WithCredentialsFile("tmp/zennius-key.json")

	// Initialize the app with a service account, granting admin privileges
	app, err := firebase.NewApp(ctx, conf, opt)
	if err != nil {
		log.Fatalln("Error initializing app:", err)
	}

	client, err := app.Database(ctx)
	if err != nil {
		log.Fatalln("Error initializing database client:", err)
	}

	// As an admin, the app has access to read and write all data, regradless of Security Rules
	ref := client.NewRef("career/NN00001")
	var data map[string]interface{}
	if err := ref.Get(ctx, &data); err != nil {
		log.Fatalln("Error reading from database:", err)
	}
	fmt.Println(data)

	jsonString, err := json.Marshal(data)
	fmt.Println(string(jsonString))
}
