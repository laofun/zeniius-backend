/*
 Navicat PostgreSQL Data Transfer

 Source Server         : Localhost Postgres
 Source Server Type    : PostgreSQL
 Source Server Version : 100004
 Source Host           : localhost:5432
 Source Catalog        : zenniius
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 100004
 File Encoding         : 65001

 Date: 10/07/2018 18:19:04
*/


-- ----------------------------
-- Sequence structure for business_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."business_id_seq";
CREATE SEQUENCE "public"."business_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for companies_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."companies_id_seq";
CREATE SEQUENCE "public"."companies_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for files_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."files_id_seq";
CREATE SEQUENCE "public"."files_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for fk_user_to_post_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."fk_user_to_post_id_seq";
CREATE SEQUENCE "public"."fk_user_to_post_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for fk_user_to_post_user_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."fk_user_to_post_user_id_seq";
CREATE SEQUENCE "public"."fk_user_to_post_user_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for post_like_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."post_like_id_seq";
CREATE SEQUENCE "public"."post_like_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for post_register_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."post_register_id_seq";
CREATE SEQUENCE "public"."post_register_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for post_share_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."post_share_id_seq";
CREATE SEQUENCE "public"."post_share_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for post_view_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."post_view_id_seq";
CREATE SEQUENCE "public"."post_view_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for users_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."users_id_seq";
CREATE SEQUENCE "public"."users_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Table structure for business
-- ----------------------------
DROP TABLE IF EXISTS "public"."business";
CREATE TABLE "public"."business" (
  "id" int8 NOT NULL DEFAULT nextval('business_id_seq'::regclass),
  "title" varchar(512) COLLATE "pg_catalog"."default",
  "description" text COLLATE "pg_catalog"."default",
  "created_at" timestamp(0),
  "updated_at" timestamp(0),
  "deleted_at" timestamp(0)
)
;
ALTER TABLE "public"."business" OWNER TO "ironsight";

-- ----------------------------
-- Table structure for companies
-- ----------------------------
DROP TABLE IF EXISTS "public"."companies";
CREATE TABLE "public"."companies" (
  "id" int8 NOT NULL DEFAULT nextval('companies_id_seq'::regclass),
  "name" varchar(512) COLLATE "pg_catalog"."default",
  "owner_id" int8,
  "description" text COLLATE "pg_catalog"."default",
  "contact" varchar(255) COLLATE "pg_catalog"."default",
  "address" varchar(255) COLLATE "pg_catalog"."default",
  "business_registration_cert" int8,
  "created_at" timestamp(0),
  "updated_at" timestamp(0),
  "deleted_at" timestamp(0),
  "logo" int8
)
;
ALTER TABLE "public"."companies" OWNER TO "ironsight";

-- ----------------------------
-- Table structure for files
-- ----------------------------
DROP TABLE IF EXISTS "public"."files";
CREATE TABLE "public"."files" (
  "id" int8 NOT NULL DEFAULT nextval('files_id_seq'::regclass),
  "filename" varchar(512) COLLATE "pg_catalog"."default",
  "mime_type" varchar(255) COLLATE "pg_catalog"."default",
  "extension" varchar(255) COLLATE "pg_catalog"."default",
  "full_path" varchar(512) COLLATE "pg_catalog"."default",
  "type" varchar(20) COLLATE "pg_catalog"."default",
  "size" int8,
  "width" int8,
  "height" int8,
  "thumbnail" varchar(255) COLLATE "pg_catalog"."default",
  "created_at" timestamp(0),
  "updated_at" timestamp(0),
  "deleted_at" timestamp(0)
)
;
ALTER TABLE "public"."files" OWNER TO "ironsight";

-- ----------------------------
-- Table structure for post_file
-- ----------------------------
DROP TABLE IF EXISTS "public"."post_file";
CREATE TABLE "public"."post_file" (
  "post_id" int8 NOT NULL,
  "file_id" int8 NOT NULL,
  "created_at" timestamp(0),
  "deleted_at" timestamp(0)
)
;
ALTER TABLE "public"."post_file" OWNER TO "ironsight";

-- ----------------------------
-- Table structure for post_like
-- ----------------------------
DROP TABLE IF EXISTS "public"."post_like";
CREATE TABLE "public"."post_like" (
  "id" int8 NOT NULL DEFAULT nextval('post_like_id_seq'::regclass),
  "post_id" int8,
  "created_at" timestamp(0),
  "updated_at" timestamp(0),
  "deleted_at" timestamp(0),
  "user_id" int8
)
;
ALTER TABLE "public"."post_like" OWNER TO "ironsight";

-- ----------------------------
-- Table structure for post_register
-- ----------------------------
DROP TABLE IF EXISTS "public"."post_register";
CREATE TABLE "public"."post_register" (
  "id" int8 NOT NULL DEFAULT nextval('post_register_id_seq'::regclass),
  "post_id" int8,
  "count" int8,
  "created_at" timestamp(0),
  "user_id" int8,
  "updated_at" timestamp(0),
  "deleted_at" timestamp(0)
)
;
ALTER TABLE "public"."post_register" OWNER TO "ironsight";

-- ----------------------------
-- Table structure for post_share
-- ----------------------------
DROP TABLE IF EXISTS "public"."post_share";
CREATE TABLE "public"."post_share" (
  "id" int8 NOT NULL DEFAULT nextval('post_share_id_seq'::regclass),
  "post_id" int8,
  "created_at" timestamp(0),
  "updated_at" timestamp(0),
  "deleted_at" timestamp(0),
  "user_id" int8
)
;
ALTER TABLE "public"."post_share" OWNER TO "ironsight";

-- ----------------------------
-- Table structure for post_view
-- ----------------------------
DROP TABLE IF EXISTS "public"."post_view";
CREATE TABLE "public"."post_view" (
  "id" int8 NOT NULL DEFAULT nextval('post_view_id_seq'::regclass),
  "post_id" int8,
  "user_id" int8,
  "created_at" timestamp(0)
)
;
ALTER TABLE "public"."post_view" OWNER TO "ironsight";

-- ----------------------------
-- Table structure for posts
-- ----------------------------
DROP TABLE IF EXISTS "public"."posts";
CREATE TABLE "public"."posts" (
  "id" int8 NOT NULL DEFAULT nextval('fk_user_to_post_id_seq'::regclass),
  "user_id" int8 NOT NULL DEFAULT nextval('fk_user_to_post_user_id_seq'::regclass),
  "type" varchar(20) COLLATE "pg_catalog"."default",
  "title" varchar(512) COLLATE "pg_catalog"."default",
  "description" text COLLATE "pg_catalog"."default",
  "created_at" timestamp(0),
  "updated_at" timestamp(0),
  "deleted_at" timestamp(0),
  "fields" jsonb
)
;
ALTER TABLE "public"."posts" OWNER TO "ironsight";

-- ----------------------------
-- Table structure for user_business
-- ----------------------------
DROP TABLE IF EXISTS "public"."user_business";
CREATE TABLE "public"."user_business" (
  "user_id" int8 NOT NULL,
  "business_id" int8 NOT NULL,
  "created_at" timestamp(0)
)
;
ALTER TABLE "public"."user_business" OWNER TO "ironsight";

-- ----------------------------
-- Table structure for user_company
-- ----------------------------
DROP TABLE IF EXISTS "public"."user_company";
CREATE TABLE "public"."user_company" (
  "user_id" int8 NOT NULL,
  "company_id" int8 NOT NULL,
  "created_at" timestamp(0)
)
;
ALTER TABLE "public"."user_company" OWNER TO "ironsight";

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS "public"."users";
CREATE TABLE "public"."users" (
  "id" int8 NOT NULL DEFAULT nextval('users_id_seq'::regclass),
  "first_name" varchar(255) COLLATE "pg_catalog"."default",
  "middle_name" varchar(255) COLLATE "pg_catalog"."default",
  "last_name" varchar(255) COLLATE "pg_catalog"."default",
  "birthday" date,
  "phone_number" varchar(20) COLLATE "pg_catalog"."default",
  "username" varchar(255) COLLATE "pg_catalog"."default",
  "profile_pic" int8,
  "email" varchar(255) COLLATE "pg_catalog"."default",
  "created_at" timestamp(0),
  "updated_at" timestamp(0),
  "deleted_at" timestamp(0),
  "cover_pic" int8,
  "facebook_link" varchar(255) COLLATE "pg_catalog"."default",
  "name_card_front" int8,
  "name_card_back" int8
)
;
ALTER TABLE "public"."users" OWNER TO "ironsight";

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."business_id_seq"
OWNED BY "public"."business"."id";
SELECT setval('"public"."business_id_seq"', 2, false);
ALTER SEQUENCE "public"."companies_id_seq"
OWNED BY "public"."companies"."id";
SELECT setval('"public"."companies_id_seq"', 2, false);
ALTER SEQUENCE "public"."files_id_seq"
OWNED BY "public"."files"."id";
SELECT setval('"public"."files_id_seq"', 2, false);
ALTER SEQUENCE "public"."fk_user_to_post_id_seq"
OWNED BY "public"."posts"."id";
SELECT setval('"public"."fk_user_to_post_id_seq"', 2, false);
ALTER SEQUENCE "public"."fk_user_to_post_user_id_seq"
OWNED BY "public"."posts"."user_id";
SELECT setval('"public"."fk_user_to_post_user_id_seq"', 2, false);
ALTER SEQUENCE "public"."post_like_id_seq"
OWNED BY "public"."post_like"."id";
SELECT setval('"public"."post_like_id_seq"', 2, false);
ALTER SEQUENCE "public"."post_register_id_seq"
OWNED BY "public"."post_register"."id";
SELECT setval('"public"."post_register_id_seq"', 2, false);
ALTER SEQUENCE "public"."post_share_id_seq"
OWNED BY "public"."post_share"."id";
SELECT setval('"public"."post_share_id_seq"', 2, false);
ALTER SEQUENCE "public"."post_view_id_seq"
OWNED BY "public"."post_view"."id";
SELECT setval('"public"."post_view_id_seq"', 2, false);
ALTER SEQUENCE "public"."users_id_seq"
OWNED BY "public"."users"."id";
SELECT setval('"public"."users_id_seq"', 2, false);

-- ----------------------------
-- Primary Key structure for table business
-- ----------------------------
ALTER TABLE "public"."business" ADD CONSTRAINT "business_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table companies
-- ----------------------------
ALTER TABLE "public"."companies" ADD CONSTRAINT "companies_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table files
-- ----------------------------
ALTER TABLE "public"."files" ADD CONSTRAINT "files_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table post_file
-- ----------------------------
ALTER TABLE "public"."post_file" ADD CONSTRAINT "post_file_pkey" PRIMARY KEY ("post_id", "file_id");

-- ----------------------------
-- Primary Key structure for table post_like
-- ----------------------------
ALTER TABLE "public"."post_like" ADD CONSTRAINT "post_like_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table post_register
-- ----------------------------
ALTER TABLE "public"."post_register" ADD CONSTRAINT "post_register_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table post_share
-- ----------------------------
ALTER TABLE "public"."post_share" ADD CONSTRAINT "post_share_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table post_view
-- ----------------------------
ALTER TABLE "public"."post_view" ADD CONSTRAINT "post_view_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table posts
-- ----------------------------
ALTER TABLE "public"."posts" ADD CONSTRAINT "fk_user_to_post_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table user_business
-- ----------------------------
ALTER TABLE "public"."user_business" ADD CONSTRAINT "user_business_pkey" PRIMARY KEY ("user_id", "business_id");

-- ----------------------------
-- Primary Key structure for table user_company
-- ----------------------------
ALTER TABLE "public"."user_company" ADD CONSTRAINT "user_company_pkey" PRIMARY KEY ("user_id", "company_id");

-- ----------------------------
-- Primary Key structure for table users
-- ----------------------------
ALTER TABLE "public"."users" ADD CONSTRAINT "users_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Foreign Keys structure for table companies
-- ----------------------------
ALTER TABLE "public"."companies" ADD CONSTRAINT "companies_business_registration_cert_fkey" FOREIGN KEY ("business_registration_cert") REFERENCES "public"."files" ("id") ON DELETE SET NULL ON UPDATE CASCADE;
ALTER TABLE "public"."companies" ADD CONSTRAINT "companies_logo_fkey" FOREIGN KEY ("logo") REFERENCES "public"."files" ("id") ON DELETE SET NULL ON UPDATE CASCADE;
ALTER TABLE "public"."companies" ADD CONSTRAINT "companies_owner_id_fkey" FOREIGN KEY ("owner_id") REFERENCES "public"."users" ("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Keys structure for table post_file
-- ----------------------------
ALTER TABLE "public"."post_file" ADD CONSTRAINT "post_file_file_id_fkey" FOREIGN KEY ("file_id") REFERENCES "public"."files" ("id") ON DELETE SET NULL ON UPDATE CASCADE;
ALTER TABLE "public"."post_file" ADD CONSTRAINT "post_file_post_id_fkey" FOREIGN KEY ("post_id") REFERENCES "public"."posts" ("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Keys structure for table post_like
-- ----------------------------
ALTER TABLE "public"."post_like" ADD CONSTRAINT "post_like_post_id_fkey" FOREIGN KEY ("post_id") REFERENCES "public"."posts" ("id") ON DELETE SET NULL ON UPDATE CASCADE;
ALTER TABLE "public"."post_like" ADD CONSTRAINT "post_like_user_id_fkey" FOREIGN KEY ("user_id") REFERENCES "public"."users" ("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Keys structure for table post_register
-- ----------------------------
ALTER TABLE "public"."post_register" ADD CONSTRAINT "post_register_post_id_fkey" FOREIGN KEY ("post_id") REFERENCES "public"."posts" ("id") ON DELETE SET NULL ON UPDATE CASCADE;
ALTER TABLE "public"."post_register" ADD CONSTRAINT "post_register_user_id_fkey" FOREIGN KEY ("user_id") REFERENCES "public"."users" ("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Keys structure for table post_share
-- ----------------------------
ALTER TABLE "public"."post_share" ADD CONSTRAINT "post_share_post_id_fkey" FOREIGN KEY ("post_id") REFERENCES "public"."posts" ("id") ON DELETE SET NULL ON UPDATE CASCADE;
ALTER TABLE "public"."post_share" ADD CONSTRAINT "post_share_user_id_fkey" FOREIGN KEY ("user_id") REFERENCES "public"."users" ("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Keys structure for table post_view
-- ----------------------------
ALTER TABLE "public"."post_view" ADD CONSTRAINT "post_view_post_id_fkey" FOREIGN KEY ("post_id") REFERENCES "public"."posts" ("id") ON DELETE SET NULL ON UPDATE CASCADE;
ALTER TABLE "public"."post_view" ADD CONSTRAINT "post_view_user_id_fkey" FOREIGN KEY ("user_id") REFERENCES "public"."users" ("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Keys structure for table posts
-- ----------------------------
ALTER TABLE "public"."posts" ADD CONSTRAINT "fk_user_to_post_user_id_fkey" FOREIGN KEY ("user_id") REFERENCES "public"."users" ("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Keys structure for table user_business
-- ----------------------------
ALTER TABLE "public"."user_business" ADD CONSTRAINT "user_business_business_id_fkey" FOREIGN KEY ("business_id") REFERENCES "public"."business" ("id") ON DELETE SET NULL ON UPDATE CASCADE;
ALTER TABLE "public"."user_business" ADD CONSTRAINT "user_business_user_id_fkey" FOREIGN KEY ("user_id") REFERENCES "public"."users" ("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Keys structure for table user_company
-- ----------------------------
ALTER TABLE "public"."user_company" ADD CONSTRAINT "user_company_company_id_fkey" FOREIGN KEY ("company_id") REFERENCES "public"."companies" ("id") ON DELETE SET NULL ON UPDATE CASCADE;
ALTER TABLE "public"."user_company" ADD CONSTRAINT "user_company_user_id_fkey" FOREIGN KEY ("user_id") REFERENCES "public"."users" ("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Keys structure for table users
-- ----------------------------
ALTER TABLE "public"."users" ADD CONSTRAINT "users_cover_pic_fkey" FOREIGN KEY ("cover_pic") REFERENCES "public"."files" ("id") ON DELETE SET NULL ON UPDATE CASCADE;
ALTER TABLE "public"."users" ADD CONSTRAINT "users_name_card_back_fkey" FOREIGN KEY ("name_card_back") REFERENCES "public"."files" ("id") ON DELETE SET NULL ON UPDATE CASCADE;
ALTER TABLE "public"."users" ADD CONSTRAINT "users_name_card_front_fkey" FOREIGN KEY ("name_card_front") REFERENCES "public"."files" ("id") ON DELETE SET NULL ON UPDATE CASCADE;
ALTER TABLE "public"."users" ADD CONSTRAINT "users_profile_pic_fkey" FOREIGN KEY ("profile_pic") REFERENCES "public"."files" ("id") ON DELETE SET NULL ON UPDATE CASCADE;
