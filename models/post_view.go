package models

import (
	"github.com/jinzhu/gorm"
	"time"
)

type PostView struct {
	ID        int64      `json:"id" gorm:"primary_key"`
	PostID    int64      `json:"post_id" gorm:"column:post_id"`
	UserID    int64      `json:"user_id" gorm:"column:user_id"`
	CreatedAt *time.Time `json:"created_at" gorm:"column:created_at"`
}

func (PostView) TableName() string {
	return "post_view"
}

func UpdatePostView(db *gorm.DB, postID int64, userID int64) error {

	if CheckUserViewedPost(db, postID, userID) {
		return nil
	}

	now := time.Now()
	postView := PostView{
		PostID:    postID,
		UserID:    userID,
		CreatedAt: &now,
	}

	tx := db.Begin()
	defer func() {
		if r := recover(); r != nil {
			tx.Rollback()
		}
	}()

	err := tx.Save(&postView).Error
	if err != nil {
		tx.Rollback()
		return err
	}

	err = db.Model(Post{}).
		Where("id = ?", postID).
		Update("view_count", gorm.Expr("view_count + 1")).
		Error
	if err != nil {
		tx.Rollback()
		return err
	}

	return tx.Commit().Error
}

func CheckUserViewedPost(db *gorm.DB, postID int64, userID int64) bool {
	count := 0
	err := db.Model(PostView{}).Where(PostView{PostID: postID, UserID: userID}).Count(&count).Error
	if err != nil {
		return true
	}
	return count != 0
}
