package models

import (
	"database/sql"

	"github.com/jinzhu/gorm"
)

// GetCurrentFriendRelationship get status of relationship between 2 user id
func GetCurrentFriendRelationship(db *gorm.DB, userID int64, friendID int64) (string, error) {
	var status sql.NullString
	query := `SELECT fr.status
		FROM friend_relationship fr
		INNER JOIN
			(
				SELECT user_id, friend_id, MAX(created_at) created_at
				FROM friend_relationship
				GROUP BY user_id, friend_id
			) sub 
		ON fr.user_id = sub.user_id AND fr.friend_id = sub.friend_id AND fr.created_at = sub.created_at 
		WHERE fr.user_id = $1 AND fr.friend_id = $2`

	row := db.Raw(query, userID, friendID).Row()
	err := row.Scan(&status)
	if err != nil {
		if err == sql.ErrNoRows {
			return "none", nil
		}
		return "", err
	}

	if status.Valid {
		return status.String, nil
	}
	return "none", nil
}

// ListReceivedFriendRequest ..
func (u User) ListReceivedFriendRequest(db *gorm.DB) ([]int64, string, error) {
	var friendID int64
	var friendIDList []int64

	query := `SELECT fr.user_id
	FROM friend_relationship fr
	INNER JOIN
		(
			SELECT user_id, friend_id, MAX(created_at) created_at
			FROM friend_relationship
			GROUP BY user_id, friend_id
		) sub 
	ON fr.user_id = sub.user_id AND fr.friend_id = sub.friend_id AND fr.created_at = sub.created_at AND fr.friend_id = $1
	WHERE fr.status = 'pending'`

	rows, err := db.Raw(query, u.ID).Rows()
	if err != nil {
		return nil, "", err
	}
	defer rows.Close()

	for rows.Next() {
		rows.Scan(&friendID)
		friendIDList = append(friendIDList, friendID)
	}
	return friendIDList, "", err
}

// ListSentFriendRequest ..
func (u User) ListSentFriendRequest(db *gorm.DB) ([]int64, string, error) {
	var friendID int64
	var friendIDList []int64

	query := `SELECT fr.user_id
	FROM friend_relationship fr
	INNER JOIN
		(
			SELECT user_id, friend_id, MAX(created_at) created_at
			FROM friend_relationship
			GROUP BY user_id, friend_id
		) sub 
	ON fr.user_id = sub.user_id AND fr.friend_id = sub.friend_id AND fr.created_at = sub.created_at AND fr.user_id = $1
	WHERE fr.status = 'pending'`

	rows, err := db.Raw(query, u.ID).Rows()
	if err != nil {
		return nil, "", err
	}
	defer rows.Close()

	for rows.Next() {
		rows.Scan(&friendID)
		friendIDList = append(friendIDList, friendID)
	}
	return friendIDList, "", err
}

// ListMyFriend ..
func (u User) ListMyFriend(db *gorm.DB) ([]int64, string, error) {
	var friendID int64
	var friendIDList []int64

	query := `SELECT fr.friend_id
	FROM friend_relationship fr
	INNER JOIN
		(
			SELECT user_id, friend_id, MAX(created_at) created_at
			FROM friend_relationship
			GROUP BY user_id, friend_id
		) sub 
	ON fr.user_id = sub.user_id AND fr.friend_id = sub.friend_id AND fr.created_at = sub.created_at
	WHERE fr.status = 'befriended' AND fr.user_id = $1`

	rows, err := db.Raw(query, u.ID).Rows()
	if err != nil {
		return nil, "", err
	}
	defer rows.Close()

	for rows.Next() {
		rows.Scan(&friendID)
		friendIDList = append(friendIDList, friendID)
	}
	return friendIDList, "", err
}
