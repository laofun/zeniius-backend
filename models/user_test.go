package models

import (
	"testing"

	_ "github.com/jinzhu/gorm/dialects/postgres"
	"gitlab.com/rockship/backend/zeniius-backend/utils"
)

//
//func parseJsonData(data interface{}) string {
//	jsonString, err := json.MarshalIndent(data, "", "  ")
//	if err != nil {
//		print(err)
//		return ""
//	}
//	return string(jsonString)
//}

func TestUserProfile_GetUserProfiles(t *testing.T) {

	db := GetTestDbConnection()

	var listUserProfile []User
	users := []int64{1, 2, 3}
	listUserProfile, err := GetMultiUserByID(db, users)

	if err != nil {
		t.Error(err)
	}

	utils.DebugDump(listUserProfile)
	print("\n")

}

//func TestUserProfile_GetUserProfile(t *testing.T) {
//	db := GetTestDbConnection()
//
//	var useID = int64(2)
//	user := User{}
//
//	err := user.GetUserProfile(db, useID)
//
//	if err != nil {
//		t.Error(err)
//	}
//
//	print(parseJsonData(user))
//	print("\n")
//}
//
//func TestUserProfile_GetFollowingUser(t *testing.T) {
//	db := GetTestDbConnection()
//	user := UserProfile{}
//
//	followingUsers, err := user.GetFollowingUser(db, 1)
//
//	if err != nil {
//		t.Error(err)
//	}
//
//	print(parseJsonData(followingUsers))
//	print("\n")
//}
