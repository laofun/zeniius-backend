package models

import (
	"github.com/jinzhu/gorm"
	"gitlab.com/rockship/backend/zeniius-backend/utils"
	"time"
)

type PostRegister struct {
	ID        int64      `json:"id" gorm:"primary_key"`
	PostID    int64      `json:"post_id" gorm:"column:post_id"`
	UserID    int64      `json:"user_id" gorm:"column:user_id"`
	CreatedAt time.Time  `json:"created_at" gorm:"column:created_at"`
	DeletedAt *time.Time `json:"deleted_at" gorm:"column:deleted_at"`
}

func (PostRegister) TableName() string {
	return "post_register"
}

func RegisterPost(db *gorm.DB, postID int64, userID int64) error {
	var l PostRegister
	result := db.Where("post_id = ? AND user_id = ? AND deleted_at IS NULL", postID, userID).
		First(&l)
	if !result.RecordNotFound() {
		return utils.ResourceExisted{
			ObjectType: "post_register",
			ObjectID:   l.ID,
		}
	}
	err := db.Create(&PostRegister{
		PostID: postID,
		UserID: userID,
	}).Error
	return err
}

func UnRegisterPost(db *gorm.DB, postID int64, userID int64) error {
	var l PostRegister
	result := db.Where("post_id = ? AND user_id = ? AND deleted_at IS NULL", postID, userID).
		First(&l)
	if result.RecordNotFound() {
		return utils.ResourceNotFound{
			ObjectType: "post_register",
			ObjectID:   l.ID,
		}
	}
	err := db.Where("post_id = ? AND user_id = ?", postID, userID).
		Delete(&PostRegister{}).Error
	return err
}

func (o *Post) ListRegisteredUser(db *gorm.DB) ([]User, error) {
	var users []User
	err := db.Preload("Avatar").Where("deleted_at is NULL AND is_verify = true AND id in (?)",
		db.Table("post_register").Select("user_id").Where("deleted_at IS NULL AND post_id = ?", o.ID).QueryExpr()).
		Find(&users).Error
	return users, err
}
