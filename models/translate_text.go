package models

import (
	"github.com/jinzhu/gorm"
	"github.com/sirupsen/logrus"
	"gitlab.com/rockship/backend/zeniius-backend/service"
	"gitlab.com/rockship/backend/zeniius-backend/utils"
	"time"
)

const (
	TranslateObjectComment         = "comment"
	TranslateObjectPostDescription = "post_description"
	TranslateObjectPostTitle       = "post_title"
)

type TranslateText struct {
	ID           int64     `json:"id" gorm:"primary_key"`
	ObjectType   string    `json:"object_type" gorm:"column:object_type"`
	ObjectID     int64     `json:"object_id" gorm:"column:object_id"`
	Content      string    `json:"content" gorm:"column:translated_text"`
	Locale       string    `json:"locale" gorm:"column:locale"`
	OriginalHash string    `json:"original_hash" gorm:"column:original_hash"`
	CreatedAt    time.Time `json:"created_at" gorm:"column:created_at"`
	UpdatedAt    time.Time `json:"updated_at" gorm:"column:updated_at"`
}

func (TranslateText) TableName() string {
	return "translate_text"
}

func GetTranslateObject(db *gorm.DB, objectType string, objectID int64, locale string) (TranslateText, error) {
	var text TranslateText
	result := db.Where(TranslateText{
		ObjectType: objectType,
		ObjectID:   objectID,
		Locale:     locale,
	}).First(&text)
	if result.RecordNotFound() {
		return text, utils.ResourceNotFound{
			ObjectType: objectType,
			ObjectID:   objectID,
		}
	}
	if result.Error != nil {
		return text, result.Error
	}
	return text, nil
}

func CreateTranslateObject(db *gorm.DB, objectType string, objectID int64, originalHash, translatedText, locale string) error {
	newText := TranslateText{
		ObjectID:   objectID,
		ObjectType: objectType,
		Locale:     locale,
	}
	return db.Where(newText).Assign(TranslateText{Content: translatedText, OriginalHash: originalHash}).
		FirstOrCreate(&newText).Error
}

func TranslateObjectText(db *gorm.DB, objectType string, objectID int64, locale string) (string, error) {
	var translatedText string
	var err error
	switch objectType {
	case TranslateObjectComment:
		translatedText, err = translateComment(db, objectID, locale)
		break
	default:
		translatedText = ""
		err = nil
	}
	return translatedText, err
}

func translateComment(db *gorm.DB, objectID int64, locale string) (string, error) {
	comment, err := GetComment(db, objectID)
	if err != nil {
		return "", err
	}
	return RetrieveOrInitTranslateObjectContent(db, TranslateObjectComment, objectID, comment.Content, comment.Locale, locale)
}

func RetrieveOrInitTranslateObjectContent(db *gorm.DB, objectType string, objectID int64, objectContent string, objectLocale string, locale string) (string, error) {
	if objectLocale == locale {
		return objectContent, nil
	}
	text, err := GetTranslateObject(db, objectType, objectID, locale)
	originalHash := utils.Sha1Hash(objectContent)

	if err != nil && !utils.IsNotFound(err) {
		return "", err
	}
	if err == nil && originalHash == text.OriginalHash {
		return text.Content, nil
	}

	translatedText, err := service.TranslateText(objectContent, locale)
	if err != nil {
		return "", err
	}
	go CreateTranslateObject(db, objectType, objectID, originalHash, translatedText, locale)
	return translatedText, nil
}

func TranslateTextAndSave(db *gorm.DB, objectType string, objectId int64, text string, locale string) (string, error) {
	translatedTitle, err := service.TranslateText(text, locale)
	if err != nil {
		return "", err
	}
	originalHash := utils.Sha1Hash(text)
	err = CreateTranslateObject(db, objectType, objectId, originalHash, translatedTitle, locale)
	if err != nil {
		return "", err
	}
	return translatedTitle, nil
}

func (o *Post) SaveTranslate(db *gorm.DB, currentLocale string) error {
	if o.Locale == currentLocale {
		return nil
	}
	_, err := TranslateTextAndSave(db, TranslateObjectPostTitle, o.ID, o.Title, currentLocale)
	if err != nil {
		logrus.Error("translate and save error", err)
		return err
	}
	_, err = TranslateTextAndSave(db, TranslateObjectPostDescription, o.ID, o.Description, currentLocale)
	if err != nil {
		logrus.Error("translate and save error", err)
		return err
	}
	return nil
}

func (o *Post) GetTranslateContent(db *gorm.DB, locale string) error {
	translateDescription, err := RetrieveOrInitTranslateObjectContent(db, TranslateObjectPostDescription, o.ID, o.Description, o.Locale, locale)
	if err != nil {
		return err
	}
	o.OriginalDescription = o.Description
	o.Description = translateDescription

	translateTitle, err := RetrieveOrInitTranslateObjectContent(db, TranslateObjectPostTitle, o.ID, o.Title, o.Locale, locale)
	if err != nil {
		return err
	}
	o.OriginalTitle = o.Title
	o.Title = translateTitle
	return nil
}
