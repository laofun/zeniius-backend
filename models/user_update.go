package models

import (
	"github.com/jinzhu/gorm"
)

func (u *User) UpdateUser(db *gorm.DB) error {
	err := db.Save(&u).Error
	if err != nil {
		return err
	}
	return nil
}

func (u *User) UpdateUserFields(db *gorm.DB, fields map[string]interface{}) error {
	err := db.Model(&u).Updates(fields).Error
	return err
}
