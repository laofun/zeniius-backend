package models

import (
	"errors"
	"github.com/jinzhu/gorm"
	"strings"
	"time"
)

// Business
// swagger:response Business
type Business struct {
	ID          int64      `json:"id" gorm:"primary_key"`
	Title       string     `json:"title" gorm:"column:title"`
	Description string     `json:"description" gorm:"description"`
	CreatedAt   *time.Time `json:"created_at" gorm:"column:created_at"`
}

type UserBusiness struct {
	UserID     int64      `json:"user_id" gorm:"primary_key;column:user_id"`
	User       *User      `json:"user"`
	BusinessID int64      `json:"business_id" gorm:"primary_key;column:business_id"`
	Business   *Business  `json:"business"`
	CreatedAt  *time.Time `json:"created_at" gorm:"column:created_at"`
}

type CompanyBusiness struct {
	ID          int64      `json:"id" gorm:"primary_key"`
	CompanyID   int64      `json:"company_id" gorm:"column:company_id"`
	Business    *Business  `json:"business" gorm:"foreignKey:BusinessID"`
	BusinessID  int64      `json:"-" gorm:"column:business_id"`
	Description string     `json:"content" gorm:"column:description"`
	DeletedAt   *time.Time `json:"-" gorm:"column:deleted_at"`
}

type PostBusiness struct {
	ID         int64      `json:"id" gorm:"primary_key"`
	PostID     int64      `json:"post_id" gorm:"column:post_id"`
	BusinessID int64      `json:"business_id" gorm:"column:business_id"`
	Business   *Business  `json:"business"`
	CreatedAt  *time.Time `json:"created_at" gorm:"column:created_at"`
}

func (Business) TableName() string {
	return "business"
}

func (UserBusiness) TableName() string {
	return "user_business"
}

func (CompanyBusiness) TableName() string {
	return "company_business"
}

func (PostBusiness) TableName() string {
	return "post_business"
}

func (b *Business) GetList(db *gorm.DB) ([]Business, error) {
	var listBusiness []Business
	err := db.Where("deleted_at IS NULL").Find(&listBusiness).Error
	if err != nil {
		return []Business{}, err
	}
	return listBusiness, nil
}

func AddUserBusinessList(db *gorm.DB, userId int64, listBusiness []int64) error {
	sqlStr := "INSERT INTO user_business(user_id, business_id, created_at) VALUES "
	vals := []interface{}{}
	const rowSQL = "(?, ?, NOW())"
	var inserts []string

	for _, businessID := range listBusiness {
		inserts = append(inserts, rowSQL)
		vals = append(vals, userId, businessID)
	}
	sqlStr = sqlStr + strings.Join(inserts, ",")

	if err := db.Exec(sqlStr, vals...).Error; err != nil {
		return err
	}
	return nil
}

func GetCompanyBusiness(db *gorm.DB, ID int64) (CompanyBusiness, error) {
	var c CompanyBusiness
	err := db.Preload("Business").Where("id = ?", ID).First(&c).Error
	return c, err
}

func GetBusinessByCompanyID(db *gorm.DB, companyID int64) ([]CompanyBusiness, error) {
	var c []CompanyBusiness
	err := db.Preload("Business").Where("company_id = ?", companyID).Find(&c).Error
	return c, err
}

func CreateCompanyBusiness(db *gorm.DB, companyID int64, businessID int64, description string) (CompanyBusiness, error) {
	var c CompanyBusiness
	if CheckBusiness(db, businessID) == false {
		return c, errors.New("business not found")
	}
	err := db.Where(CompanyBusiness{BusinessID: businessID, CompanyID: companyID}).
		Assign(CompanyBusiness{Description: description}).
		FirstOrCreate(&c).Error
	if err != nil {
		return c, err
	}
	return GetCompanyBusiness(db, c.ID)
}

func DeleteCompanyBusinessNotInList(db *gorm.DB, companyID int64, businessID []int64) error {
	err := db.Where("company_id = ? AND business_id NOT IN (?)", companyID, businessID).
		Delete(&CompanyBusiness{}).Error
	return err
}

func CheckBusiness(db *gorm.DB, businessID int64) bool {
	business := Business{
		ID: businessID,
	}
	err := db.Where("id = ?", businessID).First(&business).Error
	if err != nil {
		return false
	}
	return true
}

func GetListBusinessIDsOfUser(db *gorm.DB, userID int64) ([]int64, error) {
	var businessIDs []int64
	err := db.Model(UserBusiness{}).Where("user_id = ?", userID).
		Pluck("business_id", &businessIDs).Error
	return businessIDs, err
}

func GetListBusinessIDsOfPost(db *gorm.DB, postID int64) ([]int64, error) {
	var businessIDs []int64
	err := db.Model(&PostBusiness{}).Where("post_id = ?", postID).
		Pluck("business_id", &businessIDs).Error
	return businessIDs, err
}

func (o *Post) DeletePostBusiness(db *gorm.DB, businessIDs []int64) error {
	if err := db.Where("post_id = ? and business_id IN (?)", o.ID, businessIDs).
		Delete(PostBusiness{}).Error; err != nil {
		return err
	}
	return nil
}
