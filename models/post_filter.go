package models

import (
	"github.com/jinzhu/gorm"
	"gitlab.com/rockship/backend/zeniius-backend/utils"
)

// Get user group chat
// @Todo: refactor this, calling directly to chat table
func GetUserGroupChat(db *gorm.DB, userID int64) ([]string, error) {
	type Result struct {
		ID string
	}
	var result []Result
	err := db.Raw(`
		SELECT c.id as ID  from conversation c
		JOIN access_control a on c.id = a."conversationId"
		WHERE c.type = 'group' and a."userId" = ?
		GROUP BY c.id
		`, userID).Scan(&result).Error

	if err != nil {
		return nil, err
	}
	var groupIDs []string
	for _, value := range result {
		groupIDs = append(groupIDs, value.ID)
	}
	return groupIDs, err
}

func GetUserGroupFilter(db *gorm.DB, userID int64, userInputGroup []string) ([]string, bool, error) {
	userGroupIDs, err := GetUserGroupChat(db, userID)
	if err != nil {
		return nil, false, err
	}
	if userInputGroup == nil || len(userInputGroup) == 0 {
		return userGroupIDs, false, nil
	}
	var filterGroupIDs []string
	for _, value := range userInputGroup {
		if utils.InStringArray(userGroupIDs, value) {
			filterGroupIDs = append(filterGroupIDs, value)
		}
	}
	return filterGroupIDs, true, nil
}

func GetUserBusinessFilter(db *gorm.DB, userID int64, userInputBusiness []int64) ([]int64, bool, error) {
	businessID, err := GetListBusinessIDsOfUser(db, userID)
	if err != nil {
		return nil, false, err
	}

	if userInputBusiness == nil || len(userInputBusiness) == 0 {
		return businessID, false, nil
	}

	var filterBusinessID []int64
	for _, value := range userInputBusiness {
		if utils.InIntArray(businessID, value) {
			filterBusinessID = append(filterBusinessID, value)
		}
	}
	return filterBusinessID, true, nil
}

func GetListPostIDFilterWithBusiness(db *gorm.DB, userID int64, demand []string, businessIDs []int64, groupIDs []string, page int, limit int) ([]int64, error) {

	filter := "no_filter"
	filterGroupId, isGroupFilter, err := GetUserGroupFilter(db, userID, groupIDs)
	if err != nil {
		return nil, err
	}
	if isGroupFilter {
		filter = "filter_group"
	}

	filterBusinessID, _, err := GetUserBusinessFilter(db, userID, businessIDs)
	var postIDs []int64
	err = db.Order("posts.created_at desc").Limit(limit).Offset((page-1)*limit).Table("posts").Select("posts.id as post_id").
		Joins("inner join post_business on posts.id = post_business.post_id").
		Where("posts.demand_type in (?)", demand).
		Where(`post_business.business_id in (?) AND posts.deleted_at IS NULL AND
		((
		CASE
				WHEN 'no_filter' = ? THEN is_public = true or
				posts.id IN (
				SELECT
					post_id 
				FROM
					post_group 
				WHERE
				group_id IN (?)) 
				WHEN 'filter_group' = ? THEN
				posts.id IN (
				SELECT
					post_id 
				FROM
					post_group 
				WHERE
				group_id IN (?)) 
				ELSE FALSE 
			END 
			) = TRUE)
		`, filterBusinessID, filter, filterGroupId, filter, filterGroupId).Group("posts.id").Pluck("post_id", &postIDs).Error
	if err != nil {
		return nil, err
	}
	return postIDs, nil
}

func GetListPostIDPublicWithFilter(db *gorm.DB, userID int64, demand []string, businessIDs []int64, groupIDs []string, page int, limit int) ([]int64, error) {

	filter := "no_filter"
	filterGroupId, isGroupFilter, err := GetUserGroupFilter(db, userID, groupIDs)
	if err != nil {
		return nil, err
	}
	if isGroupFilter {
		filter = "filter_group"
	}

	isBusinessFilter := false
	if businessIDs != nil && len(businessIDs) > 0 {
		isBusinessFilter = true
	}
	filterBusinessID := businessIDs

	if filter == "no_filter" && isBusinessFilter {
		filter = "filter_business"
	} else if filter == "filter_group" && isBusinessFilter {
		filter = "filter_group_business"
	}

	var postIDs []int64
	err = db.Order("posts.created_at desc").Limit(limit).Offset((page-1)*limit).Table("posts").Select("posts.id as post_id").
		Where("posts.demand_type in (?)", demand).
		Where(`posts.deleted_at IS NULL AND
		((
		CASE
				WHEN 'no_filter' = ? THEN is_public = true or
				posts.id IN (
				SELECT
					post_id 
				FROM
					post_group 
				WHERE
				group_id IN (?)) 
				WHEN 'filter_group' = ? THEN
				posts.id IN (
				SELECT
					post_id 
				FROM
					post_group 
				WHERE
				group_id IN (?))
				WHEN 'filter_group_business' = ? THEN
				posts.id IN (
				SELECT
					post_id 
				FROM
					post_group 
				WHERE
				group_id IN (?))
				AND posts.id IN (
				SELECT post_id
				FROM
					post_business
				WHERE business_id IN (?))
				WHEN 'filter_business' = ? THEN
				is_public = true and posts.id IN (
				SELECT
					post_id 
				FROM
					post_business 
				WHERE
				business_id IN (?))
				ELSE FALSE 
			END 
			) = TRUE)
		`, filter, filterGroupId, filter, filterGroupId, filter, filterGroupId, filterBusinessID, filter, filterBusinessID).Group("posts.id").Pluck("post_id", &postIDs).Error
	if err != nil {
		return nil, err
	}
	return postIDs, nil
}

func GetListPostIDFilterWithFollowing(db *gorm.DB, userID int64, demand []string, businessIDs []int64, groupIDs []string, page int, limit int) ([]int64, error) {

	filter := "no_filter"
	filterGroupId, isGroupFilter, err := GetUserGroupFilter(db, userID, groupIDs)
	if err != nil {
		return nil, err
	}
	if isGroupFilter {
		filter = "filter_group"
	}

	isBusinessFilter := false
	if businessIDs != nil && len(businessIDs) > 0 {
		isBusinessFilter = true
	}
	filterBusinessID := businessIDs

	if filter == "no_filter" && isBusinessFilter {
		filter = "filter_business"
	} else if filter == "filter_group" && isBusinessFilter {
		filter = "filter_group_business"
	}

	var postIDs []int64
	err = db.Order("posts.created_at desc").Limit(limit).Offset((page-1)*limit).Table("posts").Select("posts.id as post_id").
		Joins("inner join follow_relationships on posts.user_id = follow_relationships.user_id").
		Where("posts.demand_type in (?) AND follow_relationships.follower_id = ?", demand, userID).
		Where(`posts.deleted_at IS NULL AND
		((
		CASE
				WHEN 'no_filter' = ? THEN is_public = true or
				posts.id IN (
				SELECT
					post_id 
				FROM
					post_group 
				WHERE
				group_id IN (?)) 
				WHEN 'filter_group' = ? THEN
				posts.id IN (
				SELECT
					post_id 
				FROM
					post_group 
				WHERE
				group_id IN (?))
				WHEN 'filter_group_business' = ? THEN
				posts.id IN (
				SELECT
					post_id 
				FROM
					post_group 
				WHERE
				group_id IN (?))
				AND posts.id IN (
				SELECT post_id
				FROM
					post_business
				WHERE business_id IN (?))
				WHEN 'filter_business' = ? THEN
				is_public = true and posts.id IN (
				SELECT
					post_id 
				FROM
					post_business 
				WHERE
				business_id IN (?))
				ELSE FALSE 
			END 
			) = TRUE)
		`, filter, filterGroupId, filter, filterGroupId, filter, filterGroupId, filterBusinessID, filter, filterBusinessID).Group("posts.id").Pluck("post_id", &postIDs).Error
	if err != nil {
		return nil, err
	}
	return postIDs, nil
}
