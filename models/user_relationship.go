package models

import (
	"github.com/jinzhu/gorm"

	"github.com/sirupsen/logrus"
)

// UserRelationship ..
type UserRelationship struct {
	User
	IsFriend      bool `json:"is_friend"`
	IsSentRequest bool `json:"is_sent_request"`
	IsFollow      bool `json:"is_follow"`
}

// GetUserRelationshipByID ..
func GetUserRelationshipByID(db *gorm.DB, userID int64, relationshipUserID int64) (UserRelationship, error) {
	var userExtend UserRelationship
	var isSentRequest, isFriend, isFollow bool
	logrus.Info("id ", userID, relationshipUserID)

	user, err := GetUserByID(db, userID)
	if err != nil {
		return userExtend, nil
	}

	myUser, err := GetUserByID(db, relationshipUserID)
	if err != nil {
		return userExtend, nil
	}

	user.GetExtraUserData(db)

	userIDs, _, err := myUser.ListMyFriend(db)
	if err != nil {
		return userExtend, err
	}
	logrus.Info("ListAllFriend", userIDs)
	for _, id := range userIDs {
		if id == userID {
			isFriend = true
		}
	}

	userIDs, _, err = user.ListReceivedFriendRequest(db)
	if err != nil {
		return userExtend, err
	}
	logrus.Info("ListAllFriendRequest", userIDs)
	for _, id := range userIDs {
		if id == myUser.ID {
			isSentRequest = true
		}
	}

	userIDs, err = myUser.GetAllFollowingID(db)
	if err != nil {
		return userExtend, err
	}
	logrus.Info("GetAllFollowing", userIDs)
	for _, id := range userIDs {
		if id == userID {
			isFollow = true
		}
	}

	userExtendInfo := UserRelationship{
		User:          user,
		IsFriend:      isFriend,
		IsFollow:      isFollow,
		IsSentRequest: isSentRequest,
	}

	return userExtendInfo, nil
}
