package models

import (
	"time"

	"github.com/jinzhu/gorm"
	"github.com/sirupsen/logrus"
	"gopkg.in/guregu/null.v3"
)

type File struct {
	ID         int64       `json:"id" gorm:"primary_key"`
	FullPath   string      `json:"fullPath" gorm:"column:full_path"`
	Size       int64       `json:"size"`
	Width      int64       `json:"width"`
	Height     int64       `json:"height"`
	Type       string      `json:"type"`
	Thumbnail  null.String `json:"thumbnail"`
	Filename   string      `json:"filename"`
	MimeType   string      `json:"mime_type" gorm:"column:mime_type"`
	Extension  string      `json:"extension"`
	CreatedAt  time.Time   `json:"created_at" gorm:"column:created_at"`
	BucketPath string      `json:"-" gorm:"column:bucket_path"`
}

type PostFile struct {
	PostID    int64      `json:"post_id" gorm:"primary_key;column:post_id"`
	FileID    int64      `json:"file_id" gorm:"primary_key;column:file_id"`
	CreatedAt *time.Time `json:"created_at" gorm:"column:created_at"`
}

func (PostFile) TableName() string {
	return "post_file"
}

func (f *File) Save(db *gorm.DB) error {
	err := db.Create(&f).Error

	if err != nil {
		return err
	}

	return nil

}

func GetFileFullPath(db *gorm.DB, id int64) (string, error) {
	var file File
	err := db.Where("id = ?", id).First(&file).Error
	if err != nil {
		return "", err
	}
	return file.FullPath, nil
}

func CheckFileExist(db *gorm.DB, id int64) bool {
	var file File
	err := db.Where("id = ?", id).First(&file).Error
	if err != nil {
		logrus.Error("check file error: ", err)
		return false
	}
	return true
}

func CheckFileOfPost(db *gorm.DB, fileID int64, postID int64) bool {
	var file PostFile
	err := db.Where("file_id = ? AND post_id = ?", fileID, postID).First(&file).Error
	if err != nil {
		return false
	}
	return true
}

func (o *Post) CountPostFile(db *gorm.DB) (int, error) {
	var count int
	err := db.Model(PostFile{}).Where("post_id = ?", o.ID).Count(&count).Error
	if err != nil {
		return 0, err
	}
	return count, nil
}
