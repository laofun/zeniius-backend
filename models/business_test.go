package models

import (
	"testing"

	"gitlab.com/rockship/backend/zeniius-backend/utils"
)

func TestAddUserBusinessList(t *testing.T) {
	db := GetTestDbConnection()
	business := []int64{1, 2, 3}
	err := AddUserBusinessList(db, 1, business)
	if err != nil {
		t.Error(err)
	}
}

func TestCreateCompanyBusiness(t *testing.T) {
	db := GetTestDbConnection()
	c, err := CreateCompanyBusiness(db, 7, 1, "test description")
	if err != nil {
		t.Error(err)
	}
	utils.DebugDump(c)
}
