package models

import (
	"time"

	"github.com/jinzhu/gorm"
)

// FollowRelationship ..
type FollowRelationship struct {
	ID int64 `json:"id" gorm:"primary_key"`

	UserID     int64 `json:"-"`
	User       *User `json:"-" gorm:"foreignkey:UserID"`
	FollowerID int64 `json:"follower_id"`
	Follower   *User `json:"follower" gorm:"foreignkey:FollowerID"`

	IsRemoved bool `json:"is_removed" gorm:"default:false"`
	CreatedAt time.Time
	RemovedAt time.Time
}

// AddFollowing make user's ID follow follower's ID
func (u User) AddFollowing(db *gorm.DB, followerID int64) (*FollowRelationship, error) {
	var follow FollowRelationship
	if !db.Where("user_id=?", u.ID).Where("follower_id=?", followerID).Where("is_removed=?", false).First(&follow).RecordNotFound() {
		return &follow, nil
	}

	follow = FollowRelationship{UserID: u.ID, FollowerID: followerID}
	err := db.Create(&follow).Error
	if err != nil {
		return nil, err
	}
	return &follow, nil
}

// GetAllFollowingID retrieve all follower
func (u User) GetAllFollowingID(db *gorm.DB) ([]int64, error) {
	var followers []FollowRelationship
	var followerIDs []int64

	if err := db.Where("user_id = ?", u.ID).Where("is_removed=?", false).Find(&followers).Error; err != nil {
		return nil, err
	}

	for _, id := range followers {
		followerIDs = append(followerIDs, id.FollowerID)
	}

	friendList, _, err := u.ListMyFriend(db)
	if err != nil {
		return nil, err
	}

	for _, id := range friendList {
		followerIDs = append(followerIDs, id)
	}

	friendList, _, err = u.ListSentFriendRequest(db)
	if err != nil {
		return nil, err
	}

	for _, id := range friendList {
		followerIDs = append(followerIDs, id)
	}

	return followerIDs, nil
}

// GetAllFollowerID retrieve all follower
func (u User) GetAllFollowerID(db *gorm.DB) ([]int64, error) {
	var followers []FollowRelationship
	var followerIDs []int64

	if err := db.Where("follower_id = ?", u.ID).Where("is_removed=?", false).Find(&followers).Error; err != nil {
		return nil, err
	}

	friendList, _, err := u.ListMyFriend(db)
	if err != nil {
		return nil, err
	}

	for _, id := range followers {
		followerIDs = append(followerIDs, id.FollowerID)
	}

	for _, id := range friendList {
		followerIDs = append(followerIDs, id)
	}
	return followerIDs, nil
}

// RemoveFollowingID a user
func (u User) RemoveFollowingID(db *gorm.DB, followerID int64) error {
	err := db.Table("follow_relationships").Where("user_id = ? AND follower_id = ?", u.ID, followerID).Update("is_removed", true).Error
	if err != nil {
		return err
	}
	return nil
}

func (u User) GetAllFollowingProfile(db *gorm.DB) ([]FollowRelationship, error) {
	var followers []FollowRelationship

	err := db.Where("follower_id = ?", u.ID).Preload("User").Find(&followers).Error
	if err != nil {
		return nil, err
	}
	return followers, err
}
