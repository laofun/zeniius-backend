package models

import (
	"os"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

var (
	postgresURI string
	DB          *gorm.DB
)

func init() {
	postgresURI = os.Getenv("POSTGRES_URI")
	var err error
	DB, err = gorm.Open("postgres", postgresURI)
	DB.LogMode(true)
	DB.Set("gorm:auto_preload", true)
	if err != nil {
		panic(err)
	}
}
