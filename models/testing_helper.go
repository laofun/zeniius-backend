package models

import (
	"os"

	"github.com/jinzhu/gorm"

	_ "github.com/jinzhu/gorm/dialects/postgres"
)

func GetTestDbConnection() *gorm.DB {
	postgresURI := os.Getenv("POSTGRES_URI")
	db, err := gorm.Open("postgres", postgresURI)
	if err != nil {
		panic(err)
	}
	db.LogMode(true)
	db.Set("gorm:auto_preload", true)
	return db
}
