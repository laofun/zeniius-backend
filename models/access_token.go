package models

import (
	"math/rand"
	"time"

	"github.com/jinzhu/gorm"
	"github.com/jinzhu/gorm/dialects/postgres"
)

type AccessToken struct {
	ID         int64          `json:"-" gorm:"primary_key"`
	Platform   string         `json:"platform"`
	Token      string         `json:"access_token" gorm:"column:access_token"`
	IPAddress  string         `json:"ip_address" gorm:"column:ip_address"`
	DeviceInfo postgres.Jsonb `json:"device_info"`
	ExpiredAt  time.Time      `json:"expired_at"`
	ExpiredIn  int32          `json:"expired_in"`
	CreatedAt  time.Time      `json:"created_at"`
	User       *User          `json:"user" gorm:"foreignkey:UserID"`
	UserID     int64          `json:"-" gorm:"column:user_id"`
}

func (AccessToken) TableName() string {
	return "user_access_token"
}

func (t *AccessToken) Save(db *gorm.DB) error {
	err := db.Create(&t).Error
	return err
}

var src = rand.NewSource(time.Now().UnixNano())

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
const (
	letterIdxBits = 6                    // 6 bits to represent a letter index
	letterIdxMask = 1<<letterIdxBits - 1 // All 1-bits, as many as letterIdxBits
	letterIdxMax  = 63 / letterIdxBits   // # of letter indices fitting in 63 bits
)

const AccessTokenDuration = 2592000

func RandString(n int) string {
	b := make([]byte, n)
	// A src.Int63() generates 63 random bits, enough for letterIdxMax characters!
	for i, cache, remain := n-1, src.Int63(), letterIdxMax; i >= 0; {
		if remain == 0 {
			cache, remain = src.Int63(), letterIdxMax
		}
		if idx := int(cache & letterIdxMask); idx < len(letterBytes) {
			b[i] = letterBytes[idx]
			i--
		}
		cache >>= letterIdxBits
		remain--
	}
	return string(b)
}

func (t *AccessToken) CheckAccessToken(db *gorm.DB, token string) bool {
	err := db.Preload("User").Where("access_token = ? AND expired_at > NOW()", token).Find(&t).Error
	if err != nil {
		return false
	}
	return true
}
