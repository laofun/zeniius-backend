package models

import (
	"github.com/jinzhu/gorm"

	"strings"

	"github.com/sirupsen/logrus"
)

// UserLimit ..
const UserLimit = 3

// SearchUser ..
func SearchUser(db *gorm.DB, query string, page int) ([]User, error) {
	var ids []int64
	words := strings.Fields(query)
	searchQuery := ""
	for _, word := range words {
		searchQuery = searchQuery + word + "|"
	}
	searchQuery = strings.TrimSuffix(searchQuery, "|")
	offset := (page - 1) * UserLimit
	err := db.Raw(`
		SELECT id FROM user_search 
		WHERE docs @@ to_tsquery(unaccent(?)) OR literal_doc LIKE unaccent(?)
		ORDER BY ts_rank(docs, to_tsquery(unaccent(?))) DESC
		LIMIT ? OFFSET ?`, searchQuery, "%"+query+"%", searchQuery, UserLimit, offset).Pluck("id", &ids).Error
	if err != nil {
		return []User{}, err
	}

	userProfiles, err := GetMultiUserByID(db, ids)
	if err != nil {
		return []User{}, err
	}
	return userProfiles, nil
}

func RefreshUserSearchView(db *gorm.DB) {
	err := db.Exec("REFRESH MATERIALIZED VIEW user_search;").Error
	if err != nil {
		logrus.Error("refresh materialized view error: ", err)
	}
}
