package models

import (
	"time"

	"strings"

	"errors"

	"github.com/jinzhu/gorm"
	"github.com/jinzhu/gorm/dialects/postgres"
	"github.com/sirupsen/logrus"
	"gitlab.com/rockship/backend/zeniius-backend/dataType"
	"gitlab.com/rockship/backend/zeniius-backend/utils"
	"gopkg.in/guregu/null.v3"
)

// RawIntValue ..
type RawIntValue struct {
	Value int64
}

const PostFileLimit = 15

// Post ..
type Post struct {
	ID                  int64          `json:"id" gorm:"primary_key"`
	User                *User          `json:"user,omitempty"`
	UserId              int64          `json:"-" gorm:"column:user_id"`
	Type                string         `json:"type"`
	Title               string         `json:"title"`
	OriginalTitle       string         `json:"original_title" gorm:"-"`
	Description         string         `json:"description"`
	OriginalDescription string         `json:"original_description" gorm:"-"`
	CreatedAt           time.Time      `json:"created_at" gorm:"column:created_at"`
	Files               []File         `json:"files" gorm:"many2many:post_file"`
	ViewCount           int64          `json:"view_count" gorm:"view_count"`
	CommentCount        int64          `json:"comment_count" gorm:"-"`
	RegisterCount       int64          `json:"register_count" gorm:"-"`
	LikeCount           int64          `json:"like_count" gorm:"-"`
	ShareCount          int64          `json:"share_count" gorm:"-"`
	Fields              postgres.Jsonb `json:"fields"`
	Status              string         `json:"status" gorm:"column:status"`
	DemandType          string         `json:"demand_type" gorm:"column:demand_type"`
	Location            string         `json:"location" gorm:"column:location"`
	Lat                 null.Float     `json:"location_lat" gorm:"column:location_lat"`
	Long                null.Float     `json:"location_long" gorm:"column:location_long"`
	Comment             *Comment       `json:"comment" gorm:"-"`
	Liked               bool           `json:"liked" gorm:"-"`
	Registered          bool           `json:"registered" gorm:"-"`
	IsPublic            bool           `json:"is_public" gorm:"column:is_public"`
	EndTime             *time.Time     `json:"end_time" gorm:"column:end_time"`
	StartTime           *time.Time     `json:"start_time" gorm:"column:start_time"`
	Business            []Business     `json:"business" gorm:"many2many:post_business"`
	Group               []Group        `json:"group"`
	Locale              string         `json:"locale" gorm:"column:locale"`
	DeletedAt           *time.Time     `json:"-" gorm:"column:deleted_at"`
}

func (o *Post) GetPostExtraData(db *gorm.DB, userID int64) {
	go UpdatePostView(db, o.ID, userID)
	var share, like, comment, register RawIntValue
	// @TODO Refactor this: remove raw query
	postId := o.ID
	err := db.Raw(`SELECT COUNT(id) as value FROM post_like WHERE post_id = ? AND deleted_at IS NULL`, postId).Scan(&like).Error
	if err != nil {
		logrus.Error(err)
	}

	err = db.Raw(`SELECT COUNT(id) as value FROM post_register WHERE post_id = ? AND deleted_at IS NULL`, postId).Scan(&register).Error
	if err != nil {
		logrus.Error(err)
	}

	err = db.Raw(`SELECT COUNT(id) as value FROM comments WHERE post_id = ? AND deleted_at IS NULL`, postId).Scan(&comment).Error
	if err != nil {
		logrus.Error(err)
	}

	count := 0
	db.Model(&PostLike{}).Where("post_id = ? and user_id = ? and deleted_at is null", postId, userID).Count(&count)
	o.Liked = count == 1

	db.Model(&PostRegister{}).Where("post_id = ? and user_id = ? and deleted_at is null", postId, userID).Count(&count)
	o.Registered = count == 1

	o.CommentCount = comment.Value
	o.LikeCount = like.Value
	o.RegisterCount = register.Value
	o.ShareCount = share.Value

	var c Comment
	comments, err := c.GetCommentOfPost(db, o.ID, 1, 1, userID)
	if err == nil && len(comments) > 0 {
		o.Comment = &comments[0]
	}
	logrus.Error("Get comments in post: ", err)

}

// Newfeeds
// Related tab: filter post by business area of user
// Following tab: filter post by follower
// Public tab: all public post
func GetPostWithFilter(db *gorm.DB, filterType string, userID int64, demand []string,
	businessIDs []int64, groupIDs []string, page int, limit int, locale string) ([]Post, error) {
	var postIDs []int64
	var err error
	switch filterType {
	case "related":
		postIDs, err = GetListPostIDFilterWithBusiness(db, userID, demand, businessIDs, groupIDs, page, limit)
		break
	case "following":
		postIDs, err = GetListPostIDFilterWithFollowing(db, userID, demand, businessIDs, groupIDs, page, limit)
		break
	default:
		postIDs, err = GetListPostIDPublicWithFilter(db, userID, demand, businessIDs, groupIDs, page, limit)
		break
	}

	if err != nil {
		return nil, err
	}
	var listPost []Post
	err = db.Order("created_at desc").Preload("Business").
		Preload("Files").Preload("User").Preload("User.Avatar").
		Where("id in (?)", postIDs).
		Find(&listPost).Error

	if err != nil {
		return nil, err
	}
	for key, _ := range listPost {
		listPost[key].GetPostExtraData(db, userID)
		err = listPost[key].GetTranslateContent(db, locale)
		println(err)
	}
	return listPost, err
}

func (o *Post) AddBusiness(db *gorm.DB, business []int64) error {
	if business == nil || len(business) == 0 {
		return nil
	}

	sqlStr := "INSERT INTO post_business(post_id, business_id, created_at) VALUES "
	vals := []interface{}{}
	const rowSQL = "(?, ?, NOW())"
	var inserts []string

	for _, businessID := range business {
		inserts = append(inserts, rowSQL)
		vals = append(vals, o.ID, businessID)
	}
	sqlStr = sqlStr + strings.Join(inserts, ",")

	if err := db.Exec(sqlStr, vals...).Error; err != nil {
		return err
	}
	return nil
}

func (o *Post) InitFiles(db *gorm.DB, files []int64) error {

	if files == nil || len(files) == 0 {
		return nil
	}

	sqlStr := "INSERT INTO post_file(post_id, file_id, created_at) VALUES "
	vals := []interface{}{}
	const rowSQL = "(?, ?, NOW())"
	var inserts []string

	for _, fileID := range files {
		inserts = append(inserts, rowSQL)
		vals = append(vals, o.ID, fileID)
	}
	sqlStr = sqlStr + strings.Join(inserts, ",")

	if err := db.Exec(sqlStr, vals...).Error; err != nil {
		return err
	}
	return nil
}

func (o *Post) CreatePostPrivacy(db *gorm.DB, postPrivacy dataType.PostPrivacy) error {
	if postPrivacy.ShareGroup == nil ||
		len(postPrivacy.ShareGroup) == 0 {
		return nil
	}
	return o.AddGroup(db, postPrivacy.ShareGroup)
}

func (o *Post) Create(db *gorm.DB, business []int64, files []int64, postPrivacy dataType.PostPrivacy) error {

	tx := db.Begin()
	defer func() {
		if r := recover(); r != nil {
			tx.Rollback()
		}
	}()

	o.IsPublic = postPrivacy.IsPublic
	err := tx.Create(&o).Error
	if err != nil {
		tx.Rollback()
		return err
	}

	err = o.AddBusiness(tx, business)
	if err != nil {
		tx.Rollback()
		return err
	}

	err = o.InitFiles(tx, files)
	if err != nil {
		tx.Rollback()
		return err
	}

	err = o.CreatePostPrivacy(tx, postPrivacy)
	if err != nil {
		tx.Rollback()
		return err
	}

	tx.Preload("Business").
		Preload("Files").Where("id = ?", o.ID).
		First(&o)

	return tx.Commit().Error
}

func GetPost(db *gorm.DB, postID int64) (Post, error) {
	var p Post
	err := db.Where("id = ? and deleted_at IS NULL", postID).First(&p).Error
	return p, err
}

func GetPostDetail(db *gorm.DB, postID int64, userID int64, locale string) (Post, error) {
	var post Post
	err := db.Preload("Business").Preload("Files").Preload("User").Preload("User.Avatar").
		Where("deleted_at IS NULL and id = ?", postID).
		First(&post).Error
	if err != nil {
		return Post{}, utils.ResourceNotFound{
			ObjectType: "post",
			ObjectID:   postID,
		}
	}

	post.GetPostExtraData(db, userID)
	post.GetTranslateContent(db, locale)
	err = post.GetShareGroupDetail(db)
	if err != nil {
		return post, err
	}
	return post, nil
}

func (o *Post) UpdatePost(db *gorm.DB, businessIDs []int64, privacy dataType.PostPrivacy) error {
	tx := db.Begin()
	defer func() {
		if r := recover(); r != nil {
			tx.Rollback()
		}
	}()

	if tx.Error != nil {
		return tx.Error
	}
	err := o.UpdateGroup(tx, privacy.ShareGroup)
	if err != nil {
		tx.Rollback()
		return err
	}

	o.IsPublic = privacy.IsPublic
	err = tx.Save(o).Error
	if err != nil {
		tx.Rollback()
		return err
	}

	if err = o.UpdatePostBusiness(tx, businessIDs); err != nil {
		tx.Rollback()
		return err
	}
	return tx.Commit().Error
}

func (o *Post) UpdatePostBusiness(db *gorm.DB, businessIDs []int64) error {

	if businessIDs == nil || len(businessIDs) == 0 {
		return errors.New("business area can not be empty")
	}

	oldBusinessIDs, err := GetListBusinessIDsOfPost(db, o.ID)
	if err != nil {
		return err
	}
	var removedIDs, addedIDs []int64
	for _, oldID := range oldBusinessIDs {
		if !utils.InIntArray(businessIDs, oldID) {
			removedIDs = append(removedIDs, oldID)
		}
	}

	for _, newID := range businessIDs {
		if !utils.InIntArray(oldBusinessIDs, newID) {
			addedIDs = append(addedIDs, newID)
		}
	}

	err = o.AddBusiness(db, addedIDs)
	if err != nil {
		return err
	}

	err = o.DeletePostBusiness(db, removedIDs)
	if err != nil {
		return err
	}
	return nil
}

func (o *Post) AddPostFile(db *gorm.DB, fileID int64) error {
	if !CheckFileExist(db, fileID) {
		return utils.ResourceNotFound{
			ObjectType: "File",
			ObjectID:   fileID,
		}
	}

	if CheckFileOfPost(db, fileID, o.ID) {
		return utils.ResourceExisted{
			ObjectType: "File",
			ObjectID:   fileID,
		}
	}

	now := time.Now()
	file := PostFile{
		FileID:    fileID,
		PostID:    o.ID,
		CreatedAt: &now,
	}
	return db.Save(&file).Error
}

func (o *Post) DeletePostFile(db *gorm.DB, fileID int64) error {
	if !CheckFileExist(db, fileID) || !CheckFileOfPost(db, fileID, o.ID) {
		return utils.ResourceNotFound{
			ObjectType: "File",
			ObjectID:   fileID,
		}
	}
	return db.Where("file_id = ? AND post_id = ?", fileID, o.ID).Delete(PostFile{}).Error
}

func (o *Post) Delete(db *gorm.DB) error {
	return db.Where("id = ?", o.ID).Delete(Post{}).Error
}
