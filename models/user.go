package models

import (
	"time"

	"github.com/jinzhu/gorm"

	"gopkg.in/guregu/null.v3"
)

// User ..
type User struct {
	ID          int64      `json:"id" gorm:"primary_key"`
	FullName    string     `json:"full_name,omitempty" gorm:"column:full_name"`
	Avatar      *File      `json:"avatar" gorm:"foreignkey:ProfilePic"`
	ProfilePic  null.Int   `json:"-"`
	Cover       *File      `json:"cover" gorm:"foreignkey:CoverPic"`
	CoverPic    null.Int   `json:"-"`
	FirebaseID  string     `json:"firebase_id" gorm:"column:firebase_id"`
	Email       string     `json:"email" gorm:"column:email"`
	Phone       string     `json:"phone" gorm:"column:phone_number"`
	Birthday    *time.Time `json:"birthday" gorm:"column:birthday"`
	Facebook    string     `json:"facebook" gorm:"column:facebook_link"`
	IsVerify    null.Bool  `json:"is_verify" gorm:"column:is_verify"`
	Description string     `json:"description" gorm:"column:description"`

	UserBusiness []*UserBusiness `json:"-"`
	CompleteInfo null.Bool       `json:"complete_info" gorm:"column:complete_info"`
	Position     null.String     `json:"position" gorm:"-"`

	FriendCount    int64 `json:"friend_count" gorm:"-"`
	FollowCount    int64 `json:"follow_count" gorm:"-"`
	FollowingCount int64 `json:"following_count" gorm:"-"`
	HelpCount      int64 `json:"help_count" gorm:"-"`
}

// TableName ..
func (User) TableName() string {
	return "users"
}

// GetUserByID ..
func GetUserByID(db *gorm.DB, userID int64) (User, error) {
	var userProfile User

	err := db.Preload("Avatar").Preload("Cover").Where("id = ?", userID).First(&userProfile).Error
	if err != nil {
		return User{}, err
	}
	return userProfile, nil
}

// GetMultiUserByID ..
func GetMultiUserByID(db *gorm.DB, userIds []int64) ([]User, error) {
	var err error
	var listUserProfile []User

	err = db.Preload("Avatar").Preload("Cover").Where("id IN (?)", userIds).Find(&listUserProfile).Error
	if err != nil {
		return []User{}, err
	}
	return listUserProfile, nil
}

// GetAllUser ..
func GetAllUser(db *gorm.DB) ([]User, error) {
	var err error
	var listUserProfile []User

	err = db.Order("created_at desc, id desc").Preload("Avatar").Preload("Cover").
		Where("is_verify = true").Find(&listUserProfile).Error
	if err != nil {
		return nil, err
	}
	return listUserProfile, nil
}

// GetUserByFirebaseID ..
func GetUserByFirebaseID(db *gorm.DB, firebaseID, email string) (*User, error) {
	u := User{}

	err := db.Preload("Avatar").Where(User{FirebaseID: firebaseID}).
		Attrs(User{IsVerify: null.BoolFrom(false), CompleteInfo: null.BoolFrom(false)}).
		FirstOrCreate(&u).Error
	if err != nil {
		return nil, err
	}

	if u.Email == "" {
		u.Email = email
		err = db.Save(&u).Error
	}

	if err != nil {
		return nil, err
	}

	return &u, nil
}
