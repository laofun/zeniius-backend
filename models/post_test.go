package models

import (
	"gitlab.com/rockship/backend/zeniius-backend/utils"
	"testing"
)

func TestGetUserGroupChat(t *testing.T) {
	db := GetTestDbConnection()
	groupIds, err := GetUserGroupChat(db, 7)
	if err != nil {
		t.Error(err)
	}
	utils.DebugDump(groupIds)
	utils.DebugDump(groupIds[0])
}
