package models

import (
	"github.com/jinzhu/gorm"
	"gitlab.com/rockship/backend/zeniius-backend/utils"
	"time"
)

type PostLike struct {
	ID        int64      `json:"id" gorm:"primary_key"`
	PostID    int64      `json:"post_id" gorm:"column:post_id"`
	UserID    int64      `json:"user_id" gorm:"column:user_id"`
	CreatedAt time.Time  `json:"created_at" gorm:"column:created_at"`
	DeletedAt *time.Time `json:"deleted_at" gorm:"column:deleted_at"`
}

type CommentLike struct {
	ID        int64      `json:"id" gorm:"primary_key"`
	CommentID int64      `json:"comment_id" gorm:"column:comment_id"`
	UserID    int64      `json:"user_id" gorm:"column:user_id"`
	CreatedAt time.Time  `json:"created_at" gorm:"column:created_at"`
	DeletedAt *time.Time `json:"deleted_at" gorm:"column:deleted_at"`
}

func (PostLike) TableName() string {
	return "post_like"
}

func (CommentLike) TableName() string {
	return "comment_like"
}

func LikePost(db *gorm.DB, postID int64, userID int64) error {
	var l PostLike
	result := db.Where("post_id = ? AND user_id = ? AND deleted_at IS NULL", postID, userID).
		First(&l)
	if !result.RecordNotFound() {
		return utils.ResourceExisted{
			ObjectType: "post_like",
			ObjectID:   l.ID,
		}
	}
	err := db.Create(&PostLike{
		PostID: postID,
		UserID: userID,
	}).Error
	return err
}

func UnLikePost(db *gorm.DB, postID int64, userID int64) error {
	var l PostLike
	result := db.Where("post_id = ? AND user_id = ? AND deleted_at IS NULL", postID, userID).
		First(&l)
	if result.RecordNotFound() {
		return utils.ResourceNotFound{
			ObjectType: "post_like",
			ObjectID:   l.ID,
		}
	}
	err := db.Where("post_id = ? AND user_id = ?", postID, userID).
		Delete(&PostLike{}).Error
	return err
}

func LikeComment(db *gorm.DB, commentID int64, userID int64) error {
	var l CommentLike
	result := db.Where("comment_id = ? AND user_id = ? AND deleted_at IS NULL", commentID, userID).
		First(&l)
	if !result.RecordNotFound() {
		return utils.ResourceExisted{
			ObjectType: "comment_like",
			ObjectID:   l.ID,
		}
	}
	err := db.Create(&CommentLike{
		CommentID: commentID,
		UserID:    userID,
	}).Error
	return err
}

func UnLikeComment(db *gorm.DB, commentID int64, userID int64) error {
	var l CommentLike
	result := db.Where("comment_id = ? AND user_id = ? AND deleted_at IS NULL", commentID, userID).
		First(&l)
	if result.RecordNotFound() {
		return utils.ResourceNotFound{
			ObjectType: "comment_like",
			ObjectID:   l.ID,
		}
	}
	err := db.Where("comment_id = ? AND user_id = ?", commentID, userID).
		Delete(&CommentLike{}).Error
	return err
}

func (o *Post) ListLikedUser(db *gorm.DB) ([]User, error) {
	var users []User
	err := db.Preload("Avatar").Where("deleted_at is NULL AND is_verify = true AND id in (?)",
		db.Table("post_like").Select("user_id").Where("deleted_at IS NULL AND post_id = ?", o.ID).QueryExpr()).
		Find(&users).Error
	return users, err
}
