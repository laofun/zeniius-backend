package models

import (
	"github.com/jinzhu/gorm"
	"gitlab.com/rockship/backend/zeniius-backend/utils"
)

type PostReport struct {
	ID      int64  `json:"id" gorm:"primary_key"`
	PostID  int64  `json:"post_id" gorm:"column:post_id"`
	UserID  int64  `json:"user_id" gorm:"column:user_id"`
	Message string `json:"message" gorm:"column:message"`
}

func (PostReport) TableName() string {
	return "post_reports"
}

func ReportPost(db *gorm.DB, postID int64, userID int64, message string) error {
	var p PostReport
	result := db.Where("post_id = ? AND user_id = ?", postID, userID).First(&p)
	if !result.RecordNotFound() {
		return utils.ResourceExisted{
			ObjectType: "post_report",
			ObjectID:   p.ID,
		}
	}
	err := db.Create(&PostReport{
		PostID:  postID,
		UserID:  userID,
		Message: message,
	}).Error

	return err

}
