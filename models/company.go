package models

import (
	"errors"
	"github.com/jinzhu/gorm"
	"github.com/sirupsen/logrus"
	"gopkg.in/guregu/null.v3"
	"strings"
	"time"
)

type Company struct {
	ID                          int64             `json:"id" gorm:"primary_key"`
	Name                        string            `json:"name" gorm:"column:name"`
	Description                 string            `json:"description" gorm:"column:description"`
	Contact                     string            `json:"contact" gorm:"column:contact"`
	Address                     string            `json:"address" gorm:"column:address"`
	CreatedAt                   *time.Time        `json:"created_at" gorm:"column:created_at"`
	UpdatedAt                   *time.Time        `json:"updated_at" gorm:"column:updated_at"`
	Logo                        *File             `json:"logo" gorm:"foreignKey:LogoPicture"`
	LogoPicture                 null.Int          `json:"-" gorm:"column:logo"`
	Phone                       string            `json:"phone" gorm:"column:phone_number"`
	Email                       string            `json:"email" gorm:"column:email"`
	Facebook                    string            `json:"facebook_link" gorm:"column:facebook_link"`
	Website                     string            `json:"website" gorm:"column:website"`
	NameCardFront               *File             `json:"name_card_front" gorm:"foreignKey:NameCardFrontPicture"`
	NameCardBack                *File             `json:"name_card_back" gorm:"foreignKey:NameCardBackPicture"`
	NameCardFrontPicture        null.Int          `json:"-" gorm:"column:name_card_front"`
	NameCardBackPicture         null.Int          `json:"-" gorm:"column:name_card_back"`
	BusinessRegistration        *File             `json:"business_registration" gorm:"foreignKey:BusinessRegistrationPicture"`
	BusinessRegistrationPicture null.Int          `json:"-" gorm:"column:business_registration_cert"`
	Role                        string            `json:"role" gorm:"-"`
	Album                       []File            `json:"album" gorm:"many2many:company_album"`
	Business                    []CompanyBusiness `json:"business"`
	Certificates                []File            `json:"certificates" gorm:"many2many:company_certificates"`
}

type UserCompany struct {
	UserID    int64      `json:"user_id" gorm:"primary_key;column:user_id"`
	CompanyID int64      `json:"company_id" gorm:"primary_key;column:company_id"`
	CreatedAt *time.Time `json:"created_at" gorm:"column:created_at"`
	Role      string     `json:"role" gorm:"column:role"`
}

type CompanyCertificates struct {
	ID        int64      `json:"id" gorm:"primary_key"`
	CompanyID int64      `json:"company_id" gorm:"column:company_id"`
	FileID    int64      `json:"file_id" gorm:"column:file_id"`
	File      *File      `json:"file" gorm:"foreignKey:FileID"`
	CreatedAt *time.Time `json:"created_at" gorm:"created_at"`
	DeletedAt *time.Time `json:"-" gorm:"column:deleted_at"`
}

type UserCompanyResponse struct {
	Company
	Role string `json:"role"`
}

func (Company) TableName() string {
	return "companies"
}

func (UserCompany) TableName() string {
	return "user_company"
}

func (CompanyCertificates) TableName() string {
	return "company_certificates"
}

const BusinessCertificatesLimit = 5

func (c *Company) Save(db *gorm.DB) error {
	err := db.Create(&c).Error
	return err
}

func (c *Company) AddUserToCompany(db *gorm.DB, companyID int64, userID int64, role string) error {
	userCompany := UserCompany{
		UserID:    userID,
		CompanyID: companyID,
		Role:      role,
	}
	err := db.Create(&userCompany).Error
	return err
}

func checkRoleFromListUserCompany(id int64, userCompanies []UserCompany) string {
	for _, value := range userCompanies {
		if value.CompanyID == id {
			return value.Role
		}
	}
	return ""
}

func GetListCompanyOfUser(db *gorm.DB, userID int64) ([]Company, error) {
	var (
		listCompany   []Company
		userCompanies []UserCompany
		companyIDs    []int64
	)
	err := db.Where("user_id = ?", userID).Find(&userCompanies).Error

	for _, c := range userCompanies {
		companyIDs = append(companyIDs, c.CompanyID)
	}

	if err != nil {
		return []Company{}, err
	}

	err = db.Preload("Logo").Preload("NameCardFront").Preload("BusinessRegistration").Preload("Album").
		Preload("NameCardBack").Preload("Certificates", "company_certificates.deleted_at is NULL").Where("id IN (?)", companyIDs).
		Find(&listCompany).Error

	if err != nil {
		return []Company{}, err
	}

	for key, _ := range listCompany {
		role := checkRoleFromListUserCompany(listCompany[key].ID, userCompanies)
		if role != "" {
			listCompany[key].Role = role
		}
		business, err := GetBusinessByCompanyID(db, listCompany[key].ID)
		if err != nil {
			logrus.Error("get business company error: ", err)
		}
		listCompany[key].Business = business
	}
	return listCompany, nil
}

func GetCompanyProfile(db *gorm.DB, companyId int64) (Company, error) {
	var company Company
	err := db.Preload("Logo").Preload("NameCardFront").Preload("BusinessRegistration").
		Preload("Album").Preload("NameCardBack").Preload("Business").Preload("Certificates", "company_certificates.deleted_at is NULL").
		Where("id = ? and deleted_at IS NULL", companyId).
		First(&company).Error
	if err != nil {
		return Company{}, err
	}
	return company, nil
}

func GetCompanyBasicInfo(db *gorm.DB, companyId int64) (Company, error) {
	var company Company
	err := db.Where("id = ? and deleted_at IS NULL", companyId).First(&company).Error
	if err != nil {
		return Company{}, err
	}
	return company, nil
}

func (c *Company) Update(db *gorm.DB, userID int64, role string) error {

	err := db.Save(&c).Error
	if err != nil {
		return err
	}

	userCompany := UserCompany{
		CompanyID: c.ID,
		UserID:    userID,
		Role:      role,
	}
	err = db.Save(&userCompany).Error
	return err

}

func (c *Company) UpdateCompanyAlbum(db *gorm.DB, addFiles []int64, removeFiles []int64) error {

	if len(removeFiles) != 0 {
		err := db.Exec("DELETE FROM company_album WHERE company_id = ? and file_id in (?) ", c.ID, removeFiles).Error
		if err != nil {
			return err
		}
	}

	if len(addFiles) != 0 {
		sqlStr := "INSERT INTO company_album(company_id, file_id, created_at) VALUES "
		vals := []interface{}{}
		const rowSQL = "(?, ?, NOW())"
		var inserts []string

		for _, fileID := range addFiles {
			inserts = append(inserts, rowSQL)
			vals = append(vals, c.ID, fileID)
		}
		sqlStr = sqlStr + strings.Join(inserts, ",")

		if err := db.Exec(sqlStr, vals...).Error; err != nil {
			return err
		}
	}
	return nil

}

func CheckCompanyExist(db *gorm.DB, companyID int64) bool {
	var company Company
	err := db.Where("id = ?", companyID).First(&company).Error
	if err != nil {
		return false
	}
	return true
}

func DeleteCompanyCertificates(db *gorm.DB, companyID int64, fileID int64) error {
	err := db.Where("company_id = ? AND file_id = ?", companyID, fileID).
		Delete(&CompanyCertificates{}).Error
	return err
}

func (c *Company) AddCertificate(db *gorm.DB, fileID int64) (CompanyCertificates, error) {
	var certificates []CompanyCertificates
	count := 0
	err := db.Where("company_id = ? AND deleted_at IS NULL", c.ID).
		Find(&certificates).Count(&count).Error
	if err != nil {
		return CompanyCertificates{}, err
	}

	if count >= BusinessCertificatesLimit {
		return CompanyCertificates{}, errors.New("certificates limit is 5")
	}

	for _, value := range certificates {
		if fileID == value.FileID {
			return value, nil
		}
	}

	newCertificate := CompanyCertificates{
		CompanyID: c.ID,
		FileID:    fileID,
	}
	err = db.Save(&newCertificate).Error

	if err != nil {
		return CompanyCertificates{}, err
	}

	err = db.Preload("File").Where("id = ?", newCertificate.ID).First(&newCertificate).Error

	if err != nil {
		return CompanyCertificates{}, err
	}

	return newCertificate, nil

}

type CompanyStub struct {
	ID   int64       `json:"id" gorm:"column:id"`
	Name null.String `json:"name" gorm:"column:name"`
}

func GetCompanyStubProfiles(db *gorm.DB) ([]CompanyStub, error) {
	var companyList []CompanyStub
	err := db.Table("companies").Select("id, name").
		Where("deleted_at IS NULl").Scan(&companyList).
		Error
	return companyList, err
}
