package models

import (
	"time"

	"errors"
	"github.com/jinzhu/gorm"
	"github.com/sirupsen/logrus"
	"gitlab.com/rockship/backend/zeniius-backend/utils"
	"gopkg.in/guregu/null.v3"
)

type Comment struct {
	ID                    int64     `json:"id"`
	User                  *User     `json:"user" gorm:"foreignKey:UserID"`
	UserID                int64     `json:"-"`
	Content               string    `json:"content"`
	CreatedAt             time.Time `json:"created_at"`
	LikeCount             int       `json:"like_count" gorm:"-"`
	Liked                 bool      `json:"liked" gorm:"-"`
	ParentID              null.Int  `json:"parent_id" gorm:"column:parent_id"`
	Attachment            *File     `json:"attachment"  gorm:"foreignKey:AttachmentID"`
	AttachmentID          null.Int  `json:"-" gorm:"column:attachment"`
	PostID                int64     `json:"post_id" gorm:"column:post_id"`
	HasChild              bool      `json:"has_children" gorm:"-"`
	ChildrenCommentsCount int64     `json:"children_comment_count" gorm:"-"`
	ChildrenComments      []Comment `json:"children_comment" gorm:"-"`
	Locale                string    `json:"locale" gorm:"column:locale"`
	DeletedAt             null.Time `json:"-" gorm:"column:deleted_at"`
}

const CommentInPostLimit = 10

func (c *Comment) GetCommentOfPost(db *gorm.DB, postID int64, page int, limit int, userID int64) ([]Comment, error) {
	var comments []Comment

	offset := (page - 1) * limit

	err := db.Order("created_at desc").Limit(limit).Offset(offset).
		Preload("Attachment").Preload("User").Preload("User.Avatar").
		Where("post_id = ? AND deleted_at IS NULL AND parent_id IS NULL", postID).Find(&comments).Error
	if err != nil {
		return []Comment{}, err
	}

	for key, _ := range comments {
		comments[key].GetLikeStatistic(db, userID)
	}

	err = getChildrenComments(db, comments, userID)

	if err != nil {
		logrus.Error("Check comment has children error: ", err)
	}

	return comments, nil

}

func (c *Comment) Save(db *gorm.DB) error {
	err := db.Create(&c).Error

	if err != nil {
		return err
	}
	return nil
}

func (c *Comment) GetDetail(db *gorm.DB) error {
	err := db.Order("created_at desc").Preload("Attachment").Preload("User").
		Where("id = ?", c.ID).Find(&c).Error
	if err != nil {
		return err
	}

	return nil
}

func (c *Comment) GetChildrenCommentsOfComment(db *gorm.DB, commentID int64, page int, limit int, userID int64) ([]Comment, error) {
	var comments []Comment

	offset := (page - 1) * limit

	err := db.Order("created_at desc").Limit(limit).Offset(offset).
		Preload("Attachment").Preload("User").Preload("User.Avatar").
		Where("deleted_at IS NULL AND parent_id = ?", commentID).Find(&comments).Error
	if err != nil {
		return []Comment{}, err
	}
	for key, _ := range comments {
		comments[key].GetLikeStatistic(db, userID)
	}
	return comments, nil
}

func (c *Comment) GetLatestChildComment(db *gorm.DB, userId int64) {
	var child Comment
	err := db.Order("created_at desc").Preload("Attachment").Preload("User").Preload("User.Avatar").
		Where("parent_id = ?", c.ID).First(&child).Error
	if err != nil {
		logrus.Error("Getting lasted child comment: ", err)
		return
	}
	child.GetLikeStatistic(db, userId)
	c.ChildrenComments = append(c.ChildrenComments, child)
}

type childCommentCount struct {
	ParentID     int64 `gorm:"column:parent_id"`
	CommentCount int64 `gorm:"column:num_comment"`
}

func getChildrenComments(db *gorm.DB, comments []Comment, userID int64) error {
	var ids []int64
	for index := range comments {
		ids = append(ids, comments[index].ID)
	}

	var result []childCommentCount
	err := db.Table("comments").Select("parent_id, COUNT(id) as num_comment").Group("parent_id").
		Where("parent_id IN (?)", ids).Scan(&result).Error

	if err != nil {
		return err
	}

	for i := range comments {
		if c := inChildCommentCountArray(result, comments[i].ID); c > 0 {
			comments[i].HasChild = true
			comments[i].ChildrenCommentsCount = c
			comments[i].GetLatestChildComment(db, userID)
		}
	}

	return nil
}

func inChildCommentCountArray(collection []childCommentCount, parentID int64) int64 {
	for _, value := range collection {
		if value.ParentID == parentID {
			return value.CommentCount
		}
	}
	return 0
}

func GetComment(db *gorm.DB, commentID int64) (Comment, error) {
	c := Comment{}
	err := db.Preload("Attachment").Preload("User").
		Where("id = ?", commentID).First(&c).Error
	if err != nil {
		return Comment{}, err
	}
	return c, nil

}

func DeleteComment(db *gorm.DB, commentID int64, userID int64) error {
	c, err := GetComment(db, commentID)
	if err != nil {
		return err
	}
	if c.UserID != userID {
		return errors.New("can not delete other user's comment")
	}
	err = db.Where("parent_id = ? OR id = ?", commentID, commentID).Delete(&Comment{}).Error

	return nil
}

func (c *Comment) UpdateComment(db *gorm.DB) error {
	err := db.Save(&c).Error
	return err
}

func (c *Comment) GetLikeStatistic(db *gorm.DB, userId int64) error {
	var userIDList []int64
	err := db.Model(&CommentLike{}).Where("deleted_at IS NULL AND comment_id = ?", c.ID).Pluck("user_id", &userIDList).Error
	if err != nil {
		return err
	}
	c.LikeCount = len(userIDList)

	if utils.InIntArray(userIDList, userId) {
		c.Liked = true
	}
	return nil
}
