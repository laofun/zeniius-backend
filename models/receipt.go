// Package models sample code
package models

import (
	"time"

	"github.com/jmoiron/sqlx"
)

// Receipt sample data model
type Receipt struct {
	ID          int       `db:"id"`
	RawReceipt  string    `db:"raw_receipt"`
	Amount      float64   `db:"parsed_amount"`
	Account     string    `db:"parsed_account"`
	PhoneNumber string    `db:"phonenumber"`
	CreateAt    time.Time `db:"create_at"`
}

// NewReceipt create new Receipt object
func NewReceipt(rawReceipt, phonenumber string, createAt time.Time) *Receipt {
	return &Receipt{
		RawReceipt:  rawReceipt,
		PhoneNumber: phonenumber,
		CreateAt:    createAt,
	}
}

// Add a receipt to database
func (r *Receipt) Add(db *sqlx.DB) (int, error) {
	var receiptID int
	query := `INSERT INTO receipt (raw_receipt, phonenumber, create_at) VALUES ($1, $2, $3) RETURNING id`
	err := db.Get(&receiptID, query, r.RawReceipt, r.PhoneNumber, r.CreateAt)
	if err != nil {
		return 0, err
	}
	return receiptID, nil
}
