package models

import (
	"errors"
	"time"

	"github.com/sirupsen/logrus"

	"github.com/jinzhu/gorm"
)

// FriendRequestStatus ..
type FriendRequestStatus string

const (
	none       FriendRequestStatus = "none" // not in sql type
	pending    FriendRequestStatus = "pending"
	accepted   FriendRequestStatus = "accepted"
	declined   FriendRequestStatus = "declined"
	canceled   FriendRequestStatus = "canceled"
	befriended FriendRequestStatus = "befriended"
	unfriended FriendRequestStatus = "unfriended"
)

// FriendRelationship model a Friend Relationship
type FriendRelationship struct {
	ID int16 `json:"id" gorm:"primary_key"`

	UserID   int64 `json:"-"`
	User     *User `json:"-" gorm:"foreignkey:UserID"`
	FriendID int64 `json:"friend_id"`
	Friend   *User `json:"friend" gorm:"foreignkey:FriendID"`

	Status FriendRequestStatus `json:"status" gorm:"type:friend_request_status;default:'pending';not null"`

	CreatedAt time.Time `json:"created_at" gorm:"column:created_at"`
}

// TableName ..
func (FriendRelationship) TableName() string {
	return "friend_relationship"
}

// MakeFriendRequest ..
func (u User) addFriendRelationship(db *gorm.DB, friendID int64, relationshipType FriendRequestStatus) (*FriendRelationship, string, error) {
	fr := FriendRelationship{
		UserID:   u.ID,
		FriendID: friendID,
		Status:   relationshipType,
	}

	err := db.Create(&fr).Error
	if err != nil {
		return nil, "Unepxected Error", err
	}
	return &fr, "", nil
}

// addTwoPeerFriendRelationship ..
func (u User) addTwoPeerFriendRelationship(db *gorm.DB, friendID int64, relationshipType FriendRequestStatus) (*FriendRelationship, *FriendRelationship, string, error) {
	if !(relationshipType == "befriended" || relationshipType == "unfriended") {
		return nil, nil, "Wrong relationship type", errors.New("Wrong relationship type")
	}

	fr1 := FriendRelationship{
		UserID:   u.ID,
		FriendID: friendID,
		Status:   relationshipType,
	}

	fr2 := FriendRelationship{
		UserID:   friendID,
		FriendID: u.ID,
		Status:   relationshipType,
	}

	err := db.Create(&fr1).Error
	if err != nil {
		return nil, nil, "Unepxected Error", err
	}

	err = db.Create(&fr2).Error
	if err != nil {
		return nil, nil, "Unepxected Error", err
	}
	return &fr1, &fr2, "", nil
}

// addFriend ..
func (u User) addFriend(db *gorm.DB, friendID int64) (string, error) {
	_, _, errString, err := u.addTwoPeerFriendRelationship(db, friendID, "befriended")
	if err != nil {
		return errString, err
	}
	return "", nil
}

// RemoveFriend ..
func (u User) RemoveFriend(db *gorm.DB, friendID int64) (string, error) {
	status, err := GetCurrentFriendRelationship(db, u.ID, friendID)
	if err != nil {
		return "getCurrentFriendRelationship Error", errors.New("getCurrentFriendRelationship Error")
	}

	if status != "befriended" {
		return "You two must befriend first", errors.New("You two must befriend first")
	}

	_, _, errString, err := u.addTwoPeerFriendRelationship(db, friendID, "unfriended")
	if err != nil {
		return errString, err
	}
	return "", nil
}

// MakeFriendRequest ..
func (u User) MakeFriendRequest(db *gorm.DB, friendID int64) (*FriendRelationship, string, error) {
	status, err := GetCurrentFriendRelationship(db, u.ID, friendID)
	if err != nil {
		return nil, "", err
	}

	if status == "befriended" {
		return nil, "You two already be friended", errors.New("You two already be friended")
	}

	if status == "pending" {
		return nil, "You already sent request", errors.New("You already sent request")
	}

	// TODO: !!! add two way

	fr, errString, err := u.addFriendRelationship(db, friendID, "pending")
	if err != nil {
		return nil, errString, err
	}
	return fr, "", err
}

// AcceptFriendRequest ..
func (u User) AcceptFriendRequest(db *gorm.DB, friendID int64) (*FriendRelationship, string, error) {
	status, err := GetCurrentFriendRelationship(db, friendID, u.ID)
	if err != nil {
		return nil, "", err
	}

	if status != "pending" {
		return nil, "No friend request found", errors.New("No friend request found")
	}

	fr, errString, err := u.addFriendRelationship(db, friendID, "accepted")
	if err != nil {
		return nil, errString, err
	}

	errString, err = u.addFriend(db, friendID)
	if err != nil {
		return nil, errString, err
	}
	return fr, "", err
}

// DeclineFriendRequest ..
func (u User) DeclineFriendRequest(db *gorm.DB, friendID int64) (*FriendRelationship, string, error) {
	status, err := GetCurrentFriendRelationship(db, friendID, u.ID)
	if err != nil {
		return nil, "", err
	}

	if status != "pending" {
		logrus.Info(status)
		return nil, "No friend request found", errors.New("No friend request found")
	}

	user, err := GetUserByID(db, friendID)
	if err != nil {
		return nil, "", err
	}

	fr, errString, err := user.addFriendRelationship(db, u.ID, "declined")
	if err != nil {
		return nil, errString, err
	}

	return fr, "", err
}
