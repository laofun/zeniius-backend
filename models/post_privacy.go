package models

import (
	"github.com/jinzhu/gorm"
	"github.com/sirupsen/logrus"
	"gitlab.com/rockship/backend/zeniius-backend/service"
	"gitlab.com/rockship/backend/zeniius-backend/utils"
	"strings"
	"time"
)

type PostGroup struct {
	ID        int64      `json:"id" gorm:"primary_key"`
	PostID    int64      `json:"company_id" gorm:"column:post_id"`
	GroupID   string     `json:"group_id" gorm:"column:group_id"`
	UserID    int64      `json:"user_id"`
	CreatedAt *time.Time `json:"created_at" gorm:"column:created_at"`
	DeletedAt *time.Time `json:"-" gorm:"column:deleted_at"`
}

type UserGroup struct {
	GroupID string `gorm:"column:conversationId"`
	UserId  int64  `gorm:"column:userId"`
	Role    string `gorm:"column:role"`
}

type Group struct {
	ID          string `json:"id" gorm:"column:id"`
	Name        string `json:"name" gorm:"column:name"`
	Description string `json:"description" gorm:"column:description"`
	Avatar      string `json:"avatar" gorm:"column:avatar"`
}

func (PostGroup) TableName() string {
	return "post_group"
}

func (UserGroup) TableName() string {
	return "access_control"
}

func (Group) TableName() string {
	return "conversation"
}

func (o *Post) DeleteGroup(db *gorm.DB, groupIds []string) error {
	return db.Where("post_id = ? AND user_id = ? and group_id IN (?)", o.ID, o.UserId, groupIds).
		Delete(PostGroup{}).Error
}

func (o *Post) AddGroup(db *gorm.DB, groupIDs []string) error {
	sqlStr := "INSERT INTO post_group(post_id, user_id, group_id, created_at) VALUES "
	vals := []interface{}{}
	const rowSQL = "(?, ?, ?, NOW())"
	var inserts []string

	for _, groupID := range groupIDs {
		inserts = append(inserts, rowSQL)
		vals = append(vals, o.ID, o.UserId, groupID)
	}
	sqlStr = sqlStr + strings.Join(inserts, ",")

	if err := db.Exec(sqlStr, vals...).Error; err != nil {
		return err
	}
	return nil
}

func (o *Post) GetShareGroupIDs(db *gorm.DB) ([]string, error) {
	var groupIDs []string
	err := db.Model(PostGroup{}).Where("post_id = ? AND user_id = ?", o.ID, o.UserId).
		Pluck("group_id", &groupIDs).Error
	return groupIDs, err
}

func (o *Post) GetShareGroupDetail(db *gorm.DB) error {
	groupIDs, err := o.GetShareGroupIDs(db)
	if err != nil {
		return err
	}
	var groups []Group
	err = db.Where("id in (?)", groupIDs).Find(&groups).Error
	if err != nil {
		return err
	}
	o.Group = groups
	return nil
}

func (o *Post) UpdateGroup(db *gorm.DB, newGroupIDs []string) error {
	if newGroupIDs == nil || len(newGroupIDs) == 0 {
		return nil
	}
	oldGroupIDs, err := o.GetShareGroupIDs(db)
	if err != nil {
		return err
	}
	var removedIDs, addedIDs []string
	for _, oldID := range oldGroupIDs {
		if !utils.InStringArray(newGroupIDs, oldID) {
			removedIDs = append(removedIDs, oldID)
		}
	}

	for _, newID := range newGroupIDs {
		if !utils.InStringArray(oldGroupIDs, newID) {
			addedIDs = append(addedIDs, newID)
		}
	}
	err = o.DeleteGroup(db, removedIDs)
	if err != nil {
		return err
	}

	err = o.AddGroup(db, newGroupIDs)
	return err
}

func (u *User) GetGroup(db *gorm.DB) ([]string, error) {
	var groupIDs []string
	err := db.Model(UserGroup{}).Where(UserGroup{UserId: u.ID}).Pluck("\"conversationId\"", &groupIDs).Error
	return groupIDs, err
}

func (o *Post) ShareToGroup(db *gorm.DB, groupID string, userID int64) error {
	postGroup := PostGroup{
		PostID:  o.ID,
		UserID:  userID,
		GroupID: groupID,
	}
	return db.Save(&postGroup).Error
}

func (o *Post) Share(db *gorm.DB, user User, groupIDs []string) ([]string, []string, error) {
	tx := db.Begin()
	defer func() {
		if r := recover(); r != nil {
			tx.Rollback()
		}
	}()
	sharedGroupIDs, err := o.GetShareGroupIDs(db)
	if err != nil {
		return nil, nil, err
	}
	userGroup, err := user.GetGroup(db)
	if err != nil {
		return nil, nil, err
	}

	var successGroup, errorGroup []string
	userID := user.ID
	for _, group := range groupIDs {
		if !utils.InStringArray(userGroup, group) {
			errorGroup = append(errorGroup, group)
			continue
		}
		if (o.IsPublic == true) ||
			(o.IsPublic == false && utils.InStringArray(sharedGroupIDs, group)) ||
			(o.IsPublic == false && !utils.InStringArray(sharedGroupIDs, group) && o.UserId == userID) {
			err = o.ShareToGroup(db, group, userID)
			if err != nil {
				errorGroup = append(errorGroup, group)
				continue
			}
			successGroup = append(successGroup, group)
			continue
		}
		errorGroup = append(errorGroup, group)
	}
	err = service.SendMessageToGroup(o.ID, successGroup, userID)
	if err != nil {
		tx.Rollback()
		return successGroup, errorGroup, err
	}
	err = tx.Commit().Error
	return successGroup, errorGroup, err

}

func CheckPostPrivacy(db *gorm.DB, postID int64, user User) bool {
	p, err := GetPost(db, postID)
	if err != nil {
		logrus.Error("get post error", err)
		return false
	}
	if p.IsPublic == true || p.UserId == user.ID {
		return true
	}

	postGroups, err := p.GetShareGroupIDs(db)
	if err != nil {
		logrus.Error("get post group error", err)
		return false
	}

	userGroups, err := user.GetGroup(db)
	if err != nil {
		logrus.Error("get user group error", err)
		return false
	}
	for _, userGroup := range userGroups {
		if utils.InStringArray(postGroups, userGroup) {
			return true
		}
	}
	return false
}
