// All of Get extra, Get detail go here
package models

import (
	"github.com/jinzhu/gorm"

	"github.com/sirupsen/logrus"
	"gopkg.in/guregu/null.v3"
)

// GetExtraUserData .. => load to
func GetExtraUserData(db *gorm.DB, u User) User {
	myFollowerIds, err := u.GetAllFollowerID(db)
	if err != nil {
		logrus.Info(err)
	}
	u.FollowingCount = int64(len(myFollowerIds))

	myFollowingIds, err := u.GetAllFollowingID(db)
	if err != nil {
		logrus.Info(err)
	}
	u.FollowCount = int64(len(myFollowingIds))

	myFriendIds, _, err := u.ListMyFriend(db)
	u.FriendCount = int64(len(myFriendIds))
	if err != nil {
		logrus.Info(err)
	}

	type userPosition struct {
		Position null.String
	}
	var p userPosition
	err = db.Raw(`select concat(uc.role, ' of ', c."name") as Position
					from user_company uc 
					left join companies c on c.id = uc.company_id
					where c.deleted_at is null and uc.user_id = ?
					ORDER BY c.updated_at DESC
					LIMIT 1 `, u.ID).Scan(&p).Error
	if err != nil {
		logrus.Error("get postion of user fail ", err)
	} else {
		u.Position = p.Position
	}
	return u
}

func (u *User) GetDetailUser(db *gorm.DB) error {
	userProfile, err := GetUserByID(db, u.ID)
	if err != nil {
		return err
	}
	*u = userProfile
	u.GetExtraUserData(db)

	return nil
}

func (u *User) GetExtraUserData(db *gorm.DB) {
	myFollowerIds, err := u.GetAllFollowerID(db)
	if err != nil {
		logrus.Info(err)
	}
	u.FollowingCount = int64(len(myFollowerIds))

	myFollowingIds, err := u.GetAllFollowingID(db)
	if err != nil {
		logrus.Info(err)
	}
	u.FollowCount = int64(len(myFollowingIds))

	myFriendIds, _, err := u.ListMyFriend(db)
	u.FriendCount = int64(len(myFriendIds))
	if err != nil {
		logrus.Info(err)
	}

	type userPosition struct {
		Position null.String
	}
	var p userPosition
	err = db.Raw(`select concat(uc.role, ' of ', c."name") as Position
					from user_company uc 
					left join companies c on c.id = uc.company_id
					where c.deleted_at is null and uc.user_id = ?
					ORDER BY c.updated_at DESC
					LIMIT 1 `, u.ID).Scan(&p).Error
	if err != nil {
		logrus.Error("get postion of user fail ", err)
	} else {
		u.Position = p.Position
	}

}
