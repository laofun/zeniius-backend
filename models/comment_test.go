package models

import (
	"testing"

	"gitlab.com/rockship/backend/zeniius-backend/utils"
)

func TestComment_GetChildrenComments(t *testing.T) {
	db := GetTestDbConnection()
	ids := []int64{9, 12, 14}
	type CountResult struct {
		ParentID     int64 `gorm:"column:parent_id"`
		CommentCount int64 `gorm:"column:num_comment"`
	}

	var result []CountResult
	err := db.Table("comments").Select("parent_id, COUNT(id) as num_comment").Group("parent_id").
		Where("parent_id IN (?)", ids).Scan(&result).Error

	if err != nil {
		t.Error(err)
	}
	utils.DebugDump(result)

}
