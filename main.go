package main

import (
	"net/http"
	"os"
	"path/filepath"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/jinzhu/gorm"
	_ "github.com/joho/godotenv/autoload"
	"github.com/sirupsen/logrus"
	"gitlab.com/rockship/backend/zeniius-backend/api"
	zeniiusMiddleware "gitlab.com/rockship/backend/zeniius-backend/middleware"
	"gitlab.com/rockship/backend/zeniius-backend/service"
	"gitlab.com/rockship/backend/zeniius-backend/validate"
)

const startServer = `Server's running!
        .---.  
      .'_:___".
      |__ --==| Beep Boop ...
      [  ]  :[| 
      |__| I=[| 
     /  / ____| 
     |-/.____.' 
    /___\ /___\
`

var (
	db  *gorm.DB
	log *logrus.Entry
)

func init() {
	logrus.SetOutput(os.Stdout)
	logrus.SetFormatter(&logrus.TextFormatter{})
	logrus.SetLevel(logrus.DebugLevel)
	db = service.GetDatabaseConnection()
	validate.InitCustomValidator()
}

func createRouter(env api.Env) *chi.Mux {
	router := chi.NewRouter()
	router.Use(
		middleware.Logger,
		middleware.RequestID,
		middleware.RedirectSlashes,
		middleware.RealIP,
		middleware.Recoverer,
		// middleware.Timeout(60*time.Second),
		zeniiusMiddleware.AccessControl,
		zeniiusMiddleware.ExtractLocale,
	)
	router.Get("/", api.HandleAPI(env.Ping).Reg)
	router.Get("/swaggerui", SwaggerUI)
	router.Get("/swaggerui/*", SwaggerUI)

	router.Post("/api/v1/login", api.HandleAPI(env.LoginWithFirebaseToken).Reg)
	// Business
	router.Get("/api/v1/business", api.HandleAPI(env.GetListBusiness).Reg)
	// Company
	router.Get("/api/v1/company", api.HandleAPI(env.GetAllCompany).Reg)

	router.Route("/api/v1", func(r chi.Router) {
		r.Use(zeniiusMiddleware.VerifyAccessTokenMiddleware)

		// User
		r.Get("/user", api.HandleAPI(env.GetAllUser).Reg)
		r.Get("/user/{id}", api.HandleAPI(env.GetUserProfile).Reg)
		r.Get("/user/search", api.HandleAPI(env.SearchUser).Reg)
		r.Put("/user/{id}/verification", api.HandleAPI(env.VerifyUser).Reg)
		r.Get("/users/{id}/following", api.HandleAPI(env.GetUserFollowering).Reg)
		r.Get("/users/{id}/friends", api.HandleAPI(env.GetUserFriend).Reg)
		r.Get("/user/{id}/company", api.HandleAPI(env.GetListCompanyOfUser).Reg)

		// Personal
		r.Get("/user/me", api.HandleAPI(env.GetCurrentUser).Reg)
		r.Put("/user/me/personal", api.HandleAPI(env.UpdateUserPersonalInformation).Reg)
		r.Put("/user/me/contact", api.HandleAPI(env.UpdateUserContact).Reg)
		r.Put("/user/me/avatar", api.HandleAPI(env.UpdateUserAvatar).Reg)
		r.Put("/user/me/cover", api.HandleAPI(env.UpdateUserCover).Reg)
		r.Post("/user/me/company", api.HandleAPI(env.CreateCompany).Reg)
		r.Get("/user/me/company", api.HandleAPI(env.GetListCompany).Reg)
		r.Put("/user/me/company/{id}", api.HandleAPI(env.UpdateUserCompany).Reg)
		r.Put("/user/me/company/{id}/album", api.HandleAPI(env.UpdateCompanyAlbum).Reg)
		r.Post("/user/me/company/{id}/business", api.HandleAPI(env.AddCompanyBusiness).Reg)
		r.Post("/user/me/company/{id}/certificate", api.HandleAPI(env.AddCompanyCertificate).Reg)
		r.Delete("/user/me/company/{company_id}/certificate/{file_id}", api.HandleAPI(env.DeleteCompanyCertificate).Reg)
		r.Post("/user/me/business", api.HandleAPI(env.AddUserBusiness).Reg)

		// Newsfeed
		r.Get("/feed", api.HandleAPI(env.GetFeeds).Reg)
		r.Post("/feed", api.HandleAPI(env.CreatePost).Reg)
		r.Get("/feed/{id}/comment", api.HandleAPI(env.GetComment).Reg)
		r.Post("/feed/{id}/comment", api.HandleAPI(env.CreateComment).Reg)
		r.Get("/feed/search", api.HandleAPI(env.GetFeeds).Reg)

		// Post
		r.Post("/post", api.HandleAPI(env.CreatePost).Reg)
		r.Get("/post/{id}", api.HandleAPI(env.GetPostDetail).Reg)
		r.Delete("/post/{id}", api.HandleAPI(env.DeletePost).Reg)
		r.Put("/post/{id}", api.HandleAPI(env.UpdatePost).Reg)
		r.Post("/post/{id}/share", api.HandleAPI(env.SharePost).Reg)
		r.Post("/post/{id}/report", api.HandleAPI(env.ReportPost).Reg)
		r.Get("/post/{id}/like", api.HandleAPI(env.GetUserLikePost).Reg)
		r.Post("/post/{id}/like", api.HandleAPI(env.LikePost).Reg)
		r.Delete("/post/{id}/like", api.HandleAPI(env.UnlikePost).Reg)
		r.Post("/post/{id}/register", api.HandleAPI(env.RegisterPost).Reg)
		r.Get("/post/{id}/register", api.HandleAPI(env.GetUserRegisterPost).Reg)
		r.Delete("/post/{id}/register", api.HandleAPI(env.UnregisterPost).Reg)
		r.Post("/post/{post_id}/file/{file_id}", api.HandleAPI(env.AddPostFile).Reg)
		r.Delete("/post/{post_id}/file/{file_id}", api.HandleAPI(env.DeletePostFile).Reg)

		// Comment
		r.Get("/comment/{id}/children", api.HandleAPI(env.GetChildrenComments).Reg)
		r.Delete("/comment/{id}", api.HandleAPI(env.DeleteComments).Reg)
		r.Put("/comment/{id}", api.HandleAPI(env.UpdateComment).Reg)
		r.Post("/comment/{id}/like", api.HandleAPI(env.LikeComment).Reg)
		r.Delete("/comment/{id}/like", api.HandleAPI(env.UnlikeComment).Reg)

		// Relationship Friend
		r.Get("/friends", api.HandleAPI(env.GetAllFriend).Reg)                              // list all of my friends
		r.Delete("/friends/{friendID}", api.HandleAPI(env.RemoveFriend).Reg)                // remove a friend
		r.Post("/friends/request", api.HandleAPI(env.AddNewFriendRequest).Reg)              // add new friend request
		r.Get("/friends/request", api.HandleAPI(env.GetAllFriendRequest).Reg)               // list my new friend request
		r.Put("/friends/request", api.HandleAPI(env.AcceptFriendRequest).Reg)               // accept my new friend request
		r.Delete("/friends/request/{friendID}", api.HandleAPI(env.RemoveFriendRequest).Reg) // decline a friend request

		// Relationship Follower
		r.Get("/followers", api.HandleAPI(env.GetAllFollower).Reg)                    // list all user following me
		r.Get("/following", api.HandleAPI(env.GetAllFollowering).Reg)                 // list all user followed by me
		r.Post("/following", api.HandleAPI(env.AddFollowering).Reg)                   // follow a user
		r.Delete("/following/{followerID}", api.HandleAPI(env.RemoveFollowering).Reg) // unfollow a user

		// Media
		r.Post("/comment/file/url", api.HandleAPI(env.GetUploadLink).Reg)
		r.Post("/file/upload-url", api.HandleAPI(env.GetFileUploadLink).Reg)
		r.Get("/files/{fid}", api.HandleAPI(env.GetFileFullPath).Reg)

		//Translate
		r.Post("/translate", api.HandleAPI(env.TranslateText).Reg)
	})

	return router
}

// SwaggerUI serve swagger document
// TODO: professionalize this function
func SwaggerUI(w http.ResponseWriter, r *http.Request) {
	workDir, _ := os.Getwd()
	root := http.Dir(filepath.Join(workDir, "swaggerui"))

	fs := http.StripPrefix("/swaggerui", http.FileServer(root))
	fs.ServeHTTP(w, r)
}

func main() {

	env := api.Env{DB: db, Log: log}
	router := createRouter(env)

	logrus.Println(startServer)
	if err := http.ListenAndServe(":"+os.Getenv("SERVER_PORT"), router); err != nil {
		logrus.Fatal("Server Fail to start", err)
	}
}
