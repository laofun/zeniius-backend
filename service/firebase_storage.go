package service

import (
	"encoding/json"
	"errors"
	"os"
	"time"

	"cloud.google.com/go/storage"
	"crypto/sha1"
	"fmt"
	"github.com/sirupsen/logrus"
	"gitlab.com/rockship/backend/zeniius-backend/response"
	"gitlab.com/rockship/backend/zeniius-backend/utils"
)

type FirbaseConfig struct {
	Type                    string `json:"type"`
	ProjectID               string `json:"project_id"`
	PrivateKeyID            string `json:"private_key_id"`
	PrivateKey              string `json:"private_key"`
	ClientEmail             string `json:"client_email"`
	ClientID                string `json:"client_id"`
	AuthURI                 string `json:"auth_uri"`
	TokenURI                string `json:"token_uri"`
	AuthProviderX509CertURL string `json:"auth_provider_x509_cert_url"`
	ClientX509CertURL       string `json:"client_x509_cert_url"`
}

const ImagePath = "upload/images/"
const VideoPath = "upload/videos/"
const UserAvatarPath = "upload/user/avatar/"
const UserCoverPath = "upload/user/cover/"
const CompanyLogoPath = "upload/company/logo/"
const CompanyNamecardPath = "upload/company/namecard/"
const CompanyOtherPath = "upload/company/other/"
const ChatPath = "upload/chat/"

const FileTypeVideo = "video"
const FileTypeImage = "image"
const FileTypeUserAvatar = "user_avatar"
const FileTypeUserCover = "user_cover"
const FileTypeCompanyLogo = "company_logo"
const FileTypeCompanyNamecard = "company_namecard"
const FileTypeCompanyOther = "company_other"
const FileTypeChatFile = "chat_file"

var (
	firebaseConfig FirbaseConfig
)

func init() {

	firebaseConfig = readFirebaseConfig(utils.GetEnv("SERVICE_KEY", ""))
}

func readFirebaseConfig(path string) FirbaseConfig {
	file, err := os.Open(path)
	if err != nil {
		logrus.Fatal("Err Can not open firbase config file: ", err)
		panic("Can not open firbase config file: " + path)
	}
	defer file.Close()
	decoder := json.NewDecoder(file)
	config := FirbaseConfig{}
	err = decoder.Decode(&config)
	if err != nil {
		logrus.Fatal("Err decode firebase config: ", err)
		panic("Can not decode firebase config")
	}
	return config
}

func createSignedUrl(bucket string, filename string, contentType string) (string, string, error) {

	getUrl, err := storage.SignedURL(bucket, filename, &storage.SignedURLOptions{
		GoogleAccessID: firebaseConfig.ClientEmail,
		PrivateKey:     []byte(firebaseConfig.PrivateKey),
		Method:         "GET",
		ContentType:    contentType,
		Expires:        time.Now().Add(time.Hour * 24 * 30 * 12 * 50),
	})

	putUrl, err := storage.SignedURL(bucket, filename, &storage.SignedURLOptions{
		GoogleAccessID: firebaseConfig.ClientEmail,
		PrivateKey:     []byte(firebaseConfig.PrivateKey),
		Method:         "PUT",
		ContentType:    contentType,
		Expires:        time.Now().Add(time.Second * 60 * 10),
	})

	return getUrl, putUrl, err

}

func CreateFileSignedUrl(filename string, contentType string, fileType string) (response.SignedUrl, error) {
	path := GetFileBucketPath(fileType)

	if path == "" {
		return response.SignedUrl{}, errors.New("file type not valid")
	}
	hashName := fmt.Sprintf("%x", sha1.Sum([]byte(filename+time.Now().String())))
	getUrl, putUrl, err := createSignedUrl(defaultBucket, path+hashName, contentType)

	if err != nil {
		return response.SignedUrl{}, nil
	}

	return response.SignedUrl{
		Filename:    hashName,
		DownloadURL: getUrl,
		UploadURL:   putUrl,
		ContentType: contentType,
	}, nil

}

func GetFileBucketPath(fileType string) string {
	switch fileType {
	case FileTypeImage:
		return ImagePath
	case FileTypeVideo:
		return VideoPath
	case FileTypeUserAvatar:
		return UserAvatarPath
	case FileTypeUserCover:
		return UserCoverPath
	case FileTypeCompanyLogo:
		return CompanyLogoPath
	case FileTypeCompanyNamecard:
		return CompanyNamecardPath
	case FileTypeCompanyOther:
		return CompanyOtherPath
	case FileTypeChatFile:
		return ChatPath
	default:
		return ""
	}
}
