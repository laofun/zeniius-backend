package service

import (
	"os"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/sirupsen/logrus"
)

var (
	postgresURI string
	db          *gorm.DB
)

func init() {
	postgresURI = os.Getenv("POSTGRES_URI")
	var err error
	db, err = gorm.Open("postgres", postgresURI)
	db.LogMode(true)
	db.Set("gorm:auto_preload", true)
	if err != nil {
		panic(err)
	}

	logrus.Info("Database is connected: ", postgresURI)
}

func GetDatabaseConnection() *gorm.DB {
	return db
}
