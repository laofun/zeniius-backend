package service

import (
	"context"

	"cloud.google.com/go/translate"
	"github.com/sirupsen/logrus"
	"golang.org/x/text/language"
)

const (
	LanguageVietnam = "vi"
	LanguageEnglish = "en"
)

var (
	translateClient *translate.Client
	AcceptLanguage  = []string{LanguageEnglish, LanguageVietnam}
)

func init() {
	println("running translate")
	// Creates a client.
	var err error
	translateClient, err = translate.NewClient(ctx)
	if err != nil {
		logrus.Fatalf("Failed to create translate client: %v", err)
	}
}

func TranslateText(text string, targetLanguage string) (string, error) {
	target, err := language.Parse(targetLanguage)
	if err != nil {
		logrus.Error("Failed to parse target language: %v", err)
		return "", err
	}

	// Translates the text into Russian.
	ctx := context.Background()
	header := "(Translated Content) "
	translations, err := translateClient.Translate(ctx, []string{text, header}, target, nil)
	if err != nil {
		logrus.Error("Failed to translate text: %v", err)
		return "", err
	}
	translatedText := translations[1].Text + translations[0].Text

	return translatedText, nil

}

func DetectLanguage(text string) string {
	result, err := translateClient.DetectLanguage(ctx, []string{text})
	if err != nil {
		logrus.Error("Detect language error", err)
		return LanguageVietnam
	}
	if result[0][0].Language.String() == LanguageEnglish {
		return LanguageEnglish
	}
	return LanguageVietnam

}
