package service

import (
	"log"

	firebase "firebase.google.com/go"
	"firebase.google.com/go/auth"
	"github.com/sirupsen/logrus"
	"gitlab.com/rockship/backend/zeniius-backend/utils"
	"golang.org/x/net/context"
	"google.golang.org/api/option"
)

var (
	app           *firebase.App
	ctx           context.Context
	defaultBucket string
)

func init() {
	defaultBucket = utils.GetEnv("DEFAULT_BUCKET", "")
	config := &firebase.Config{
		StorageBucket: defaultBucket,
	}
	ctx = context.Background()
	opt := option.WithCredentialsFile(utils.GetEnv("SERVICE_KEY", ""))
	var err error
	app, err = firebase.NewApp(ctx, config, opt)
	if err != nil {
		log.Fatalf("error initializing app: %v\n", err)
	}

}

func VerifyIDToken(idToken string) (*auth.Token, error) {
	client, err := app.Auth(ctx)
	if err != nil {
		logrus.Errorf("error getting Auth client: %v\n", err)
		return nil, err
	}

	token, err := client.VerifyIDToken(ctx, idToken)
	if err != nil {
		logrus.Errorf("error verifying ID token: %v\n", err)
		return nil, err
	}

	return token, nil
}
