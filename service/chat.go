package service

import (
	"bytes"
	"errors"
	"fmt"
	"gitlab.com/rockship/backend/zeniius-backend/response"
	"gitlab.com/rockship/backend/zeniius-backend/utils"
	"net/http"
)

var (
	chatServiceHost, chatServiceToken string
)

func init() {
	chatServiceHost = utils.GetEnv("CHAT_SERVICE", "")
	chatServiceToken = utils.GetEnv("CHAT_TOKEN", "")
}

func SendMessageToGroup(postID int64, groupIDs []string, userID int64) error {

	if groupIDs == nil || len(groupIDs) == 0 {
		return nil
	}

	url := chatServiceHost + "/chat/v1/internal/sharing/post"
	fmt.Println("URL:>", url)
	payload := map[string]interface{}{
		"post_id":           postID,
		"conversation_list": groupIDs,
	}
	jsonPayload := response.JsonEncode(payload)
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonPayload))
	req.Header.Set("X-User-ID", fmt.Sprintf("%d", userID))
	req.Header.Set("Authorization", chatServiceToken)

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	fmt.Println("response Status:", resp.Status)
	if resp.StatusCode != 200 {
		return errors.New("call chat service error")
	}
	return nil
}
