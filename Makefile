.PHONY: run build migrate

run:
	go run main.go

start:
	pwd
	sed -i"" 's/zeniius.rockship.co/$(API_HOSTNAME)/g' swaggerui/swagger.json
	./build/zeniius-api

build:
	swagger generate spec -o swaggerui/swagger.json
	go build -o ./build/zeniius-api

doc:
	swagger generate spec -o swaggerui/swagger.json
	swagger serve --flavor=swagger  -p 3100 swaggerui/swagger.json

local:
	swagger generate spec -o swaggerui/swagger.json
	sed -i "" 's/zeniius.rockship.co/localhost:3500/g' swaggerui/swagger.json
	go build -o ./build/zeniius-api
	./build/zeniius-api

migrate:
	/build/zeniius-api --migrate
