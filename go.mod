module gitlab.com/rockship/backend/zeniius-backend

require (
	9fans.net/go v0.0.0-20180727211846-5d4fa602e1e8 // indirect
	cloud.google.com/go v0.25.0
	firebase.google.com/go v3.2.0+incompatible
	github.com/acroca/go-symbols v0.0.0-20180523203557-953befd75e22 // indirect
	github.com/asaskevich/govalidator v0.0.0-20180720115003-f9ffefc3facf
	github.com/cosiner/argv v0.0.0-20170225145430-13bacc38a0a5 // indirect
	github.com/davecgh/go-spew v1.1.0
	github.com/derekparker/delve v1.1.0 // indirect
	github.com/fatih/camelcase v1.0.0 // indirect
	github.com/fatih/gomodifytags v0.0.0-20180122092213-db1e1dec6c55 // indirect
	github.com/fatih/structtag v0.2.0 // indirect
	github.com/go-chi/chi v3.3.2+incompatible
	github.com/go-chi/cors v1.0.0
	github.com/golang/lint v0.0.0-20180702182130-06c8688daad7 // indirect
	github.com/golang/protobuf v1.1.0
	github.com/googleapis/gax-go v2.0.0+incompatible
	github.com/jinzhu/gorm v0.0.0-20180726234349-409121d9e394
	github.com/jinzhu/inflection v0.0.0-20180308033659-04140366298a
	github.com/jmoiron/sqlx v0.0.0-20180614180643-0dae4fefe7c0
	github.com/joho/godotenv v0.0.0-20180405053634-1709ab122c98
	github.com/josharian/impl v0.0.0-20180228163738-3d0f908298c4 // indirect
	github.com/karrick/godirwalk v1.7.3 // indirect
	github.com/lib/pq v0.0.0-20180523175426-90697d60dd84
	github.com/mattn/go-isatty v0.0.4 // indirect
	github.com/mdempsky/gocode v0.0.0-20180727200127-00e7f5ac290a // indirect
	github.com/peterh/liner v1.1.0 // indirect
	github.com/ramya-rao-a/go-outline v0.0.0-20170803230019-9e9d089bb61a // indirect
	github.com/rogpeppe/godef v0.0.0-20170920080713-b692db1de522 // indirect
	github.com/sirupsen/logrus v1.0.6
	github.com/spf13/cobra v0.0.3 // indirect
	github.com/spf13/pflag v1.0.2 // indirect
	github.com/sqs/goreturns v0.0.0-20180302073349-83e02874ec12 // indirect
	github.com/uudashr/gopkgs v1.3.2 // indirect
	go.opencensus.io v0.14.0
	golang.org/x/arch v0.0.0-20180516175055-5de9028c2478 // indirect
	golang.org/x/crypto v0.0.0-20180723164146-c126467f60eb
	golang.org/x/lint v0.0.0-20180702182130-06c8688daad7 // indirect
	golang.org/x/net v0.0.0-20180724234803-3673e40ba225
	golang.org/x/oauth2 v0.0.0-20180724155351-3d292e4d0cdc
	golang.org/x/sys v0.0.0-20180724212812-e072cadbbdc8
	golang.org/x/text v0.3.0
	golang.org/x/tools v0.0.0-20180904205237-0aa4b8830f48 // indirect
	google.golang.org/api v0.0.0-20180726000515-082d5fa4f1f0
	google.golang.org/appengine v1.1.0
	google.golang.org/genproto v0.0.0-20180726180014-2a72893556e4
	google.golang.org/grpc v1.13.0
	gopkg.in/guregu/null.v3 v3.3.0
	gopkg.in/yaml.v2 v2.2.1 // indirect
)
