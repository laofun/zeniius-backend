package response

// SignedUrl
// swagger:response SignedUrl
type SignedUrl struct {
	Filename        string `json:"filename"`
	DownloadURL     string `json:"download_url"`
	UploadURL       string `json:"upload_url"`
	ContentType     string `json:"content_type"`
	FileID          int64  `json:"file_id"`
	ThumbnailUpload string `json:"thumbnail_upload_url"`
}
