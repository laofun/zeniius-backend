package response

import (
	"encoding/json"

	"github.com/sirupsen/logrus"
)

func JsonEncode(data map[string]interface{}) []byte {
	b, err := json.Marshal(data)
	if err != nil {
		logrus.Error("Error encode json data: ", err)
		return []byte{}
	}
	return b
}

func JsonEncodeModel(data interface{}) []byte {
	b, err := json.Marshal(data)
	if err != nil {
		logrus.Error("Error encode json data: ", err)
		return []byte{}
	}
	return b
}
