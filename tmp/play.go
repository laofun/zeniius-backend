package main

import (
	"fmt"

	"time"
)

type ColorGroup struct {
	ID     int
	name   string
	Colors []string
}

func (c *ColorGroup) Name() string {
	return c.name
}

func f(n int) {
	for i:= 0; i < 10 ; i++ {
		fmt.Println(n, " : ", i)
	}
}

func pinger(c chan string) {
	for i := 0; ; i++ {
		c <- "ping" }
}

func ponger(c chan string) {
	for i := 0; ; i++ {
		c <- "pong" }
}

func printer(c chan string) {
	for {
		msg := <- c
		fmt.Println(msg)
		time.Sleep(time.Second * 1)
	}
}

func main() {

	var c chan string = make(chan string)
	go pinger(c)
	go ponger(c)
	go printer(c)
	var input string
	fmt.Scanln(&input)

}
