package validate

import (
	"github.com/asaskevich/govalidator"
	"gitlab.com/rockship/backend/zeniius-backend/api"
	"gitlab.com/rockship/backend/zeniius-backend/utils"
	"time"
)

func InitCustomValidator() {
	// post_cost
	// Validate cost struct member of post payload
	govalidator.CustomTypeTagMap.Set("post_cost", govalidator.CustomTypeValidator(func(i interface{}, context interface{}) bool {
		postCost, ok := context.(api.PostCost)
		if !ok {
			return false
		}
		if postCost.Type != utils.CostTypeRange && (postCost.CostFrom != nil || postCost.CostTo != nil) {
			return false
		}
		return true
	}))

	// post_type
	// validate required fields for each type of post
	govalidator.CustomTypeTagMap.Set("post_type", govalidator.CustomTypeValidator(func(i interface{}, context interface{}) bool {
		post, ok := context.(api.PostPayload)
		if !ok {
			return false
		}
		switch post.Type {
		case utils.PostTypeEvent:
			if post.StartTime == nil || post.EndTime == nil || post.LocationLong == nil || post.LocationLat == nil {
				return false
			}
			if post.StartTime.After(post.EndTime.UTC()) {
				return false
			}
			if post.EndTime.Before(time.Now()) {
				return false
			}
		}
		return true
	}))

	// post_privacy
	// validate privacy settings for post
	govalidator.CustomTypeTagMap.Set("post_privacy", govalidator.CustomTypeValidator(func(i interface{}, context interface{}) bool {
		post, ok := context.(api.PostPayload)
		if !ok {
			return false
		}
		if post.Privacy.IsPublic == false &&
			(post.Privacy.ShareGroup == nil || len(post.Privacy.ShareGroup) == 0) {
			return false
		}
		return true
	}))
}
